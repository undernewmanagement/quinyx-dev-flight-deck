<?php
$server = 0; // 0 = localhost

if (getenv('unittest') || isset($_GET['unittest']) || isset($_ENV['unittest'])) {
    $conf_unittest = 'unittest';
}

/*
if (!empty($conf_unittest) && 'unittest' == $conf_unittest) {
    define("LOCAL_HOSTNAME", "qwfm");
    define("SERVER", "mariadb01.test.de01.quinyx.com");
    define("USER", "developer");
    define("PWORD", "SuLw3A1SXAW7CVBIZ0xD");
    define("DBASE", "jenkins_qwfm_b9288c448875");
    define("USINGTESTDB", TRUE);
} else {
*/
    if( $server == 0 )
    {
        # use php7 ?? coalesce operator to set default values if no env vars are set
        $dbserver = getenv('DB_SERVER') ?? '127.0.0.1' ;
        $dbpass = getenv('DB_PASSWORD') ?? '123456';
        $dbuser = getenv('DB_USER') ?? 'root';
        $dbname = getenv('DB_NAME') ?? 'quintime';

        $redis_server = getenv('REDIS_SERVER') ?? 'tcp://redis01.test.hq.quinyx.com:6379';

        $gelf_host = getenv('GELF_HOST') ?? 'graylog';
        $gelf_port = getenv('GELF_PORT') ?? 12201;

        define("SERVER", $dbserver);
        define("USER", $dbuser);
        define("PWORD", $dbpass);
        define("DBASE", $dbname);
    }
//}

//log
define('DEBUG_LOG','/var/log/debug.log');
define('APPLICATION_LOG','/var/log/application.log');

// RestClient
define("JAVA_API_BASE_URL", "http://web.test.quinyx.com/api");
define("PHPUSER_LOGIN", ""); // A "php" user that has to have CRUD access to the Global group in Auth. Used by RestClient to authenticate as the php-service to the java api.
define("PHPUSER_PASSWORD", "");
define('AUTH_SESSION_NAME', 'SESSIONID'); // The name of the session cookie coming from auth
define('AUTH_SESSION_NAME_IN_REDIS', 'AUTH_SESSION_ID');
define('AUTH_SESSION_EXPIRE_NAME_IN_REDIS', 'AUTH_SESSION_ID_EXPIRES');

define('REDIS_CONFIG', $redis_server ); // tcp://host:port?option=value

// Feature flags
define("SECTIONS_FROM_AUTH", false); // Enable to fetch a users sections from the java auth api, requires rest client.
define("QWFM_TITLE", "Quinyx WFM");
define("QWFM_FAVICON", "img/qlogoyellow16.png");

// LOGGING
//define('GELF_HOST', $gelf_host );
//define('GELF_PORT', $gelf_port );

// MACAROONS
define("MACAROON_SHARED_SECRET", "supersecretsecretofsecrets");
define("MACAROON_COOKIE_NAME", "auth-local");
define("MACAROON_DOMAIN", "qwfm");

// PDFMERGER
define("PDF_MERGE_URL", "http://pdf.ws.quinyx.com/");

// Enable to stop all incoming logins.
define("MAINTENANCE_MODE", 0);
define("PRODUCTION", 0);
define("DEBUG", 1);
define("PERFLOG", 1);
define("PERFLOG_USE_MESS_ID", 0);
define("METHODS_IN_QUERY_LOG", "/var/log/mysqld.log");

define("DBASE_ENGINE", "mysqli");
define("DBASE_ABSTRACTION_LAYER", "ADOdb");
define("SMTP_ISV", "127.0.0.1");
define("EMAIL_SENDER", "info@calnet.se");
define("EMAIL_WEBMASTER", "webmaster@calnet.se");
define("ERROR_LOG_RECIPIENTS","martin.danielsson@quinyx.com");
define("ERROR_LOG_SENDER","error@quinyx.com");
// Enable to send outgoing emails and sms.
define("SEND_MAIL", "0");
define("SEND_SMS", "0");

define("SEND_ERROR_EMAILS",false);
define("SEND_DEBUG_EMAILS",false);
define("SEND_SMS_CONFIRMATION",false);
#define("CLICKATELL_NUMBER","+46737494553");
define("AUTHMAIL_USE", "0");
define("USE_PHP_SESSIONS", false);
define("BACKUPTABLE", "backup");
define("ROLLBACKTABLE", "rollback");
define("AUTHMAIL_HOST", "mailhog");
define("AUTHMAIL_PORT", "1025");
define("AUTHMAIL_ENCRYPTION", null);
define("AUTHMAIL_USER", "test");
define("AUTHMAIL_PWORD", "!QAZ2wsx");
define("MESSAGING_SERVER_ADDRESS", "");
define("MESSAGING_PORT", 61613);
define("MESSAGING_SERVICE", "LCDS");
date_default_timezone_set("Europe/Stockholm");