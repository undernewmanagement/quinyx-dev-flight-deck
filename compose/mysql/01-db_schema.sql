# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: mariadb01.test.de01.quinyx.com (MySQL 5.5.5-10.0.27-MariaDB-0+deb8u1)
# Database: quintime
# Generation Time: 2016-12-19 10:34:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table absschedule
# ------------------------------------------------------------

CREATE TABLE `absschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `replaceScheduledDays` tinyint(1) NOT NULL DEFAULT '0',
  `notOnFreeDays` tinyint(1) NOT NULL DEFAULT '0',
  `replaceAllDays` tinyint(1) NOT NULL DEFAULT '0',
  `replaceFreeDays` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `absschedule_1` (`restId`),
  KEY `absschedule_2` (`categoryId`),
  CONSTRAINT `absschedule_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `absschedule_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=724 DEFAULT CHARSET=utf8;



# Dump of table absschedule_shift
# ------------------------------------------------------------

CREATE TABLE `absschedule_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '0',
  `weekDay` tinyint(1) NOT NULL DEFAULT '0',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `absschedule_shift_1` (`scheduleId`),
  CONSTRAINT `absschedule_shift_1` FOREIGN KEY (`scheduleId`) REFERENCES `absschedule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6815 DEFAULT CHARSET=utf8;



# Dump of table access
# ------------------------------------------------------------

CREATE TABLE `access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `value` tinyint(2) NOT NULL,
  `roleId` int(11) NOT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `customerId` int(11) NOT NULL,
  `districtId` int(11) DEFAULT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `level` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `categoryId` (`categoryId`),
  KEY `customerId` (`customerId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `identifier` (`identifier`),
  KEY `value` (`value`),
  KEY `roleId` (`roleId`),
  KEY `districtId` (`districtId`),
  KEY `level` (`level`),
  KEY `merge_customer_role` (`customerId`,`roleId`),
  KEY `merge_customer_role_identifier` (`identifier`,`customerId`,`roleId`),
  CONSTRAINT `access_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `access_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `jobcategory` (`jcatId`) ON DELETE CASCADE,
  CONSTRAINT `access_ibfk_3` FOREIGN KEY (`customerId`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `access_ibfk_4` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=195380 DEFAULT CHARSET=utf8;



# Dump of table access_changes_log
# ------------------------------------------------------------

CREATE TABLE `access_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=84644 DEFAULT CHARSET=latin1;



# Dump of table access_deletes_log
# ------------------------------------------------------------

CREATE TABLE `access_deletes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `qt_custId` int(11) NOT NULL,
  `objectName` varchar(128) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table access_summary_log
# ------------------------------------------------------------

CREATE TABLE `access_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=20794 DEFAULT CHARSET=latin1;



# Dump of table activity
# ------------------------------------------------------------

CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `isOptional` tinyint(1) NOT NULL DEFAULT '0',
  `hasData` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `groupId` int(11) DEFAULT NULL,
  `flowgraph_id` int(11) DEFAULT NULL,
  `flowgraph_predefined_set_id` int(11) DEFAULT NULL,
  `mandatory_approved` tinyint(1) NOT NULL DEFAULT '0',
  `submodule_id` int(11) unsigned DEFAULT NULL,
  KEY `id` (`id`),
  KEY `fk_flowgraph` (`flowgraph_id`),
  KEY `fk_flowgraph_predefined_process_steps` (`flowgraph_predefined_set_id`),
  KEY `idx_activity_groupid` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=1081 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table activity_employee
# ------------------------------------------------------------

CREATE TABLE `activity_employee` (
  `activityGroupId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  PRIMARY KEY (`activityGroupId`,`employeeId`),
  KEY `fk_activity_employee_employee` (`employeeId`),
  CONSTRAINT `fk_activity_employee_employee` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;



# Dump of table activity_files
# ------------------------------------------------------------

CREATE TABLE `activity_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) DEFAULT NULL,
  `activityGroupId` int(11) DEFAULT NULL,
  `subgroupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;



# Dump of table activity_flowgraph
# ------------------------------------------------------------

CREATE TABLE `activity_flowgraph` (
  `flowgraphId` int(11) NOT NULL,
  `activityVersionId` int(11) NOT NULL,
  PRIMARY KEY (`activityVersionId`,`flowgraphId`),
  KEY `fk_activity_flowgraph_flowgraph1` (`flowgraphId`),
  KEY `fk_activity` (`activityVersionId`),
  CONSTRAINT `fk_activity` FOREIGN KEY (`activityVersionId`) REFERENCES `version` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_activity_flowgraph_flowgraph1` FOREIGN KEY (`flowgraphId`) REFERENCES `flow_graph` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table activity_order
# ------------------------------------------------------------

CREATE TABLE `activity_order` (
  `activityGroupId` int(11) NOT NULL,
  `sort` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table activity_products
# ------------------------------------------------------------

CREATE TABLE `activity_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityId` int(11) NOT NULL,
  `predefinedValueId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_predefined_processteps_activity1` (`activityId`),
  KEY `fk_predefined_processteps_predefined_values1` (`predefinedValueId`),
  CONSTRAINT `fk_predefined_processteps_activity1` FOREIGN KEY (`activityId`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_predefined_processteps_predefined_values1` FOREIGN KEY (`predefinedValueId`) REFERENCES `predefined_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2703 DEFAULT CHARSET=utf8;



# Dump of table activity_subgroup
# ------------------------------------------------------------

CREATE TABLE `activity_subgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1151 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table activity_subgroup_relation
# ------------------------------------------------------------

CREATE TABLE `activity_subgroup_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityId` int(11) NOT NULL,
  `subgroupId` int(11) NOT NULL,
  PRIMARY KEY (`subgroupId`,`activityId`),
  KEY `activity_subgroup_relation_fk2` (`activityId`),
  KEY `id` (`id`),
  CONSTRAINT `activity_subgroup_relation_fk1` FOREIGN KEY (`subgroupId`) REFERENCES `activity_subgroup` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `activity_subgroup_relation_fk2` FOREIGN KEY (`activityId`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2954 DEFAULT CHARSET=utf8;



# Dump of table additional_field_data_employee
# ------------------------------------------------------------

CREATE TABLE `additional_field_data_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entityId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `data` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `entityId` (`entityId`,`fieldId`),
  CONSTRAINT `additional_field_data_employee_ibfk_1` FOREIGN KEY (`entityId`) REFERENCES `employee` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table additional_field_employee
# ------------------------------------------------------------

CREATE TABLE `additional_field_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qt_customerId` int(11) NOT NULL,
  `field` varchar(64) NOT NULL DEFAULT '',
  `title` varchar(128) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL,
  `unique` tinyint(1) NOT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `regex` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `additional_field_employee_ibfk_1` (`qt_customerId`),
  CONSTRAINT `additional_field_employee_ibfk_1` FOREIGN KEY (`qt_customerId`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table agree_mhours
# ------------------------------------------------------------

CREATE TABLE `agree_mhours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreeId` int(11) NOT NULL DEFAULT '0',
  `year` year(4) NOT NULL DEFAULT '2009',
  `month` int(2) NOT NULL DEFAULT '0',
  `hours` decimal(6,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agreeId` (`agreeId`),
  CONSTRAINT `agree_mhours_1` FOREIGN KEY (`agreeId`) REFERENCES `agreement_v2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=879843 DEFAULT CHARSET=utf8;



# Dump of table agree_thours
# ------------------------------------------------------------

CREATE TABLE `agree_thours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL DEFAULT '0',
  `year` year(4) NOT NULL DEFAULT '2009',
  `month` int(2) NOT NULL DEFAULT '0',
  `hours` decimal(6,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  CONSTRAINT `agree_thours_1` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83810 DEFAULT CHARSET=utf8;



# Dump of table agreement_cache
# ------------------------------------------------------------

CREATE TABLE `agreement_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreementId` int(11) DEFAULT '0',
  `templateId` int(11) DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `recordType` tinyint(1) DEFAULT '0',
  `agreementVersion` int(4) NOT NULL DEFAULT '0',
  `isMainAgreement` tinyint(1) NOT NULL DEFAULT '0',
  `agreement` mediumtext NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agreementId` (`agreementId`),
  KEY `templateId` (`templateId`),
  KEY `persId` (`persId`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=3178186 DEFAULT CHARSET=utf8;



# Dump of table agreement_overtime_method
# ------------------------------------------------------------

CREATE TABLE `agreement_overtime_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreementId` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  `overtimeMethodId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agreementId` (`agreementId`),
  KEY `overtimeMethodId` (`overtimeMethodId`),
  CONSTRAINT `fk_agreement_overtime_method_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_agreement_overtime_method_2` FOREIGN KEY (`overtimeMethodId`) REFERENCES `overtime_method` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50598 DEFAULT CHARSET=utf8;



# Dump of table agreement_rounding
# ------------------------------------------------------------

CREATE TABLE `agreement_rounding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreementId` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `punchPart` tinyint(1) NOT NULL DEFAULT '0',
  `insideShift` tinyint(1) NOT NULL DEFAULT '0',
  `roundingRule` tinyint(1) NOT NULL DEFAULT '0',
  `minutes` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `indicatorColor` int(10) NOT NULL DEFAULT '6684825',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agreementId` (`agreementId`),
  CONSTRAINT `fk_agreement_rounding_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22375 DEFAULT CHARSET=utf8;



# Dump of table agreement_settings
# ------------------------------------------------------------

CREATE TABLE `agreement_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreementId` int(11) DEFAULT NULL,
  `agreementTemplateId` int(11) DEFAULT NULL,
  `type` int(10) unsigned NOT NULL,
  `value` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agreementId` (`agreementId`),
  KEY `agreementTemplateId` (`agreementTemplateId`),
  KEY `type` (`type`),
  CONSTRAINT `agreementId` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `agreementTemplateId` FOREIGN KEY (`agreementTemplateId`) REFERENCES `agreement_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=625 DEFAULT CHARSET=utf8;



# Dump of table agreement_template
# ------------------------------------------------------------

CREATE TABLE `agreement_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `templateName` varchar(50) NOT NULL,
  `weekHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `minHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `maxHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `monthlySalary` decimal(10,2) NOT NULL DEFAULT '0.00',
  `customMonthlyHoursDivisor` decimal(5,2) NOT NULL DEFAULT '0.00',
  `salary` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary` decimal(10,2) NOT NULL DEFAULT '125.00',
  `premiumSalary2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary4` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary5` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary6` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary7` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary8` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumType` tinyint(1) NOT NULL DEFAULT '0',
  `hourly` tinyint(1) NOT NULL DEFAULT '1',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `expires` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `periodType` tinyint(1) NOT NULL DEFAULT '1',
  `periodDay` int(2) NOT NULL DEFAULT '16',
  `periodStart` date NOT NULL DEFAULT '0000-00-00',
  `periodLength` int(4) NOT NULL DEFAULT '1',
  `weeklyAvg` decimal(10,2) NOT NULL DEFAULT '0.00',
  `empLevel` decimal(5,2) NOT NULL DEFAULT '1.00',
  `stdRedDays` tinyint(1) NOT NULL DEFAULT '1',
  `stdOverTime` tinyint(1) NOT NULL DEFAULT '1',
  `maxHourDay` decimal(6,2) NOT NULL DEFAULT '8.00',
  `maxHourWeek` decimal(6,2) NOT NULL DEFAULT '40.00',
  `minShift` int(2) NOT NULL DEFAULT '8',
  `minRest` int(2) NOT NULL DEFAULT '8',
  `weekStart` tinyint(1) NOT NULL DEFAULT '1',
  `atDayBreak` tinyint(1) NOT NULL DEFAULT '1',
  `maxDays` int(2) NOT NULL DEFAULT '5',
  `minFreeDays` int(2) NOT NULL DEFAULT '2',
  `reduxBefRedDay` decimal(6,2) NOT NULL DEFAULT '0.00',
  `reduxBefRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `reduxRedDay` decimal(6,2) NOT NULL DEFAULT '100.00',
  `reduxRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `weeksAvail` int(2) NOT NULL DEFAULT '1',
  `minHrsWeek` decimal(6,2) NOT NULL DEFAULT '0.00',
  `dayBreak` time NOT NULL DEFAULT '00:00:00',
  `schedWeekEnds` tinyint(1) NOT NULL DEFAULT '1',
  `minRestWeek` decimal(6,2) NOT NULL DEFAULT '0.00',
  `minWeekEndYear` int(2) NOT NULL DEFAULT '0',
  `seqDays` int(3) NOT NULL DEFAULT '0',
  `seqMaxDays` int(3) NOT NULL DEFAULT '0',
  `seqMaxHrs` int(3) NOT NULL DEFAULT '0',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `maxTimeWoBreak` decimal(6,2) NOT NULL DEFAULT '8.00',
  `minBreak` int(2) NOT NULL DEFAULT '0',
  `maxBreak` int(4) NOT NULL DEFAULT '0',
  `scheduleLockPeriod` int(3) NOT NULL DEFAULT '0',
  `scheduleLockPeriodType` tinyint(1) NOT NULL DEFAULT '0',
  `checkAvailability` tinyint(1) NOT NULL DEFAULT '1',
  `isGlobalTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `locale` varchar(15) NOT NULL DEFAULT '',
  `punchType` tinyint(1) NOT NULL DEFAULT '0',
  `appPayOnAbsence` tinyint(1) NOT NULL DEFAULT '1',
  `payOtime` tinyint(1) NOT NULL DEFAULT '1',
  `showIgnoreOtime` tinyint(1) NOT NULL DEFAULT '0',
  `showUnspecifiedLeaveInWP` tinyint(1) NOT NULL DEFAULT '0',
  `allowPunchingOnSubshifts` tinyint(1) NOT NULL DEFAULT '0',
  `genMoretime` tinyint(1) NOT NULL DEFAULT '1',
  `overtimeMethod` tinyint(1) NOT NULL DEFAULT '1',
  `overtimeWindow` int(4) NOT NULL DEFAULT '31',
  `useMaxMin` tinyint(1) NOT NULL DEFAULT '0',
  `useRound` tinyint(1) NOT NULL DEFAULT '0',
  `maxMinBef` int(3) NOT NULL DEFAULT '0',
  `maxMinAft` int(3) NOT NULL DEFAULT '0',
  `roundBef` int(3) NOT NULL DEFAULT '0',
  `roundAft` int(3) NOT NULL DEFAULT '0',
  `autoPunchOut` decimal(6,2) NOT NULL DEFAULT '12.00',
  `lateChange` tinyint(1) NOT NULL DEFAULT '0',
  `lateChangeUT` tinyint(1) NOT NULL DEFAULT '1',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestUT` tinyint(1) NOT NULL DEFAULT '1',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestUT` tinyint(1) NOT NULL DEFAULT '1',
  `threeSixtyYear` tinyint(1) NOT NULL DEFAULT '1',
  `fiveDayWeek` tinyint(1) NOT NULL DEFAULT '1',
  `timeBank` tinyint(1) NOT NULL DEFAULT '1',
  `payrollSystem` int(2) NOT NULL DEFAULT '0',
  `adjustForLeave` tinyint(1) NOT NULL DEFAULT '0',
  `excludeSundaysInWorkHours` tinyint(1) NOT NULL DEFAULT '1',
  `employmentRate` decimal(5,4) NOT NULL DEFAULT '1.0000',
  `schedulePeriodStartDate` date NOT NULL DEFAULT '0000-00-00',
  `schedulePeriodLength` int(3) unsigned NOT NULL DEFAULT '0',
  `schedulePeriodType` tinyint(1) NOT NULL DEFAULT '0',
  `fullEmploymentDaysPerWeek` int(2) unsigned NOT NULL DEFAULT '0',
  `fullEmploymentDays` decimal(6,2) NOT NULL DEFAULT '0.00',
  `fullEmploymentHrs` decimal(6,2) NOT NULL DEFAULT '0.00',
  `danishTimeRules` tinyint(1) NOT NULL DEFAULT '0',
  `otCancelsUt` tinyint(1) NOT NULL DEFAULT '1',
  `allowOverrideOverlapping` tinyint(1) NOT NULL DEFAULT '1',
  `utThreshold` decimal(10,2) NOT NULL DEFAULT '0.00',
  `utThresholdType` int(3) NOT NULL DEFAULT '0',
  `dailyOvertimeHours` decimal(6,2) NOT NULL DEFAULT '0.00',
  `weeklyOvertimeHours` decimal(6,2) NOT NULL DEFAULT '0.00',
  `costMultiplier` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `noCostPeriod` int(3) NOT NULL DEFAULT '0',
  `noCostPeriodStart` varchar(11) DEFAULT NULL,
  `noCostPeriodEnd` varchar(11) DEFAULT NULL,
  `balancePeriodType` tinyint(4) NOT NULL DEFAULT '0',
  `balancePeriodLength` int(3) NOT NULL DEFAULT '0',
  `balancePeriodHours` float NOT NULL DEFAULT '0',
  `balancePeriodStartDate` date NOT NULL DEFAULT '0000-00-00',
  `balancePeriodOnlyMainAgreement` tinyint(4) NOT NULL DEFAULT '1',
  `accumulationFactor` decimal(6,4) NOT NULL DEFAULT '0.0000',
  `accumulationType` tinyint(1) NOT NULL DEFAULT '0',
  `breakRounding` tinyint(1) NOT NULL DEFAULT '0',
  `useWorkDayModel` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `isGlobalTemplate` (`isGlobalTemplate`),
  CONSTRAINT `agreement_template_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15050 DEFAULT CHARSET=utf8;



# Dump of table agreement_template_changes_log
# ------------------------------------------------------------

CREATE TABLE `agreement_template_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=488013 DEFAULT CHARSET=latin1;



# Dump of table agreement_template_deletes_log
# ------------------------------------------------------------

CREATE TABLE `agreement_template_deletes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `qt_custId` int(11) NOT NULL,
  `objectName` varchar(128) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table agreement_template_overtime_method
# ------------------------------------------------------------

CREATE TABLE `agreement_template_overtime_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  `overtimeMethodId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  KEY `overtimeMethodId` (`overtimeMethodId`),
  CONSTRAINT `fk_agreement_template_overtime_method_1` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_agreement_template_overtime_method_2` FOREIGN KEY (`overtimeMethodId`) REFERENCES `overtime_method` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11293 DEFAULT CHARSET=utf8;



# Dump of table agreement_template_rounding
# ------------------------------------------------------------

CREATE TABLE `agreement_template_rounding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `punchPart` tinyint(1) NOT NULL DEFAULT '0',
  `insideShift` tinyint(1) NOT NULL DEFAULT '0',
  `roundingRule` tinyint(1) NOT NULL DEFAULT '0',
  `minutes` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `indicatorColor` int(10) NOT NULL DEFAULT '6684825',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  CONSTRAINT `fk_agreement_template_rounding_1` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=49416 DEFAULT CHARSET=utf8;



# Dump of table agreement_template_summary_log
# ------------------------------------------------------------

CREATE TABLE `agreement_template_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=50023 DEFAULT CHARSET=latin1;



# Dump of table agreement_template_timetracker
# ------------------------------------------------------------

CREATE TABLE `agreement_template_timetracker` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `agreementTemplateId` int(11) NOT NULL,
  `timeTrackerId` int(11) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `accrualDriver` varchar(64) NOT NULL DEFAULT '',
  `accrualFactorLevelDeterminer` varchar(64) NOT NULL DEFAULT '',
  `onlyThresholds` tinyint(1) NOT NULL DEFAULT '0',
  `useDriverDeltaValue` tinyint(1) NOT NULL DEFAULT '1',
  `employmentRateAffectsAccrualFactor` tinyint(1) NOT NULL,
  `dateData` varchar(64) NOT NULL DEFAULT '',
  `operator` varchar(64) NOT NULL DEFAULT 'add',
  PRIMARY KEY (`id`),
  KEY `agreementTemplateId` (`agreementTemplateId`),
  KEY `timeTrackerId` (`timeTrackerId`),
  CONSTRAINT `agreement_template_timetracker_ibfk_1` FOREIGN KEY (`agreementTemplateId`) REFERENCES `agreement_template` (`id`) ON DELETE CASCADE,
  CONSTRAINT `agreement_template_timetracker_ibfk_2` FOREIGN KEY (`timeTrackerId`) REFERENCES `timetracker` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1046 DEFAULT CHARSET=utf8;



# Dump of table agreement_template_timetracker_factor_level
# ------------------------------------------------------------

CREATE TABLE `agreement_template_timetracker_factor_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `templateTimeTrackerAssocId` int(11) unsigned NOT NULL,
  `threshold` double NOT NULL,
  `factor` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templateTimeTrackerAssocId` (`templateTimeTrackerAssocId`),
  CONSTRAINT `agreement_template_timetracker_factor_level_ibfk_1` FOREIGN KEY (`templateTimeTrackerAssocId`) REFERENCES `agreement_template_timetracker` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1424 DEFAULT CHARSET=utf8;



# Dump of table agreement_v2
# ------------------------------------------------------------

CREATE TABLE `agreement_v2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `persId` int(11) NOT NULL DEFAULT '0',
  `weekHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `minHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `maxHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `monthlySalary` decimal(10,2) NOT NULL DEFAULT '0.00',
  `customMonthlyHoursDivisor` decimal(5,2) NOT NULL DEFAULT '0.00',
  `salary` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary` decimal(10,2) NOT NULL DEFAULT '125.00',
  `premiumSalary2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary4` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary5` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary6` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary7` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumSalary8` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumType` tinyint(1) NOT NULL DEFAULT '0',
  `hourly` tinyint(1) NOT NULL DEFAULT '1',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `expires` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `periodType` tinyint(1) NOT NULL DEFAULT '1',
  `periodDay` int(2) NOT NULL DEFAULT '16',
  `periodStart` date NOT NULL DEFAULT '0000-00-00',
  `periodLength` int(4) NOT NULL DEFAULT '1',
  `weeklyAvg` decimal(10,2) NOT NULL DEFAULT '0.00',
  `empLevel` decimal(8,4) NOT NULL DEFAULT '1.0000',
  `templateId` int(11) NOT NULL DEFAULT '0',
  `weeksAvail` int(2) NOT NULL DEFAULT '1',
  `useTempAvail` tinyint(1) NOT NULL DEFAULT '1',
  `useTempTimeRules` tinyint(1) NOT NULL DEFAULT '1',
  `useTempSalary` tinyint(1) NOT NULL DEFAULT '1',
  `useTempWorkTime` tinyint(1) NOT NULL DEFAULT '1',
  `useTempMonthHours` tinyint(1) NOT NULL DEFAULT '1',
  `useTempRedDates` tinyint(1) NOT NULL DEFAULT '1',
  `useStdTimeRules` tinyint(1) NOT NULL DEFAULT '0',
  `useTempTandA` tinyint(1) NOT NULL DEFAULT '1',
  `useStdRedDates` tinyint(1) NOT NULL DEFAULT '1',
  `maxHourDay` decimal(6,2) NOT NULL DEFAULT '8.00',
  `maxHourWeek` decimal(6,2) NOT NULL DEFAULT '40.00',
  `minHrsWeek` decimal(6,2) NOT NULL DEFAULT '0.00',
  `minShift` int(2) NOT NULL DEFAULT '1',
  `minRest` int(2) NOT NULL DEFAULT '13',
  `weekStart` tinyint(1) NOT NULL DEFAULT '1',
  `atDayBreak` tinyint(1) NOT NULL DEFAULT '1',
  `dayBreak` time NOT NULL DEFAULT '00:00:00',
  `schedWeekEnds` tinyint(1) NOT NULL DEFAULT '1',
  `maxDays` int(2) NOT NULL DEFAULT '5',
  `minFreeDays` int(2) NOT NULL DEFAULT '2',
  `minRestWeek` decimal(6,2) NOT NULL DEFAULT '0.00',
  `minWeekEndYear` int(2) NOT NULL DEFAULT '0',
  `reduxBefRedDay` decimal(6,2) NOT NULL DEFAULT '0.00',
  `reduxBefRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `reduxRedDay` decimal(6,2) NOT NULL DEFAULT '100.00',
  `reduxRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `seqDays` int(3) NOT NULL DEFAULT '0',
  `seqMaxDays` int(3) NOT NULL DEFAULT '0',
  `seqMaxHrs` int(3) NOT NULL DEFAULT '0',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `maxTimeWoBreak` decimal(6,2) NOT NULL DEFAULT '8.00',
  `minBreak` int(2) NOT NULL DEFAULT '0',
  `maxBreak` int(4) NOT NULL DEFAULT '0',
  `scheduleLockPeriod` int(3) NOT NULL DEFAULT '0',
  `scheduleLockPeriodType` tinyint(1) NOT NULL DEFAULT '0',
  `checkAvailability` tinyint(1) NOT NULL DEFAULT '1',
  `isMainAgreement` tinyint(1) NOT NULL DEFAULT '1',
  `punchType` tinyint(1) NOT NULL DEFAULT '0',
  `appPayOnAbsence` tinyint(1) NOT NULL DEFAULT '1',
  `payOtime` tinyint(1) NOT NULL DEFAULT '1',
  `showIgnoreOtime` tinyint(1) NOT NULL DEFAULT '0',
  `showUnspecifiedLeaveInWP` tinyint(1) NOT NULL DEFAULT '0',
  `allowPunchingOnSubshifts` tinyint(1) NOT NULL DEFAULT '0',
  `genMoretime` tinyint(1) NOT NULL DEFAULT '1',
  `overtimeMethod` tinyint(1) NOT NULL DEFAULT '1',
  `overtimeWindow` int(4) NOT NULL DEFAULT '31',
  `useMaxMin` tinyint(1) NOT NULL DEFAULT '0',
  `useRound` tinyint(1) NOT NULL DEFAULT '0',
  `maxMinBef` int(3) NOT NULL DEFAULT '0',
  `maxMinAft` int(3) NOT NULL DEFAULT '0',
  `roundBef` int(3) NOT NULL DEFAULT '0',
  `roundAft` int(3) NOT NULL DEFAULT '0',
  `autoPunchOut` decimal(6,2) NOT NULL DEFAULT '12.00',
  `lateChange` tinyint(1) NOT NULL DEFAULT '0',
  `lateChangeUT` tinyint(1) NOT NULL DEFAULT '1',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestUT` tinyint(1) NOT NULL DEFAULT '1',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestUT` tinyint(1) NOT NULL DEFAULT '1',
  `threeSixtyYear` tinyint(1) NOT NULL DEFAULT '1',
  `fiveDayWeek` tinyint(1) NOT NULL DEFAULT '1',
  `timeBank` tinyint(1) NOT NULL DEFAULT '1',
  `payrollSystem` int(2) NOT NULL DEFAULT '0',
  `adjustForLeave` tinyint(1) NOT NULL DEFAULT '0',
  `excludeSundaysInWorkHours` tinyint(1) NOT NULL DEFAULT '1',
  `crmCustomerId` int(11) NOT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `additionalField1` varchar(50) DEFAULT NULL,
  `additionalField2` varchar(50) DEFAULT NULL,
  `additionalField3` varchar(50) DEFAULT NULL,
  `additionalField4` varchar(50) DEFAULT NULL,
  `additionalField5` varchar(50) DEFAULT NULL,
  `useObjectAgreement` tinyint(1) NOT NULL DEFAULT '1',
  `employmentRate` decimal(5,4) NOT NULL DEFAULT '1.0000',
  `schedulePeriodStartDate` date NOT NULL DEFAULT '0000-00-00',
  `schedulePeriodLength` int(3) unsigned NOT NULL DEFAULT '0',
  `schedulePeriodType` tinyint(1) NOT NULL DEFAULT '0',
  `fullEmploymentDaysPerWeek` int(2) unsigned NOT NULL DEFAULT '0',
  `fullEmploymentDays` decimal(6,2) NOT NULL DEFAULT '0.00',
  `fullEmploymentHrs` decimal(6,2) NOT NULL DEFAULT '0.00',
  `danishTimeRules` tinyint(1) NOT NULL DEFAULT '0',
  `otCancelsUt` tinyint(1) NOT NULL DEFAULT '1',
  `allowOverrideOverlapping` tinyint(1) NOT NULL DEFAULT '1',
  `utThreshold` decimal(10,2) NOT NULL DEFAULT '0.00',
  `utThresholdType` int(3) NOT NULL DEFAULT '0',
  `dailyOvertimeHours` decimal(6,2) NOT NULL DEFAULT '0.00',
  `weeklyOvertimeHours` decimal(6,2) NOT NULL DEFAULT '0.00',
  `costMultiplier` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `noCostPeriod` int(3) NOT NULL DEFAULT '0',
  `noCostPeriodStart` varchar(11) DEFAULT NULL,
  `noCostPeriodEnd` varchar(11) DEFAULT NULL,
  `balancePeriodType` tinyint(4) NOT NULL DEFAULT '0',
  `balancePeriodLength` int(3) NOT NULL DEFAULT '0',
  `balancePeriodHours` float NOT NULL DEFAULT '0',
  `balancePeriodStartDate` date NOT NULL DEFAULT '0000-00-00',
  `useTemplateBalancePeriod` tinyint(4) NOT NULL DEFAULT '1',
  `useTempCosts` tinyint(1) NOT NULL DEFAULT '1',
  `accumulationFactor` decimal(6,4) NOT NULL DEFAULT '0.0000',
  `accumulationType` tinyint(1) NOT NULL DEFAULT '0',
  `breakRounding` tinyint(1) NOT NULL DEFAULT '0',
  `useWorkDayModel` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId_fromDate` (`persId`,`fromDate`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `agreement_v2_ibfk_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `agreement_v2_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=697390 DEFAULT CHARSET=utf8;



# Dump of table agreement_v2_changes_log
# ------------------------------------------------------------

CREATE TABLE `agreement_v2_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=53968943 DEFAULT CHARSET=latin1;



# Dump of table agreement_v2_deletes_log
# ------------------------------------------------------------

CREATE TABLE `agreement_v2_deletes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `qt_custId` int(11) NOT NULL,
  `objectName` varchar(128) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=352805 DEFAULT CHARSET=latin1;



# Dump of table agreement_v2_summary_log
# ------------------------------------------------------------

CREATE TABLE `agreement_v2_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=25130129 DEFAULT CHARSET=latin1;



# Dump of table agreementscheduleminlength
# ------------------------------------------------------------

CREATE TABLE `agreementscheduleminlength` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agreementId` int(11) NOT NULL,
  `length` float NOT NULL,
  `fromTime` varchar(8) NOT NULL,
  `toTime` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agreementIndex` (`agreementId`),
  CONSTRAINT `agreement_v2_fk` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57751 DEFAULT CHARSET=latin1;



# Dump of table agreementtemplatescheduleminlength
# ------------------------------------------------------------

CREATE TABLE `agreementtemplatescheduleminlength` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agreementTemplateId` int(11) NOT NULL,
  `length` float NOT NULL,
  `fromTime` varchar(8) NOT NULL,
  `toTime` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agreementTemplateIndex` (`agreementTemplateId`),
  CONSTRAINT `agreement_template_fk` FOREIGN KEY (`agreementTemplateId`) REFERENCES `agreement_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=77736 DEFAULT CHARSET=latin1;



# Dump of table agreetemp_blockreason
# ------------------------------------------------------------

CREATE TABLE `agreetemp_blockreason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `portal` tinyint(1) NOT NULL DEFAULT '0',
  `templateId` int(11) NOT NULL DEFAULT '0',
  `reasonId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  KEY `reasonId` (`reasonId`),
  CONSTRAINT `reasonId` FOREIGN KEY (`reasonId`) REFERENCES `leavereason` (`id`),
  CONSTRAINT `templateId` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=992587 DEFAULT CHARSET=utf8;



# Dump of table agreetemp_blocksaltypes
# ------------------------------------------------------------

CREATE TABLE `agreetemp_blocksaltypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL DEFAULT '0',
  `stCode` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  CONSTRAINT `agreetemp_blocksaltypes_ibfk_1` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9441819 DEFAULT CHARSET=utf8;



# Dump of table agreetemp_extcode
# ------------------------------------------------------------

CREATE TABLE `agreetemp_extcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) DEFAULT '0',
  `leaveId` int(11) DEFAULT '0',
  `extCode` varchar(20) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agreetemp_extcode_1` (`templateId`),
  KEY `agreetemp_extcode_2` (`leaveId`),
  CONSTRAINT `agreetemp_extcode_2` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`),
  CONSTRAINT `agreetemp_extcode_3` FOREIGN KEY (`leaveId`) REFERENCES `leavereason` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40326 DEFAULT CHARSET=utf8;



# Dump of table agreetemp_saltypes
# ------------------------------------------------------------

CREATE TABLE `agreetemp_saltypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) DEFAULT '0',
  `stCode` int(11) DEFAULT '0',
  `salaryCode` varchar(20) NOT NULL DEFAULT '',
  `localLabel` varchar(50) NOT NULL DEFAULT '',
  `trackerId` int(11) NOT NULL DEFAULT '0',
  `trackerOperator` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor` decimal(5,2) NOT NULL DEFAULT '1.00',
  `trackerId2` int(11) NOT NULL DEFAULT '0',
  `trackerOperator2` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor2` decimal(5,2) NOT NULL DEFAULT '1.00',
  `trackerId3` int(11) NOT NULL DEFAULT '0',
  `trackerOperator3` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor3` decimal(5,2) NOT NULL DEFAULT '1.00',
  `requiresApproval` tinyint(1) NOT NULL DEFAULT '0',
  `approvedByDefault` tinyint(1) NOT NULL DEFAULT '0',
  `round` int(2) NOT NULL DEFAULT '0',
  `roundCeil` tinyint(1) NOT NULL DEFAULT '1',
  `costType` int(3) NOT NULL DEFAULT '0',
  `incInTotalCost` tinyint(1) NOT NULL DEFAULT '1',
  `salaryCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `minimumValue` decimal(5,2) NOT NULL DEFAULT '0.00',
  `maximumValue` decimal(5,2) NOT NULL DEFAULT '0.00',
  `transferToPayroll` tinyint(1) NOT NULL DEFAULT '1',
  `utReduction` decimal(5,2) NOT NULL DEFAULT '0.00',
  `roundingSalaryTypeId` int(11) unsigned NOT NULL DEFAULT '0',
  `allowManualAdd` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agreetemp_saltypes_1` (`templateId`),
  KEY `agreetemp_saltypes_2` (`stCode`),
  CONSTRAINT `agreetemp_saltypes_1` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1613003 DEFAULT CHARSET=utf8;



# Dump of table agresso_relation
# ------------------------------------------------------------

CREATE TABLE `agresso_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreignId` char(32) NOT NULL,
  `keyType` char(2) NOT NULL,
  `keyName` char(32) NOT NULL,
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '0000-00-00',
  `status` char(2) NOT NULL,
  `value` char(255) NOT NULL,
  `description` char(32) DEFAULT '',
  `restId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `foreignId` (`foreignId`,`keyType`,`keyName`,`toDate`)
) ENGINE=InnoDB AUTO_INCREMENT=284059899 DEFAULT CHARSET=utf8;



# Dump of table api_ip_ranges
# ------------------------------------------------------------

CREATE TABLE `api_ip_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qt_customerId` int(11) NOT NULL DEFAULT '0',
  `ipv4_start` decimal(20,0) NOT NULL DEFAULT '0',
  `ipv4_end` decimal(20,0) NOT NULL DEFAULT '0',
  `ipv4_delta` decimal(20,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `range_unique` (`ipv4_start`,`ipv4_end`,`qt_customerId`)
) ENGINE=InnoDB AUTO_INCREMENT=1717 DEFAULT CHARSET=utf8;



# Dump of table ats_config
# ------------------------------------------------------------

CREATE TABLE `ats_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qt_customerId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `language` int(11) NOT NULL DEFAULT '0',
  `model` varchar(255) NOT NULL,
  `sn` varchar(45) NOT NULL,
  `badgeLength` int(1) DEFAULT '8',
  `bioType` varchar(45) DEFAULT NULL,
  `proxType` varchar(45) DEFAULT NULL,
  `cardType` varchar(45) DEFAULT NULL,
  `displayType` varchar(45) DEFAULT NULL,
  `dldVersion` varchar(45) DEFAULT NULL,
  `configVersion` varchar(45) DEFAULT NULL,
  `flashVersion` varchar(45) DEFAULT NULL,
  `employeeVersion` varchar(45) DEFAULT NULL,
  `scheduleVersion` varchar(45) DEFAULT NULL,
  `appTablesVersion` varchar(45) DEFAULT NULL,
  `ucsMessage` varchar(45) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `row1` varchar(20) DEFAULT NULL,
  `row2` varchar(20) DEFAULT NULL,
  `row3` varchar(20) DEFAULT NULL,
  `row4` varchar(20) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `currentBuild` varchar(45) DEFAULT NULL,
  `server` varchar(45) NOT NULL,
  `lastVisit` datetime DEFAULT NULL,
  `smsWarningSent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sn_UNIQUE` (`sn`)
) ENGINE=InnoDB AUTO_INCREMENT=476 DEFAULT CHARSET=utf8;



# Dump of table ats_timepunch
# ------------------------------------------------------------

CREATE TABLE `ats_timepunch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sn` varchar(45) NOT NULL,
  `badge` varchar(64) NOT NULL,
  `punchtime` varchar(32) NOT NULL,
  `timepunch_id` int(11) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sn_badge_punchtime` (`sn`,`badge`,`punchtime`),
  KEY `timepunchid` (`timepunch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5165786 DEFAULT CHARSET=latin1;



# Dump of table ats_timepunch_uid
# ------------------------------------------------------------

CREATE TABLE `ats_timepunch_uid` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sn` varchar(45) NOT NULL,
  `uid` varchar(128) NOT NULL,
  `punchId` int(11) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sn_uid` (`sn`,`uid`),
  KEY `fk_punch_id_idx` (`punchId`)
) ENGINE=InnoDB AUTO_INCREMENT=417150 DEFAULT CHARSET=latin1;



# Dump of table avail_templ_shift
# ------------------------------------------------------------

CREATE TABLE `avail_templ_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL DEFAULT '0',
  `weekNo` int(2) NOT NULL DEFAULT '1',
  `weekDay` tinyint(1) NOT NULL DEFAULT '0',
  `availfrom` time NOT NULL DEFAULT '00:00:00',
  `availto` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  CONSTRAINT `avail_templ_shift_1` FOREIGN KEY (`templateId`) REFERENCES `avail_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19987 DEFAULT CHARSET=utf8;



# Dump of table avail_template
# ------------------------------------------------------------

CREATE TABLE `avail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0',
  `weeksAvail` int(2) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `avail_template_1` (`restId`),
  CONSTRAINT `avail_template_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2900 DEFAULT CHARSET=utf8;



# Dump of table availability
# ------------------------------------------------------------

CREATE TABLE `availability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreeId` int(11) NOT NULL DEFAULT '0',
  `weekNo` int(2) NOT NULL DEFAULT '1',
  `weekDay` tinyint(1) NOT NULL DEFAULT '0',
  `availfrom` time NOT NULL DEFAULT '00:00:00',
  `availto` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agreeId` (`agreeId`),
  CONSTRAINT `availability_1` FOREIGN KEY (`agreeId`) REFERENCES `agreement_v2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1383108 DEFAULT CHARSET=utf8;



# Dump of table availability_template
# ------------------------------------------------------------

CREATE TABLE `availability_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL DEFAULT '0',
  `weekNo` int(2) NOT NULL DEFAULT '1',
  `weekDay` tinyint(1) NOT NULL DEFAULT '0',
  `availfrom` time NOT NULL DEFAULT '00:00:00',
  `availto` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  CONSTRAINT `availability_template_1` FOREIGN KEY (`templateId`) REFERENCES `agreement_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=728428 DEFAULT CHARSET=utf8;



# Dump of table backup
# ------------------------------------------------------------

CREATE TABLE `backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `persId` int(11) NOT NULL,
  `backupFile` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=127654 DEFAULT CHARSET=utf8;



# Dump of table bankSignature
# ------------------------------------------------------------

CREATE TABLE `bankSignature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateSigned` datetime DEFAULT NULL,
  `signature` text,
  `ocspResponse` text,
  `userId` int(11) DEFAULT NULL,
  `signatureType` tinyint(2) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;



# Dump of table blacklist
# ------------------------------------------------------------

CREATE TABLE `blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `info` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



# Dump of table buggs
# ------------------------------------------------------------

CREATE TABLE `buggs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` char(10) NOT NULL,
  `buggType` int(2) NOT NULL DEFAULT '0',
  `buggStatus` int(2) NOT NULL DEFAULT '0',
  `section` int(2) NOT NULL DEFAULT '0',
  `security` int(1) NOT NULL DEFAULT '0',
  `severity` int(1) NOT NULL DEFAULT '0',
  `reproduced` int(1) NOT NULL DEFAULT '0',
  `discover` int(1) NOT NULL DEFAULT '0',
  `version` char(30) NOT NULL,
  `header` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `reproduce` text NOT NULL,
  `ownerQA` char(40) NOT NULL DEFAULT '',
  `resolvedBy` char(40) NOT NULL DEFAULT '',
  `fixedVersion` char(30) NOT NULL,
  `resolution` int(2) NOT NULL DEFAULT '0',
  `priority` int(2) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `persId` int(11) NOT NULL,
  `postedDate` date NOT NULL,
  `attachment` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;



# Dump of table buggs_comments
# ------------------------------------------------------------

CREATE TABLE `buggs_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buggId` int(11) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `persId` int(11) NOT NULL,
  `postedDate` date NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `buggs_comments_1` (`buggId`),
  CONSTRAINT `buggs_comments_1` FOREIGN KEY (`buggId`) REFERENCES `buggs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table chapters
# ------------------------------------------------------------

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRole` int(1) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `submodule_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table checklists
# ------------------------------------------------------------

CREATE TABLE `checklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL,
  `ts_edit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `frequency` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `groupId` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;



# Dump of table checklists_activity
# ------------------------------------------------------------

CREATE TABLE `checklists_activity` (
  `activityGroupId` int(11) NOT NULL,
  `subgroupId` int(11) DEFAULT NULL,
  `checklistId` int(11) DEFAULT NULL,
  KEY `checklistGroupId` (`activityGroupId`),
  KEY `activityGroupId` (`activityGroupId`),
  KEY `checklists_activity_ibfk_5` (`activityGroupId`),
  CONSTRAINT `checklists_activity_ibfk_5` FOREIGN KEY (`activityGroupId`) REFERENCES `activity` (`groupId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table checklists_item
# ------------------------------------------------------------

CREATE TABLE `checklists_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `method` text,
  `instrument` text,
  `frequency` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=492 DEFAULT CHARSET=utf8;



# Dump of table checklists_item_relation
# ------------------------------------------------------------

CREATE TABLE `checklists_item_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checklistId` int(11) NOT NULL,
  `checklistItemId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `checklistId` (`checklistId`),
  KEY `checklistItemId` (`checklistItemId`),
  CONSTRAINT `checklists_item_relation_ibfk_1` FOREIGN KEY (`checklistId`) REFERENCES `checklists` (`id`) ON DELETE CASCADE,
  CONSTRAINT `checklists_item_relation_ibfk_2` FOREIGN KEY (`checklistItemId`) REFERENCES `checklists_item` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=693 DEFAULT CHARSET=utf8;



# Dump of table client_ip_log
# ------------------------------------------------------------

CREATE TABLE `client_ip_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientIp` int(11) unsigned NOT NULL DEFAULT '0',
  `log_reference_id` int(11) unsigned NOT NULL DEFAULT '0',
  `log_reference_type` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `log_ref_key` (`log_reference_id`,`log_reference_type`)
) ENGINE=InnoDB AUTO_INCREMENT=288050501 DEFAULT CHARSET=utf8;



# Dump of table config
# ------------------------------------------------------------

CREATE TABLE `config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `customerId` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurantId` (`restaurantId`),
  KEY `customerId` (`customerId`),
  KEY `createdBy` (`createdBy`),
  CONSTRAINT `config_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `config_ibfk_2` FOREIGN KEY (`customerId`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `config_ibfk_3` FOREIGN KEY (`createdBy`) REFERENCES `employee` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=144962 DEFAULT CHARSET=utf8;



# Dump of table control_point
# ------------------------------------------------------------

CREATE TABLE `control_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `name` varchar(90) DEFAULT NULL,
  `activityGroupId` int(11) DEFAULT NULL,
  `subgroupId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `gfCpp` int(11) DEFAULT NULL,
  `instruction` varchar(4000) DEFAULT NULL,
  `requireConfirmation` tinyint(1) DEFAULT '0',
  `isTemplate` tinyint(1) DEFAULT NULL,
  `schemaType` int(11) DEFAULT NULL,
  `dailyLatest` tinyint(1) DEFAULT '0',
  `dailyLatestTime` time DEFAULT NULL,
  `weeklyInterval` int(11) DEFAULT NULL,
  `weeklyLatest` tinyint(1) DEFAULT '0',
  `weeklyLatestTime` time DEFAULT NULL,
  `weekdays` int(11) DEFAULT NULL,
  `months` int(11) DEFAULT NULL,
  `monthlyOrdinalNumber` int(11) DEFAULT NULL,
  `monthlyWeekday` int(11) DEFAULT NULL,
  `monthlyNonHoliday` tinyint(1) DEFAULT '1',
  `canBePerformed` tinyint(1) DEFAULT '0',
  `canBePerformedValue` int(11) DEFAULT NULL,
  `canBePerformedTimeUnit` int(11) DEFAULT NULL,
  `becomesDeviation` tinyint(1) DEFAULT '0',
  `becomesDeviationValue` int(11) DEFAULT NULL,
  `becomesDeviationTimeUnit` int(11) DEFAULT NULL,
  `submodule_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activitySubgroupRelation` (`activityGroupId`),
  KEY `groupId` (`groupId`),
  KEY `submodule_id` (`submodule_id`),
  CONSTRAINT `control_point_ibfk_1` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3306 DEFAULT CHARSET=utf8;



# Dump of table control_point_deviation
# ------------------------------------------------------------

CREATE TABLE `control_point_deviation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `measuredValue` varchar(255) DEFAULT NULL,
  `comment` text,
  `created` datetime NOT NULL,
  `eventId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `fieldId` int(11) DEFAULT NULL,
  `directSolutionId` int(11) DEFAULT NULL,
  `adjustmentSolutionId` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  `controlPointId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `users` int(11) unsigned DEFAULT NULL,
  `isStarted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `fieldId` (`fieldId`),
  KEY `directSolutionId` (`directSolutionId`),
  KEY `adjustmentSolutionId` (`adjustmentSolutionId`),
  KEY `controlPointId` (`controlPointId`),
  KEY `eventId` (`eventId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `users` (`users`),
  CONSTRAINT `control_point_deviation_ibfk_1` FOREIGN KEY (`fieldId`) REFERENCES `control_point_field` (`id`),
  CONSTRAINT `control_point_deviation_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `employee` (`id`),
  CONSTRAINT `control_point_deviation_ibfk_3` FOREIGN KEY (`directSolutionId`) REFERENCES `control_point_solution` (`id`),
  CONSTRAINT `control_point_deviation_ibfk_4` FOREIGN KEY (`adjustmentSolutionId`) REFERENCES `control_point_solution` (`id`),
  CONSTRAINT `control_point_deviation_ibfk_5` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`),
  CONSTRAINT `control_point_deviation_ibfk_6` FOREIGN KEY (`eventId`) REFERENCES `control_point_event` (`id`),
  CONSTRAINT `control_point_deviation_ibfk_7` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_deviation_ibfk_8` FOREIGN KEY (`users`) REFERENCES `control_point_employee_archive` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142215 DEFAULT CHARSET=utf8;



# Dump of table control_point_employee_archive
# ------------------------------------------------------------

CREATE TABLE `control_point_employee_archive` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) DEFAULT NULL,
  `controlPointListId` int(11) DEFAULT NULL,
  `employees` varchar(500) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurantId` (`restaurantId`),
  KEY `controlPointListId` (`controlPointListId`),
  CONSTRAINT `control_point_employee_archive_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_employee_archive_ibfk_2` FOREIGN KEY (`controlPointListId`) REFERENCES `control_point_list` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1097 DEFAULT CHARSET=utf8;



# Dump of table control_point_event
# ------------------------------------------------------------

CREATE TABLE `control_point_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controlPointId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `occasionId` int(11) DEFAULT NULL,
  `users` int(11) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `controlPointId` (`controlPointId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `occasionId` (`occasionId`),
  CONSTRAINT `control_point_event_ibfk_1` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_event_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_event_ibfk_3` FOREIGN KEY (`occasionId`) REFERENCES `control_point_occasion` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=174827 DEFAULT CHARSET=utf8;



# Dump of table control_point_event_archive
# ------------------------------------------------------------

CREATE TABLE `control_point_event_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controlPointId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `occasionId` int(11) DEFAULT NULL,
  `users` int(11) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `controlPointId` (`controlPointId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `occasionId` (`occasionId`),
  KEY `users` (`users`),
  CONSTRAINT `control_point_event_archive_ibfk_1` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`),
  CONSTRAINT `control_point_event_archive_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_event_archive_ibfk_3` FOREIGN KEY (`occasionId`) REFERENCES `control_point_occasion` (`id`) ON DELETE SET NULL,
  CONSTRAINT `control_point_event_archive_ibfk_4_new` FOREIGN KEY (`users`) REFERENCES `control_point_employee_archive` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=169129 DEFAULT CHARSET=utf8;



# Dump of table control_point_event_item
# ------------------------------------------------------------

CREATE TABLE `control_point_event_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `measuredValue` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `comment` text,
  `created` datetime NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `fieldId` int(11) DEFAULT NULL,
  `directSolutionId` int(11) DEFAULT NULL,
  `adjustmentSolutionId` int(11) DEFAULT NULL,
  `eventId` int(11) NOT NULL,
  `childId` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `fieldId` (`fieldId`),
  KEY `eventId` (`eventId`),
  KEY `directSolutionId` (`directSolutionId`),
  KEY `adjustmentSolutionId` (`adjustmentSolutionId`),
  KEY `groupId` (`groupId`),
  KEY `status` (`status`),
  CONSTRAINT `control_point_event_item_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `employee` (`id`) ON DELETE SET NULL,
  CONSTRAINT `control_point_event_item_ibfk_2` FOREIGN KEY (`fieldId`) REFERENCES `control_point_field` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_event_item_ibfk_4` FOREIGN KEY (`eventId`) REFERENCES `control_point_event` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_event_item_ibfk_5` FOREIGN KEY (`directSolutionId`) REFERENCES `control_point_solution` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_event_item_ibfk_6` FOREIGN KEY (`adjustmentSolutionId`) REFERENCES `control_point_solution` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=268373 DEFAULT CHARSET=utf8;



# Dump of table control_point_event_item_archive
# ------------------------------------------------------------

CREATE TABLE `control_point_event_item_archive` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `measuredValue` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `comment` text,
  `created` datetime NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `fieldId` int(11) DEFAULT NULL,
  `directSolutionId` int(11) DEFAULT NULL,
  `adjustmentSolutionId` int(11) DEFAULT NULL,
  `eventId` int(11) NOT NULL,
  `childId` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `fieldId` (`fieldId`),
  KEY `eventId` (`eventId`),
  KEY `directSolutionId` (`directSolutionId`),
  KEY `adjustmentSolutionId` (`adjustmentSolutionId`),
  KEY `groupId` (`groupId`),
  KEY `status` (`status`),
  CONSTRAINT `control_point_event_item_archive_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `employee` (`id`) ON DELETE SET NULL,
  CONSTRAINT `control_point_event_item_archive_ibfk_2` FOREIGN KEY (`fieldId`) REFERENCES `control_point_field` (`id`),
  CONSTRAINT `control_point_event_item_archive_ibfk_6` FOREIGN KEY (`eventId`) REFERENCES `control_point_event_archive` (`id`),
  CONSTRAINT `control_point_event_item_archive_ibfk_7` FOREIGN KEY (`directSolutionId`) REFERENCES `control_point_solution` (`id`),
  CONSTRAINT `control_point_event_item_archive_ibfk_8` FOREIGN KEY (`adjustmentSolutionId`) REFERENCES `control_point_solution` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=254450 DEFAULT CHARSET=utf8;



# Dump of table control_point_field
# ------------------------------------------------------------

CREATE TABLE `control_point_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controlPointId` int(11) NOT NULL,
  `fieldType` int(11) NOT NULL,
  `number1` int(11) DEFAULT NULL,
  `number2` int(11) DEFAULT NULL,
  `text` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `text2` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_control_point_field_control_point` (`controlPointId`),
  CONSTRAINT `fk_control_point_field_control_point` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9227 DEFAULT CHARSET=utf8;



# Dump of table control_point_list
# ------------------------------------------------------------

CREATE TABLE `control_point_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `description` varchar(160) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `submodule_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurantId` (`restaurantId`),
  KEY `submodule_id` (`submodule_id`),
  CONSTRAINT `control_point_list_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_list_ibfk_2` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=342 DEFAULT CHARSET=utf8;



# Dump of table control_point_list_control_points
# ------------------------------------------------------------

CREATE TABLE `control_point_list_control_points` (
  `controlPointListId` int(11) NOT NULL,
  `controlPointGroupId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `controlPointOrder` int(11) NOT NULL,
  PRIMARY KEY (`controlPointListId`,`controlPointGroupId`),
  KEY `fk_controlPointList` (`controlPointListId`),
  KEY `fk_controlPoint` (`controlPointGroupId`),
  CONSTRAINT `fk_controlPointList` FOREIGN KEY (`controlPointListId`) REFERENCES `control_point_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table control_point_list_employees
# ------------------------------------------------------------

CREATE TABLE `control_point_list_employees` (
  `controlPointListId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  PRIMARY KEY (`controlPointListId`,`employeeId`),
  KEY `fk_controlPointList1` (`controlPointListId`),
  KEY `fk_controlPointList_employee` (`employeeId`),
  CONSTRAINT `fk_controlPointList1` FOREIGN KEY (`controlPointListId`) REFERENCES `control_point_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_controlPointList_employee` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table control_point_occasion
# ------------------------------------------------------------

CREATE TABLE `control_point_occasion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controlPointId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `scheduleDate` datetime NOT NULL,
  `lateDate` datetime DEFAULT NULL,
  `deviationDate` datetime NOT NULL,
  `users` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `controlPointId` (`controlPointId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `scheduleDate` (`scheduleDate`),
  KEY `lateDate` (`lateDate`),
  KEY `deviationDate` (`deviationDate`),
  CONSTRAINT `control_point_occasion_ibfk_1` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`) ON DELETE CASCADE,
  CONSTRAINT `control_point_occasion_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=168832 DEFAULT CHARSET=utf8;



# Dump of table control_point_occasion_created
# ------------------------------------------------------------

CREATE TABLE `control_point_occasion_created` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `restaurantId` (`restaurantId`,`created`),
  CONSTRAINT `control_point_occasion_created_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=221416 DEFAULT CHARSET=utf8;



# Dump of table control_point_routines
# ------------------------------------------------------------

CREATE TABLE `control_point_routines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controlPointId` int(11) NOT NULL,
  `routineGroupId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_routines_control_point` (`controlPointId`),
  KEY `routineGroupId` (`routineGroupId`),
  CONSTRAINT `fk_routines_control_point` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4367 DEFAULT CHARSET=utf8;



# Dump of table control_point_solution
# ------------------------------------------------------------

CREATE TABLE `control_point_solution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controlPointId` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `fieldType` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_control_point_solution_control_point` (`controlPointId`),
  CONSTRAINT `fk_control_point_solution_control_point` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11487 DEFAULT CHARSET=utf8;



# Dump of table control_point_status
# ------------------------------------------------------------

CREATE TABLE `control_point_status` (
  `controlPointId` int(11) NOT NULL,
  `controlPointGroupId` int(11) DEFAULT NULL,
  `restaurantId` int(11) NOT NULL,
  `lastChangeTimestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`controlPointId`,`restaurantId`),
  KEY `fk_control_point_status_control_point` (`controlPointId`),
  KEY `fk_control_point_status_restaurant` (`restaurantId`),
  KEY `controlPointGroupId` (`controlPointGroupId`),
  CONSTRAINT `fk_control_point_status_control_point` FOREIGN KEY (`controlPointId`) REFERENCES `control_point` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_control_point_status_restaurant` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table country_email
# ------------------------------------------------------------

CREATE TABLE `country_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT 'sales@quinyx.com',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=latin1;



# Dump of table crm_customer
# ------------------------------------------------------------

CREATE TABLE `crm_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerNo` varchar(20) NOT NULL,
  `givenName` varchar(40) NOT NULL,
  `familyName` varchar(40) NOT NULL,
  `address1` varchar(50) NOT NULL DEFAULT '',
  `address2` varchar(50) NOT NULL DEFAULT '',
  `zip` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `cellPhone` varchar(20) NOT NULL,
  `phoneNo` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `socsecNo` varchar(30) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL DEFAULT '',
  `canLogin` tinyint(1) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `groupId` int(11) NOT NULL DEFAULT '0',
  `pictureURL` varchar(50) NOT NULL DEFAULT 'portraits/no_picture.jpg',
  `customerCat` int(11) NOT NULL DEFAULT '0',
  `workLeader` int(11) NOT NULL DEFAULT '0',
  `customerIsWorkLeader` tinyint(1) NOT NULL DEFAULT '0',
  `accountManager` int(11) NOT NULL DEFAULT '0',
  `payrollManager` int(11) NOT NULL DEFAULT '0',
  `passive` tinyint(1) NOT NULL DEFAULT '0',
  `routeDescription` text NOT NULL,
  `accessCode` varchar(20) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  `keyGiven` tinyint(1) NOT NULL DEFAULT '0',
  `createdByForceBy` int(11) NOT NULL DEFAULT '0',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '0000-00-00',
  `agreeTemplateId` int(11) NOT NULL DEFAULT '0',
  `externalReference` varchar(50) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customerNo` (`customerNo`),
  KEY `restId` (`restId`),
  KEY `email` (`email`),
  KEY `nameSearch` (`familyName`,`givenName`),
  KEY `groupIdIndex` (`groupId`),
  KEY `createdByForceBy` (`createdByForceBy`),
  KEY `customerCat` (`customerCat`),
  KEY `workLeader` (`workLeader`),
  KEY `accountManager` (`accountManager`),
  KEY `payrollManager` (`payrollManager`),
  CONSTRAINT `crm_customer_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10076 DEFAULT CHARSET=utf8;



# Dump of table crm_customer_approvers
# ------------------------------------------------------------

CREATE TABLE `crm_customer_approvers` (
  `customerId` int(11) NOT NULL DEFAULT '0',
  `employeeId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerId`,`employeeId`),
  KEY `customerId` (`customerId`),
  KEY `employeeId` (`employeeId`),
  CONSTRAINT `crm_customer_approvers_ibfk_1` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `crm_customer_approvers_ibfk_2` FOREIGN KEY (`customerId`) REFERENCES `crm_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table crm_customer_team
# ------------------------------------------------------------

CREATE TABLE `crm_customer_team` (
  `customerId` int(11) NOT NULL DEFAULT '0',
  `teamId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerId`,`teamId`),
  KEY `customerId` (`customerId`),
  KEY `teamId` (`teamId`),
  CONSTRAINT `crm_customer_team_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `crm_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table crm_decision
# ------------------------------------------------------------

CREATE TABLE `crm_decision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL DEFAULT '0',
  `decisionNo` varchar(20) NOT NULL,
  `begDate` date NOT NULL,
  `endDate` date NOT NULL,
  `expires` tinyint(1) NOT NULL DEFAULT '1',
  `decisionTypeId` int(11) NOT NULL DEFAULT '0',
  `hoursType_1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hoursType_2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hoursType_3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `periodType` tinyint(1) NOT NULL DEFAULT '0',
  `decisionDate` date NOT NULL,
  `reportFrom` date NOT NULL,
  `periodInterval` int(2) NOT NULL DEFAULT '3',
  `increasedFee` tinyint(1) NOT NULL DEFAULT '0',
  `hourlyFee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `info` text NOT NULL,
  `useFixSchedules` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customerId` (`customerId`),
  CONSTRAINT `crm_decision_1` FOREIGN KEY (`customerId`) REFERENCES `crm_customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42436 DEFAULT CHARSET=utf8;



# Dump of table crm_parameters
# ------------------------------------------------------------

CREATE TABLE `crm_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `onCallFactor` decimal(10,2) NOT NULL DEFAULT '4.00',
  `standbyFactor` decimal(10,2) NOT NULL DEFAULT '7.00',
  `limitEmpView` tinyint(1) NOT NULL DEFAULT '1',
  `mandatoryDecision` tinyint(1) NOT NULL DEFAULT '1',
  `approvalWarning` tinyint(1) NOT NULL DEFAULT '0',
  `mandatoryEmployeeMapping` tinyint(1) NOT NULL DEFAULT '1',
  `canUseFixSchedules` tinyint(1) NOT NULL DEFAULT '0',
  `sendAutomaticallyToFK` tinyint(1) NOT NULL DEFAULT '0',
  `xmlCreatedNotifyAssistant` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  CONSTRAINT `crm_parameters_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;



# Dump of table crm_schedule
# ------------------------------------------------------------

CREATE TABLE `crm_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `decisionId` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `weeks` int(2) NOT NULL DEFAULT '0',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `expires` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `comment` text NOT NULL,
  `lastDate` date NOT NULL DEFAULT '0000-00-00',
  `weekBased` tinyint(1) NOT NULL DEFAULT '1',
  `publishedTo` date NOT NULL DEFAULT '0000-00-00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `decisionId` (`decisionId`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=47555 DEFAULT CHARSET=utf8;



# Dump of table crm_schedule_shift
# ------------------------------------------------------------

CREATE TABLE `crm_schedule_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crmSchedId` int(11) NOT NULL,
  `persId` int(11) NOT NULL DEFAULT '0',
  `weekNo` int(2) NOT NULL,
  `weekDay` int(1) NOT NULL,
  `weekDayAlt` int(1) NOT NULL,
  `begTime` time NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL DEFAULT '00:00:00',
  `endBreak` time NOT NULL DEFAULT '00:00:00',
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `section` int(11) NOT NULL DEFAULT '1',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `countScheduleHours` tinyint(1) NOT NULL DEFAULT '1',
  `countPunchHours` tinyint(1) NOT NULL DEFAULT '1',
  `restId` int(11) NOT NULL DEFAULT '0',
  `manned` tinyint(1) NOT NULL DEFAULT '1',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId` (`persId`),
  KEY `restId` (`restId`),
  KEY `categoryId` (`categoryId`),
  KEY `crmSchedId_2` (`crmSchedId`,`weekNo`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  CONSTRAINT `crm_schedule_shift_1` FOREIGN KEY (`crmSchedId`) REFERENCES `crm_schedule` (`id`),
  CONSTRAINT `crm_schedule_shift_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `crm_schedule_shift_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4195838 DEFAULT CHARSET=utf8;



# Dump of table crm_schedule_task
# ------------------------------------------------------------

CREATE TABLE `crm_schedule_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `crm_schedule_task_1` (`scheduleId`),
  KEY `crm_schedule_task_2` (`categoryId`),
  CONSTRAINT `crm_schedule_task_1` FOREIGN KEY (`scheduleId`) REFERENCES `crm_schedule_shift` (`id`),
  CONSTRAINT `crm_schedule_task_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;



# Dump of table crm_timereport
# ------------------------------------------------------------

CREATE TABLE `crm_timereport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UUID` varchar(36) NOT NULL DEFAULT '',
  `decisionId` int(11) NOT NULL DEFAULT '0',
  `assistantId` int(11) NOT NULL DEFAULT '0',
  `managerId` int(11) DEFAULT NULL,
  `reportType` int(3) NOT NULL DEFAULT '0',
  `period` varchar(7) NOT NULL DEFAULT '',
  `fromDate` date NOT NULL,
  `toDate` date NOT NULL,
  `hoursActive` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hoursOnCall` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hoursStandBy` decimal(10,2) NOT NULL DEFAULT '0.00',
  `dateCreated` date NOT NULL,
  `createdById` int(11) NOT NULL DEFAULT '0',
  `dateSentToFK` date DEFAULT NULL,
  `sentById` int(11) DEFAULT NULL,
  `status` int(3) NOT NULL DEFAULT '0',
  `responseFK` text,
  `idFK` varchar(36) NOT NULL DEFAULT '',
  `timeReportXML` text,
  `checksum` varchar(255) NOT NULL DEFAULT '',
  `assistantSignId` int(11) NOT NULL DEFAULT '0',
  `managerSignId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `decisionId` (`decisionId`),
  KEY `assistantId` (`assistantId`),
  KEY `UUID` (`UUID`),
  CONSTRAINT `fk_crm_timereport_1` FOREIGN KEY (`decisionId`) REFERENCES `crm_decision` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_crm_timereport_2` FOREIGN KEY (`assistantId`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;



# Dump of table crm_timereport_line
# ------------------------------------------------------------

CREATE TABLE `crm_timereport_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeReportId` int(11) NOT NULL DEFAULT '0',
  `timepunchId` int(11) NOT NULL DEFAULT '0',
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `type` int(3) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `timeReportId` (`timeReportId`),
  KEY `timepunchId` (`timepunchId`),
  CONSTRAINT `fk_crm_timereport_line_1` FOREIGN KEY (`timeReportId`) REFERENCES `crm_timereport` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_crm_timereport_line_2` FOREIGN KEY (`timepunchId`) REFERENCES `timepunch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2304 DEFAULT CHARSET=utf8;



# Dump of table customer
# ------------------------------------------------------------

CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `contact` varchar(80) NOT NULL,
  `address1` varchar(50) NOT NULL DEFAULT '',
  `address2` varchar(50) NOT NULL DEFAULT '',
  `zip` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `phoneNo` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `accountManager` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `groupId` int(11) NOT NULL DEFAULT '0',
  `customerCategoryId` int(11) NOT NULL DEFAULT '0',
  `info` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `restId` (`restId`),
  KEY `customer_3` (`accountManager`),
  KEY `customer_4` (`customerCategoryId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `customer_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `customer_4` FOREIGN KEY (`customerCategoryId`) REFERENCES `customercategory` (`id`),
  CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=577 DEFAULT CHARSET=utf8;



# Dump of table customer_locales
# ------------------------------------------------------------

CREATE TABLE `customer_locales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `locale` varchar(30) NOT NULL DEFAULT '',
  `value` text,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `language` (`locale`),
  KEY `customer_id` (`customer_id`),
  KEY `user_id` (`user_id`),
  KEY `locale_id` (`locale_id`),
  CONSTRAINT `customer_locales_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `customer_locales_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `employee` (`id`) ON DELETE SET NULL,
  CONSTRAINT `customer_locales_ibfk_3` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34482 DEFAULT CHARSET=utf8;



# Dump of table customer_staff
# ------------------------------------------------------------

CREATE TABLE `customer_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customerId` (`customerId`),
  KEY `employeeId` (`employeeId`),
  CONSTRAINT `customer_staff_1` FOREIGN KEY (`customerId`) REFERENCES `crm_customer` (`id`),
  CONSTRAINT `customer_staff_2` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90806735 DEFAULT CHARSET=utf8;



# Dump of table customercategory
# ------------------------------------------------------------

CREATE TABLE `customercategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `objectType` int(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customercategory_2` (`restId`),
  CONSTRAINT `customercategory_2` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=385 DEFAULT CHARSET=utf8;



# Dump of table daycountgroup
# ------------------------------------------------------------

CREATE TABLE `daycountgroup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `excludeDaysBetweenApplications` tinyint(1) NOT NULL DEFAULT '0',
  `countSicDaysOnly` tinyint(1) NOT NULL DEFAULT '0',
  `qualDayBuffer` int(3) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;



# Dump of table debuglog
# ------------------------------------------------------------

CREATE TABLE `debuglog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=960603 DEFAULT CHARSET=utf8;



# Dump of table deleted_scheduletask
# ------------------------------------------------------------

CREATE TABLE `deleted_scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `work_scheduletask_1` (`scheduleId`),
  KEY `work_scheduletask_2` (`categoryId`),
  CONSTRAINT `deleted_scheduletask_1` FOREIGN KEY (`scheduleId`) REFERENCES `schedule_deleted` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=287910 DEFAULT CHARSET=utf8;



# Dump of table district
# ------------------------------------------------------------

CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `qt_customerId` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `managerId` int(11) DEFAULT NULL,
  `managerGroupId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qt_customerId` (`qt_customerId`),
  KEY `managerId` (`managerId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `district_2` FOREIGN KEY (`qt_customerId`) REFERENCES `qt_customer` (`id`),
  CONSTRAINT `district_ibfk_1` FOREIGN KEY (`managerId`) REFERENCES `employee` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2511 DEFAULT CHARSET=utf8;



# Dump of table document
# ------------------------------------------------------------

CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `typeName` varchar(30) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `isTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `isGlobal` tinyint(1) NOT NULL DEFAULT '0',
  `requireConfirmation` tinyint(1) DEFAULT '0',
  `submodule_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_document_document_types1` (`typeName`),
  KEY `groupId` (`groupId`),
  KEY `name` (`name`),
  KEY `submodule_id` (`submodule_id`),
  CONSTRAINT `document_ibfk_1` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1632 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table document_activity
# ------------------------------------------------------------

CREATE TABLE `document_activity` (
  `documentId` int(11) NOT NULL,
  `activityGroupId` int(11) NOT NULL,
  PRIMARY KEY (`documentId`,`activityGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table document_activity_flowstep
# ------------------------------------------------------------

CREATE TABLE `document_activity_flowstep` (
  `activityGroupId` int(11) NOT NULL,
  `predefinedFlowstepId` int(11) NOT NULL,
  `documentId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`documentId`,`activityGroupId`,`predefinedFlowstepId`),
  KEY `fk_document_activity_flowstep_activity` (`activityGroupId`),
  KEY `fk_document_activity_flowstep_predefined_flowstep` (`predefinedFlowstepId`),
  CONSTRAINT `fk_document_activity_flowstep_activity` FOREIGN KEY (`activityGroupId`) REFERENCES `activity` (`groupId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_document_activity_flowstep_predefined_flowstep` FOREIGN KEY (`predefinedFlowstepId`) REFERENCES `predefined_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table document_activity_subgroup
# ------------------------------------------------------------

CREATE TABLE `document_activity_subgroup` (
  `activityGroupId` int(11) NOT NULL,
  `subgroupId` int(11) NOT NULL,
  `documentId` int(11) NOT NULL,
  PRIMARY KEY (`documentId`,`activityGroupId`,`subgroupId`),
  KEY `subgroupId` (`subgroupId`),
  CONSTRAINT `document_activity_subgroup_ibfk_1` FOREIGN KEY (`subgroupId`) REFERENCES `activity_subgroup` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table document_field
# ------------------------------------------------------------

CREATE TABLE `document_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documentId` int(11) NOT NULL,
  `typeName` varchar(50) COLLATE utf8_bin NOT NULL,
  `value` mediumtext COLLATE utf8_bin,
  `sortorder` int(11) NOT NULL DEFAULT '100',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1',
  `isDraggable` tinyint(4) NOT NULL DEFAULT '1',
  `isLocal` tinyint(1) NOT NULL DEFAULT '0',
  `origin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_document_fields_document1` (`documentId`),
  CONSTRAINT `fk_document_fields_document1` FOREIGN KEY (`documentId`) REFERENCES `document` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9745 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table document_order
# ------------------------------------------------------------

CREATE TABLE `document_order` (
  `groupId` int(11) NOT NULL,
  `orderNr` int(11) NOT NULL,
  UNIQUE KEY `groupId` (`groupId`,`orderNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table documentfolder
# ------------------------------------------------------------

CREATE TABLE `documentfolder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `description` text,
  `publishDate` datetime NOT NULL,
  `employeeId` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  `customerCategoryId` int(11) DEFAULT NULL,
  `folderTypeId` int(11) DEFAULT '0',
  `selectiveEmployeeId` text,
  `createdBy` int(11) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdTs` timestamp NULL DEFAULT NULL,
  `editableByAllAdmin` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=902 DEFAULT CHARSET=utf8;



# Dump of table documentFolderAdmins
# ------------------------------------------------------------

CREATE TABLE `documentFolderAdmins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documentFolderId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1049 DEFAULT CHARSET=utf8;



# Dump of table emp_pods
# ------------------------------------------------------------

CREATE TABLE `emp_pods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `todo_pods` varchar(255) DEFAULT NULL,
  `user_portal_pods` varchar(255) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId` (`persId`),
  CONSTRAINT `emp_pods_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35019 DEFAULT CHARSET=utf8;



# Dump of table emp_report
# ------------------------------------------------------------

CREATE TABLE `emp_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) NOT NULL,
  `isGlobal` tinyint(1) NOT NULL DEFAULT '0',
  `locale` varchar(15) NOT NULL DEFAULT 'en_US',
  `sortBy` int(3) NOT NULL DEFAULT '45',
  `sortBy2` int(3) NOT NULL DEFAULT '0',
  `sortBy3` int(3) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `emp_report_1` (`restId`),
  KEY `isGlobal` (`isGlobal`,`locale`),
  CONSTRAINT `emp_report_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1932 DEFAULT CHARSET=utf8;



# Dump of table emp_report_item
# ------------------------------------------------------------

CREATE TABLE `emp_report_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `fieldId` int(3) NOT NULL,
  `heading` varchar(100) NOT NULL,
  `columnOrder` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `reportId` (`reportId`,`columnOrder`),
  CONSTRAINT `emp_report_item_1` FOREIGN KEY (`reportId`) REFERENCES `emp_report` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41474 DEFAULT CHARSET=utf8;



# Dump of table empgroup
# ------------------------------------------------------------

CREATE TABLE `empgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `groupId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `managerId` int(11) NOT NULL DEFAULT '0',
  `extMail` varchar(50) NOT NULL DEFAULT '',
  `accessForAll` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `extMail` (`extMail`),
  KEY `groupId` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=24440 DEFAULT CHARSET=utf8;



# Dump of table empgroup_access
# ------------------------------------------------------------

CREATE TABLE `empgroup_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `groupSort` (`groupId`,`employeeId`),
  KEY `empgroup_access_2` (`employeeId`),
  CONSTRAINT `empgroup_access_1` FOREIGN KEY (`groupId`) REFERENCES `empgroup` (`id`),
  CONSTRAINT `empgroup_access_2` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2509 DEFAULT CHARSET=utf8;



# Dump of table empgroup_member
# ------------------------------------------------------------

CREATE TABLE `empgroup_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `groupSort` (`groupId`,`employeeId`),
  KEY `empgroup_member_2` (`employeeId`),
  CONSTRAINT `empgroup_member_1` FOREIGN KEY (`groupId`) REFERENCES `empgroup` (`id`),
  CONSTRAINT `empgroup_member_2` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=223879705 DEFAULT CHARSET=utf8;



# Dump of table employee
# ------------------------------------------------------------

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `badgeNo` varchar(20) NOT NULL,
  `givenName` varchar(40) NOT NULL,
  `familyName` varchar(40) NOT NULL,
  `address1` varchar(50) NOT NULL DEFAULT '',
  `address2` varchar(50) NOT NULL DEFAULT '',
  `zip` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `cellPhone` varchar(20) NOT NULL,
  `phoneNo` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `extMail` varchar(50) NOT NULL DEFAULT '',
  `useSMS` tinyint(1) NOT NULL DEFAULT '1',
  `useMail` tinyint(1) NOT NULL DEFAULT '1',
  `useQmail` tinyint(1) NOT NULL DEFAULT '0',
  `copyToMail` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `readRights` tinyint(1) NOT NULL DEFAULT '0',
  `localAdminRights` tinyint(1) NOT NULL DEFAULT '0',
  `adminRights` tinyint(1) NOT NULL DEFAULT '0',
  `accountManager` tinyint(1) NOT NULL DEFAULT '0',
  `districtManager` tinyint(4) NOT NULL DEFAULT '0',
  `sectionManager` tinyint(1) NOT NULL DEFAULT '0',
  `corpRights` tinyint(1) NOT NULL DEFAULT '0',
  `canUseAdminPortal` tinyint(1) NOT NULL DEFAULT '1',
  `leaveDate` date NOT NULL DEFAULT '0000-00-00',
  `passwordExpireDate` date NOT NULL DEFAULT '0000-00-00',
  `resetPassword` int(1) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '',
  `passwordTandA` varchar(50) NOT NULL DEFAULT '',
  `restId` int(11) NOT NULL DEFAULT '0',
  `groupId` int(11) NOT NULL DEFAULT '0',
  `logedOn` tinyint(1) NOT NULL DEFAULT '0',
  `pictureURL` varchar(50) NOT NULL DEFAULT 'portraits/no_picture.jpg',
  `staffCat` int(11) NOT NULL DEFAULT '0',
  `section` int(11) NOT NULL DEFAULT '1',
  `passive` tinyint(1) NOT NULL DEFAULT '0',
  `info` text NOT NULL,
  `dateOfBirth` date NOT NULL DEFAULT '0000-00-00',
  `employedDate` date NOT NULL DEFAULT '0000-00-00',
  `cardNo` varchar(20) NOT NULL,
  `socsecNo` varchar(30) NOT NULL DEFAULT '',
  `nextOfKind` varchar(50) NOT NULL,
  `nextPhone` varchar(50) NOT NULL DEFAULT '',
  `rank` int(3) NOT NULL DEFAULT '50',
  `autoMan` tinyint(1) NOT NULL DEFAULT '1',
  `isMonitor` tinyint(1) NOT NULL DEFAULT '0',
  `mobi` varchar(50) DEFAULT NULL,
  `shareAble` tinyint(1) NOT NULL DEFAULT '1',
  `createdByForceBy` int(11) NOT NULL DEFAULT '0',
  `latestVisit` datetime NOT NULL,
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `role` int(2) NOT NULL DEFAULT '0',
  `loginId` varchar(100) NOT NULL DEFAULT '',
  `reportingTo` int(11) NOT NULL DEFAULT '0',
  `sharedScheduling` tinyint(1) NOT NULL DEFAULT '0',
  `cleanCellPhone` varchar(20) NOT NULL DEFAULT '',
  `punchType` tinyint(1) NOT NULL DEFAULT '0',
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `myDistrict` int(11) DEFAULT NULL,
  `myFilterShiftType` int(11) NOT NULL DEFAULT '0',
  `trials` int(1) NOT NULL DEFAULT '3',
  `inactiveUntil` datetime DEFAULT NULL,
  `updatelock` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allowAttestOfOwnPunches` tinyint(1) NOT NULL DEFAULT '1',
  `hasBankId` tinyint(1) DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `badgeNo` (`badgeNo`),
  KEY `email` (`email`),
  KEY `extMail` (`extMail`),
  KEY `nameSearch` (`familyName`,`givenName`),
  KEY `groupIdIndex` (`groupId`),
  KEY `createdByForceBy` (`createdByForceBy`),
  KEY `loginId` (`loginId`),
  KEY `reportingTo` (`reportingTo`),
  KEY `staffCat` (`staffCat`),
  KEY `cleanCellPhone` (`cleanCellPhone`),
  KEY `restId_2` (`restId`,`ts`),
  KEY `section_idx` (`section`) USING BTREE,
  KEY `USER` (`userId`),
  KEY `cardNo` (`cardNo`),
  KEY `employee_3` (`myDistrict`),
  CONSTRAINT `employee_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `employee_2` FOREIGN KEY (`section`) REFERENCES `section` (`id`),
  CONSTRAINT `employee_3` FOREIGN KEY (`myDistrict`) REFERENCES `district` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=574675 DEFAULT CHARSET=utf8;



# Dump of table employee_changes_log
# ------------------------------------------------------------

CREATE TABLE `employee_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=36790263 DEFAULT CHARSET=latin1;



# Dump of table employee_deletes_log
# ------------------------------------------------------------

CREATE TABLE `employee_deletes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `qt_custId` int(11) NOT NULL,
  `objectName` varchar(128) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=787207 DEFAULT CHARSET=latin1;



# Dump of table employee_document_status
# ------------------------------------------------------------

CREATE TABLE `employee_document_status` (
  `employeeId` int(11) NOT NULL,
  `documentId` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `documentGroupId` int(11) DEFAULT NULL,
  `majorRevision` int(3) DEFAULT NULL,
  `minorRevision` int(3) DEFAULT NULL,
  PRIMARY KEY (`employeeId`,`documentId`,`status`),
  KEY `employeeId` (`employeeId`),
  CONSTRAINT `employee_document_status_ibfk_1` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table employee_summary_log
# ------------------------------------------------------------

CREATE TABLE `employee_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=25300424 DEFAULT CHARSET=latin1;



# Dump of table employee_units
# ------------------------------------------------------------

CREATE TABLE `employee_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `rollOutShiftEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `employeeId` (`employeeId`),
  KEY `restId` (`restId`),
  CONSTRAINT `employee_units_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `employee_units_2` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23781475 DEFAULT CHARSET=utf8;



# Dump of table environment
# ------------------------------------------------------------

CREATE TABLE `environment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `baseUrl` varchar(255) DEFAULT NULL,
  `display` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;



# Dump of table environment_oauth_customer
# ------------------------------------------------------------

CREATE TABLE `environment_oauth_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `environmentId` int(11) NOT NULL,
  `qt_customerId` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `loginUrl` varchar(1024) DEFAULT NULL,
  `redirectUrl` varchar(255) DEFAULT NULL,
  `authCodeParameter` varchar(45) DEFAULT NULL,
  `display` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_environment_oauth_customer_1_idx` (`environmentId`),
  CONSTRAINT `fk_environment_oauth_customer_1` FOREIGN KEY (`environmentId`) REFERENCES `environment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;



# Dump of table faq
# ------------------------------------------------------------

CREATE TABLE `faq` (
  `faqId` int(11) NOT NULL AUTO_INCREMENT,
  `faqNo` int(11) NOT NULL,
  `restId` int(11) NOT NULL DEFAULT '1',
  `faqLanguage` char(5) NOT NULL DEFAULT 'en_US',
  `faqQuestion` char(100) NOT NULL DEFAULT '',
  `faqAnswer` text,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`faqId`),
  KEY `faqNo` (`faqNo`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=1025 DEFAULT CHARSET=utf8;



# Dump of table fc_budget
# ------------------------------------------------------------

CREATE TABLE `fc_budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `section` int(11) NOT NULL DEFAULT '0',
  `year` year(4) NOT NULL DEFAULT '2009',
  `version` tinyint(1) NOT NULL DEFAULT '0',
  `variableType` int(2) NOT NULL DEFAULT '0',
  `budget01` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget02` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget03` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget04` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget05` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget06` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget07` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget08` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget09` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget10` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget11` decimal(10,2) NOT NULL DEFAULT '0.00',
  `budget12` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fc_budget_1` (`restId`),
  KEY `fc_budget_2` (`variableType`),
  CONSTRAINT `fc_budget_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `fc_budget_2` FOREIGN KEY (`variableType`) REFERENCES `fc_variable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9461 DEFAULT CHARSET=utf8;



# Dump of table fc_variable
# ------------------------------------------------------------

CREATE TABLE `fc_variable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `isSales` tinyint(1) NOT NULL DEFAULT '0',
  `isTransactions` tinyint(1) NOT NULL DEFAULT '0',
  `isWorkedHours` tinyint(1) NOT NULL DEFAULT '0',
  `isCalculated` tinyint(1) NOT NULL DEFAULT '0',
  `variable1` int(11) NOT NULL DEFAULT '0',
  `operator` char(1) NOT NULL DEFAULT '+',
  `variable2` int(11) NOT NULL DEFAULT '0',
  `dispDecimals` tinyint(1) NOT NULL DEFAULT '0',
  `calcFromSales` tinyint(1) NOT NULL DEFAULT '0',
  `useErlangC` tinyint(1) NOT NULL DEFAULT '0',
  `erlServiceLevel` int(2) NOT NULL DEFAULT '80',
  `erlAnswerTime` int(3) NOT NULL DEFAULT '15',
  `erlAvgDuration` decimal(6,2) NOT NULL DEFAULT '4.00',
  `fcVariableType` int(2) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `variableType` (`fcVariableType`)
) ENGINE=InnoDB AUTO_INCREMENT=4424 DEFAULT CHARSET=utf8;



# Dump of table file_activities
# ------------------------------------------------------------

CREATE TABLE `file_activities` (
  `fileId` int(11) DEFAULT NULL,
  `activityGroupId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table files
# ------------------------------------------------------------

CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) DEFAULT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `employeeId` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `extension` varchar(100) DEFAULT NULL,
  `uniqueName` varchar(250) DEFAULT NULL,
  `sizeInKiloBytes` int(11) DEFAULT NULL,
  `uploaded` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;



# Dump of table fix_schedule
# ------------------------------------------------------------

CREATE TABLE `fix_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `weeks` int(2) NOT NULL DEFAULT '0',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `expires` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `comment` text NOT NULL,
  `lastDate` date NOT NULL DEFAULT '0000-00-00',
  `weekBased` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId` (`persId`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=342688 DEFAULT CHARSET=utf8;



# Dump of table fix_schedule_shift
# ------------------------------------------------------------

CREATE TABLE `fix_schedule_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fSchedId` int(11) NOT NULL,
  `weekNo` int(2) NOT NULL,
  `weekDay` int(1) NOT NULL,
  `weekDayAlt` int(1) NOT NULL,
  `begTime` time NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL DEFAULT '00:00:00',
  `endBreak` time NOT NULL DEFAULT '00:00:00',
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `section` int(11) NOT NULL DEFAULT '1',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `restId` int(11) NOT NULL DEFAULT '0',
  `manned` tinyint(1) NOT NULL DEFAULT '1',
  `onVacation` tinyint(1) NOT NULL DEFAULT '0',
  `leaveId` int(11) NOT NULL DEFAULT '0',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `countScheduleHours` tinyint(1) NOT NULL DEFAULT '1',
  `countPunchHours` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fSchedId` (`fSchedId`),
  KEY `restId` (`restId`),
  KEY `categoryId` (`categoryId`),
  KEY `fSchedId_2` (`fSchedId`,`weekNo`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `crmSchedId` (`crmSchedId`),
  CONSTRAINT `fix_schedule_shift_1` FOREIGN KEY (`fSchedId`) REFERENCES `fix_schedule` (`id`),
  CONSTRAINT `fix_schedule_shift_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fix_schedule_shift_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=19316763 DEFAULT CHARSET=utf8;



# Dump of table fix_scheduletask
# ------------------------------------------------------------

CREATE TABLE `fix_scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fix_scheduletask_1` (`scheduleId`),
  KEY `fix_scheduletask_2` (`categoryId`),
  CONSTRAINT `fix_scheduletask_1` FOREIGN KEY (`scheduleId`) REFERENCES `fix_schedule_shift` (`id`),
  CONSTRAINT `fix_scheduletask_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=806923 DEFAULT CHARSET=utf8;



# Dump of table fixed_report_presets
# ------------------------------------------------------------

CREATE TABLE `fixed_report_presets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `author` int(11) NOT NULL,
  `report_type` varchar(64) NOT NULL DEFAULT '',
  `preset_data` text NOT NULL,
  `shared_with_unit` int(11) DEFAULT NULL,
  `shared_with_district` int(11) DEFAULT NULL,
  `shared_with_customer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`),
  KEY `shared_with_unit` (`shared_with_unit`),
  KEY `shared_with_district` (`shared_with_district`),
  KEY `shared_with_customer` (`shared_with_customer`),
  CONSTRAINT `fixed_report_presets_ibfk_1` FOREIGN KEY (`author`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fixed_report_presets_ibfk_2` FOREIGN KEY (`shared_with_unit`) REFERENCES `restaurants` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fixed_report_presets_ibfk_3` FOREIGN KEY (`shared_with_district`) REFERENCES `district` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fixed_report_presets_ibfk_4` FOREIGN KEY (`shared_with_customer`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;



# Dump of table fixed_report_presets_defaults
# ------------------------------------------------------------

CREATE TABLE `fixed_report_presets_defaults` (
  `report_type` varchar(64) NOT NULL DEFAULT '',
  `employee_id` int(11) NOT NULL,
  `default_preset` int(11) unsigned NOT NULL,
  PRIMARY KEY (`report_type`,`employee_id`),
  KEY `employee_id` (`employee_id`),
  KEY `default_preset` (`default_preset`),
  CONSTRAINT `fixed_report_presets_defaults_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fixed_report_presets_defaults_ibfk_3` FOREIGN KEY (`default_preset`) REFERENCES `fixed_report_presets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table fixed_report_presets_defaults_customer
# ------------------------------------------------------------

CREATE TABLE `fixed_report_presets_defaults_customer` (
  `report_type` varchar(64) NOT NULL DEFAULT '',
  `customer_id` int(11) NOT NULL,
  `default_preset` int(11) unsigned NOT NULL,
  PRIMARY KEY (`report_type`,`customer_id`),
  KEY `employee_id` (`customer_id`),
  KEY `default_preset` (`default_preset`),
  CONSTRAINT `fixed_report_presets_defaults_customer_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fixed_report_presets_defaults_customer_ibfk_3` FOREIGN KEY (`default_preset`) REFERENCES `fixed_report_presets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table fixed_report_presets_defaults_district
# ------------------------------------------------------------

CREATE TABLE `fixed_report_presets_defaults_district` (
  `report_type` varchar(64) NOT NULL DEFAULT '',
  `district_id` int(11) NOT NULL,
  `default_preset` int(11) unsigned NOT NULL,
  PRIMARY KEY (`report_type`,`district_id`),
  KEY `employee_id` (`district_id`),
  KEY `default_preset` (`default_preset`),
  CONSTRAINT `fixed_report_presets_defaults_district_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fixed_report_presets_defaults_district_ibfk_3` FOREIGN KEY (`default_preset`) REFERENCES `fixed_report_presets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table fixed_report_presets_defaults_unit
# ------------------------------------------------------------

CREATE TABLE `fixed_report_presets_defaults_unit` (
  `report_type` varchar(64) NOT NULL DEFAULT '',
  `unit_id` int(11) NOT NULL,
  `default_preset` int(11) unsigned NOT NULL,
  PRIMARY KEY (`report_type`,`unit_id`),
  KEY `employee_id` (`unit_id`),
  KEY `default_preset` (`default_preset`),
  CONSTRAINT `fixed_report_presets_defaults_unit_ibfk_3` FOREIGN KEY (`unit_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fixed_report_presets_defaults_unit_ibfk_4` FOREIGN KEY (`default_preset`) REFERENCES `fixed_report_presets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table flow_angle
# ------------------------------------------------------------

CREATE TABLE `flow_angle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flowgraph_id` int(11) NOT NULL,
  `x` int(11) DEFAULT '0',
  `y` int(11) DEFAULT '0',
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2126 DEFAULT CHARSET=utf8;



# Dump of table flow_arrow
# ------------------------------------------------------------

CREATE TABLE `flow_arrow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flow_graph_id` int(11) NOT NULL,
  `from_client_id` int(11) DEFAULT NULL,
  `to_client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flow_graph_id` (`flow_graph_id`),
  CONSTRAINT `flow_arrow_ibfk_1` FOREIGN KEY (`flow_graph_id`) REFERENCES `flow_graph` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10436 DEFAULT CHARSET=latin1;



# Dump of table flow_graph
# ------------------------------------------------------------

CREATE TABLE `flow_graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isTemplate` tinyint(1) DEFAULT NULL,
  `templateName` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `customerId` int(11) NOT NULL,
  `submodule_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2252 DEFAULT CHARSET=latin1;



# Dump of table flow_step
# ------------------------------------------------------------

CREATE TABLE `flow_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flowgraph_id` int(11) NOT NULL,
  `predefined_values_id` int(11) NOT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_flowsteps_flowgraph1` (`flowgraph_id`),
  KEY `fk_flowsteps_activity_predefined_values1` (`predefined_values_id`),
  CONSTRAINT `flow_step_ibfk_1` FOREIGN KEY (`flowgraph_id`) REFERENCES `flow_graph` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8536 DEFAULT CHARSET=latin1;



# Dump of table forecast
# ------------------------------------------------------------

CREATE TABLE `forecast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `section` int(11) NOT NULL DEFAULT '0',
  `variableType` int(2) NOT NULL DEFAULT '0',
  `forecastDate` date NOT NULL,
  `isTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `templateName` varchar(50) NOT NULL,
  `salesTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `comments` varchar(200) NOT NULL,
  `year` year(4) NOT NULL,
  `month` int(2) NOT NULL,
  `week` int(2) NOT NULL,
  `weekday` tinyint(1) NOT NULL,
  `forecast00_01` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast01_02` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast02_03` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast03_04` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast04_05` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast05_06` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast06_07` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast07_08` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast08_09` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast09_10` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast10_11` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast11_12` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast12_13` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast13_14` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast14_15` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast15_16` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast16_17` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast17_18` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast18_19` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast19_20` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast20_21` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast21_22` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast22_23` decimal(10,2) NOT NULL DEFAULT '0.00',
  `forecast23_24` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forecast_2` (`restId`,`forecastDate`),
  CONSTRAINT `forecast_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111757349 DEFAULT CHARSET=utf8;



# Dump of table forecastNew
# ------------------------------------------------------------

CREATE TABLE `forecastNew` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `variableType` int(11) NOT NULL,
  `section` int(11) DEFAULT '0',
  `date` datetime NOT NULL,
  `value` decimal(9,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=11070 DEFAULT CHARSET=latin1;



# Dump of table forms
# ------------------------------------------------------------

CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formType` tinyint(1) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `extDocument` varchar(100) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forms_1` (`restId`),
  CONSTRAINT `forms_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2519 DEFAULT CHARSET=utf8;



# Dump of table forum
# ------------------------------------------------------------

CREATE TABLE `forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(80) NOT NULL,
  `htmlText` text NOT NULL,
  `pubDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `persId` int(11) NOT NULL DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forum_1` (`persId`),
  CONSTRAINT `forum_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6597 DEFAULT CHARSET=utf8;



# Dump of table forum_comment
# ------------------------------------------------------------

CREATE TABLE `forum_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `htmlText` text NOT NULL,
  `pubDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `persId` int(11) NOT NULL DEFAULT '0',
  `forumId` int(11) NOT NULL DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `forum_comment_1` (`forumId`),
  KEY `forum_comment_2` (`persId`),
  CONSTRAINT `forum_comment_1` FOREIGN KEY (`forumId`) REFERENCES `forum` (`id`),
  CONSTRAINT `forum_comment_2` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2146 DEFAULT CHARSET=utf8;



# Dump of table fxx_schedule
# ------------------------------------------------------------

CREATE TABLE `fxx_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `days` int(2) NOT NULL DEFAULT '0',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `expires` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `comment` text NOT NULL,
  `lastDate` date NOT NULL DEFAULT '0000-00-00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId` (`persId`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=11906 DEFAULT CHARSET=utf8;



# Dump of table fxx_schedule_shift
# ------------------------------------------------------------

CREATE TABLE `fxx_schedule_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fSchedId` int(11) NOT NULL,
  `dayNo` int(2) NOT NULL,
  `begTime` time NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL DEFAULT '00:00:00',
  `endBreak` time NOT NULL DEFAULT '00:00:00',
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `section` int(11) NOT NULL DEFAULT '1',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `countScheduleHours` tinyint(1) NOT NULL DEFAULT '1',
  `countPunchHours` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fSchedId` (`fSchedId`),
  KEY `categoryId` (`categoryId`),
  KEY `fSchedId_2` (`fSchedId`,`dayNo`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `crmSchedId` (`crmSchedId`),
  CONSTRAINT `fxx_schedule_shift_1` FOREIGN KEY (`fSchedId`) REFERENCES `fxx_schedule` (`id`),
  CONSTRAINT `fxx_schedule_shift_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fxx_schedule_shift_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=356978 DEFAULT CHARSET=utf8;



# Dump of table fxx_scheduletask
# ------------------------------------------------------------

CREATE TABLE `fxx_scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fxx_scheduletask_1` (`scheduleId`),
  KEY `fxx_scheduletask_2` (`categoryId`),
  CONSTRAINT `fxx_scheduletask_1` FOREIGN KEY (`scheduleId`) REFERENCES `fxx_schedule_shift` (`id`),
  CONSTRAINT `fxx_scheduletask_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4668 DEFAULT CHARSET=utf8;



# Dump of table genrule
# ------------------------------------------------------------

CREATE TABLE `genrule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `section` int(11) NOT NULL DEFAULT '0',
  `variableType` int(2) NOT NULL DEFAULT '0',
  `sales` decimal(10,0) NOT NULL DEFAULT '0',
  `multiplyToForecast` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  CONSTRAINT `genrule_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27238 DEFAULT CHARSET=utf8;



# Dump of table genrule_shift
# ------------------------------------------------------------

CREATE TABLE `genrule_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruleId` int(11) NOT NULL,
  `schedulecategoryId` int(11) NOT NULL,
  `sCount` int(10) NOT NULL,
  `sDelay` int(3) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `groupSort` (`ruleId`,`schedulecategoryId`),
  KEY `genrule_shift_2` (`schedulecategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=71843 DEFAULT CHARSET=utf8;



# Dump of table hourmatrix
# ------------------------------------------------------------

CREATE TABLE `hourmatrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `section` int(11) NOT NULL DEFAULT '0',
  `weekday` tinyint(1) NOT NULL DEFAULT '0',
  `dayIndex` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix00_01` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix01_02` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix02_03` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix03_04` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix04_05` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix05_06` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix06_07` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix07_08` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix08_09` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix09_10` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix10_11` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix11_12` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix12_13` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix13_14` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix14_15` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix15_16` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix16_17` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix17_18` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix18_19` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix19_20` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix20_21` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix21_22` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix22_23` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hourmatrix23_24` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `hourmatrix_1` (`restId`),
  CONSTRAINT `hourmatrix_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8352 DEFAULT CHARSET=utf8;



# Dump of table integration_metadata
# ------------------------------------------------------------

CREATE TABLE `integration_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyType` varchar(2) NOT NULL,
  `localId` int(11) NOT NULL DEFAULT '0',
  `foreignData` varchar(255) NOT NULL,
  `tag` varchar(50) NOT NULL DEFAULT '',
  `restId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `integration_metadata_2` (`keyType`,`localId`,`restId`),
  KEY `integration_metadata_1` (`restId`),
  CONSTRAINT `integration_metadata_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table integrationkey
# ------------------------------------------------------------

CREATE TABLE `integrationkey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyType` varchar(45) NOT NULL,
  `localId` int(11) NOT NULL,
  `foreignId` varchar(50) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `keyType_1` (`keyType`,`localId`),
  KEY `keyType_2` (`keyType`,`foreignId`)
) ENGINE=InnoDB AUTO_INCREMENT=8562846 DEFAULT CHARSET=utf8;



# Dump of table integrationkey_type
# ------------------------------------------------------------

CREATE TABLE `integrationkey_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyType` tinyint(4) NOT NULL DEFAULT '0',
  `externalCode` varchar(45) NOT NULL,
  `qt_customerId` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_integrationkey_type_1_idx` (`qt_customerId`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;



# Dump of table integrationkilled
# ------------------------------------------------------------

CREATE TABLE `integrationkilled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyType` char(2) NOT NULL,
  `localId` int(11) NOT NULL,
  `foreignId` varchar(50) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `keyType_1` (`keyType`,`localId`),
  KEY `keyType_2` (`keyType`,`foreignId`)
) ENGINE=InnoDB AUTO_INCREMENT=5533100 DEFAULT CHARSET=utf8;



# Dump of table interruption_shifts_connection
# ------------------------------------------------------------

CREATE TABLE `interruption_shifts_connection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interruption_shift_id` int(11) NOT NULL,
  `org_schedule_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `unique_pair` (`interruption_shift_id`,`org_schedule_id`),
  KEY `interruption_shift_id_idx` (`interruption_shift_id`),
  KEY `org_schedule_id_idx` (`org_schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=267075 DEFAULT CHARSET=latin1;



# Dump of table jobapplicant
# ------------------------------------------------------------

CREATE TABLE `jobapplicant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL DEFAULT '',
  `lname` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `phone` varchar(50) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `jobId` int(11) NOT NULL,
  `internalNotes` text NOT NULL,
  `cvFile` varchar(100) NOT NULL,
  `persId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `jobId` (`jobId`),
  CONSTRAINT `jobapplicant_1` FOREIGN KEY (`jobId`) REFERENCES `joboffering` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2836 DEFAULT CHARSET=utf8;



# Dump of table jobcategory
# ------------------------------------------------------------

CREATE TABLE `jobcategory` (
  `jcatId` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `jcatName` varchar(50) NOT NULL,
  `roleId` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`jcatId`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=22794 DEFAULT CHARSET=utf8;



# Dump of table joboffering
# ------------------------------------------------------------

CREATE TABLE `joboffering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `refNo` char(15) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(50) NOT NULL,
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `refNo` (`refNo`),
  KEY `joboffering_1` (`restId`),
  CONSTRAINT `joboffering_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=885 DEFAULT CHARSET=utf8;



# Dump of table jobrequirement
# ------------------------------------------------------------

CREATE TABLE `jobrequirement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobId` int(11) NOT NULL,
  `skillId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `jobrequirement_3` (`jobId`,`skillId`),
  KEY `jobrequirement_2` (`skillId`),
  CONSTRAINT `jobrequirement_1` FOREIGN KEY (`jobId`) REFERENCES `joboffering` (`id`),
  CONSTRAINT `jobrequirement_2` FOREIGN KEY (`skillId`) REFERENCES `skills` (`skillId`)
) ENGINE=InnoDB AUTO_INCREMENT=3135 DEFAULT CHARSET=utf8;



# Dump of table jobskills
# ------------------------------------------------------------

CREATE TABLE `jobskills` (
  `jsId` int(11) NOT NULL AUTO_INCREMENT,
  `jsEmpId` int(11) NOT NULL,
  `jsSkillId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `skillExpires` tinyint(1) NOT NULL DEFAULT '0',
  `skillExpiringDate` varchar(10) NOT NULL,
  PRIMARY KEY (`jsId`),
  KEY `jsSort` (`jsEmpId`,`jsSkillId`),
  KEY `jobskills_2` (`jsSkillId`),
  CONSTRAINT `jobskills_1` FOREIGN KEY (`jsEmpId`) REFERENCES `employee` (`id`),
  CONSTRAINT `jobskills_2` FOREIGN KEY (`jsSkillId`) REFERENCES `skills` (`skillId`)
) ENGINE=InnoDB AUTO_INCREMENT=821588 DEFAULT CHARSET=utf8;



# Dump of table jobtemplates
# ------------------------------------------------------------

CREATE TABLE `jobtemplates` (
  `jtempId` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `jtempTitle` varchar(100) NOT NULL,
  `jtempCategory` int(11) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`jtempId`),
  KEY `jtempCategory` (`jtempCategory`),
  CONSTRAINT `jobtemplates_1` FOREIGN KEY (`jtempCategory`) REFERENCES `jobcategory` (`jcatId`)
) ENGINE=InnoDB AUTO_INCREMENT=2282 DEFAULT CHARSET=utf8;



# Dump of table jobtemplateskills
# ------------------------------------------------------------

CREATE TABLE `jobtemplateskills` (
  `jtsId` int(11) NOT NULL AUTO_INCREMENT,
  `jtsJobId` int(11) NOT NULL,
  `jtsSkillId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`jtsId`),
  KEY `jtsSort` (`jtsJobId`,`jtsSkillId`),
  KEY `jobtemplateskills_2` (`jtsSkillId`),
  CONSTRAINT `jobtemplateskills_1` FOREIGN KEY (`jtsJobId`) REFERENCES `jobtemplates` (`jtempId`),
  CONSTRAINT `jobtemplateskills_2` FOREIGN KEY (`jtsSkillId`) REFERENCES `skills` (`skillId`)
) ENGINE=InnoDB AUTO_INCREMENT=11509 DEFAULT CHARSET=utf8;



# Dump of table leaveapp
# ------------------------------------------------------------

CREATE TABLE `leaveapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `moderated` tinyint(1) DEFAULT '1',
  `beenViewed` tinyint(1) NOT NULL DEFAULT '0',
  `leaveReason` varchar(50) NOT NULL,
  `leaveId` int(11) DEFAULT NULL,
  `fromDate` date DEFAULT '0000-00-00',
  `fromTime` time DEFAULT '00:00:00',
  `toDate` date DEFAULT '0000-00-00',
  `toTime` time DEFAULT '00:00:00',
  `restId` int(11) NOT NULL DEFAULT '0',
  `extCode` varchar(10) NOT NULL DEFAULT '',
  `estHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estSalary` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sickLevel` decimal(5,2) NOT NULL DEFAULT '1.00',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `managerComments` text,
  `decideTs` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isPreliminary` tinyint(1) NOT NULL DEFAULT '0',
  `approvedPayment` tinyint(1) NOT NULL DEFAULT '1',
  `approvedLeaveByEmployee` tinyint(1) NOT NULL DEFAULT '0',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `absScheduleId` int(11) NOT NULL DEFAULT '0',
  `externalInfo1` varchar(50) DEFAULT NULL,
  `externalInfo2` varchar(50) DEFAULT NULL,
  `externalInfo3` varchar(50) DEFAULT NULL,
  `decisionId` int(11) NOT NULL DEFAULT '0',
  `teamId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `leaveapp_1` (`toPersId`),
  KEY `leaveapp_3` (`restId`),
  KEY `fixSchedId` (`fixSchedId`),
  KEY `fromPersId_fromDate` (`fromPersId`,`beenViewed`,`isPreliminary`,`fromDate`),
  KEY `teamId` (`teamId`),
  KEY `toPersId_idx` (`toPersId`) USING BTREE,
  KEY `fromPersId_idx` (`fromPersId`) USING BTREE,
  KEY `beenViewed_idx` (`beenViewed`) USING BTREE,
  KEY `fk_leaveapp_1` (`leaveId`),
  CONSTRAINT `fk_leaveapp_1` FOREIGN KEY (`leaveId`) REFERENCES `leavereason` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `leaveapp_1` FOREIGN KEY (`toPersId`) REFERENCES `employee` (`id`),
  CONSTRAINT `leaveapp_2` FOREIGN KEY (`fromPersId`) REFERENCES `employee` (`id`),
  CONSTRAINT `leaveapp_3` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4828378 DEFAULT CHARSET=utf8;



# Dump of table leaveapp_deleted
# ------------------------------------------------------------

CREATE TABLE `leaveapp_deleted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `moderated` tinyint(1) DEFAULT '1',
  `beenViewed` tinyint(1) NOT NULL DEFAULT '0',
  `leaveReason` varchar(50) NOT NULL,
  `leaveId` int(11) NOT NULL DEFAULT '0',
  `fromDate` date DEFAULT '0000-00-00',
  `fromTime` time DEFAULT '00:00:00',
  `toDate` date DEFAULT '0000-00-00',
  `toTime` time DEFAULT '00:00:00',
  `restId` int(11) NOT NULL DEFAULT '0',
  `extCode` varchar(10) NOT NULL DEFAULT '',
  `estHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estSalary` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sickLevel` decimal(5,2) NOT NULL DEFAULT '1.00',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `managerComments` text,
  `decideTs` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isPreliminary` tinyint(1) NOT NULL DEFAULT '0',
  `approvedPayment` tinyint(1) NOT NULL DEFAULT '1',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `absScheduleId` int(11) NOT NULL DEFAULT '0',
  `externalInfo1` varchar(50) DEFAULT NULL,
  `externalInfo2` varchar(50) DEFAULT NULL,
  `externalInfo3` varchar(50) DEFAULT NULL,
  `decisionId` int(11) NOT NULL DEFAULT '0',
  `teamId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `leaveapp_deleted_1` (`toPersId`),
  KEY `leaveapp_deleted_2` (`fromPersId`),
  KEY `leaveapp_deleted_3` (`restId`),
  KEY `leaveId` (`leaveId`),
  KEY `fixSchedId` (`fixSchedId`),
  KEY `teamId` (`teamId`),
  CONSTRAINT `leaveapp_deleted_1` FOREIGN KEY (`toPersId`) REFERENCES `employee` (`id`),
  CONSTRAINT `leaveapp_deleted_2` FOREIGN KEY (`fromPersId`) REFERENCES `employee` (`id`),
  CONSTRAINT `leaveapp_deleted_3` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4828378 DEFAULT CHARSET=utf8;



# Dump of table leaveoccasion
# ------------------------------------------------------------

CREATE TABLE `leaveoccasion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leaveReasonId` int(11) DEFAULT NULL,
  `occasions` int(11) NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL DEFAULT '0',
  `period` int(11) NOT NULL DEFAULT '0',
  `periodType` int(3) NOT NULL DEFAULT '0',
  `periodStartDate` date NOT NULL DEFAULT '0000-00-00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_leaveoccasion_1_idx` (`leaveReasonId`),
  CONSTRAINT `fk_leaveoccasion_1` FOREIGN KEY (`leaveReasonId`) REFERENCES `leavereason` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;



# Dump of table leavereason
# ------------------------------------------------------------

CREATE TABLE `leavereason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `ttext` varchar(80) NOT NULL DEFAULT '',
  `extCode` varchar(20) NOT NULL DEFAULT '',
  `confirmationText` text,
  `salaryBase` tinyint(1) NOT NULL DEFAULT '0',
  `abbrCode` varchar(10) NOT NULL DEFAULT '',
  `usedInTA` tinyint(1) NOT NULL DEFAULT '1',
  `isSickness` tinyint(1) NOT NULL DEFAULT '0',
  `fromDaySick` int(3) NOT NULL DEFAULT '0',
  `toDaySick` int(3) NOT NULL DEFAULT '0',
  `vacBgColor` int(10) NOT NULL DEFAULT '13107',
  `vacTextColor` int(10) NOT NULL DEFAULT '16777215',
  `prelvacBgColor` int(10) NOT NULL DEFAULT '13421721',
  `prelvacTextColor` int(10) NOT NULL DEFAULT '0',
  `lappBgColor` int(10) NOT NULL DEFAULT '39372',
  `lappTextColor` int(10) NOT NULL DEFAULT '16777215',
  `isVacation` tinyint(1) NOT NULL DEFAULT '0',
  `isPermission` tinyint(1) NOT NULL DEFAULT '0',
  `genOB` tinyint(1) NOT NULL DEFAULT '1',
  `fromDayOB` int(3) NOT NULL DEFAULT '0',
  `toDayOB` int(3) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '1',
  `fromDayOverTime` int(3) NOT NULL DEFAULT '0',
  `toDayOverTime` int(3) NOT NULL DEFAULT '0',
  `nonPayHours` int(2) NOT NULL DEFAULT '0',
  `trackerId` int(11) NOT NULL DEFAULT '0',
  `trackerOperator` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor` decimal(5,2) NOT NULL DEFAULT '1.00',
  `trackerId2` int(11) NOT NULL DEFAULT '0',
  `trackerOperator2` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor2` decimal(5,2) NOT NULL DEFAULT '1.00',
  `trackerId3` int(11) NOT NULL DEFAULT '0',
  `trackerOperator3` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor3` decimal(5,2) NOT NULL DEFAULT '1.00',
  `qualDayBuffer` int(3) NOT NULL DEFAULT '0',
  `maxQualDayYr` int(3) NOT NULL DEFAULT '0',
  `excludeDaysBetweenApplications` tinyint(1) NOT NULL DEFAULT '0',
  `countSicDaysOnly` tinyint(1) NOT NULL DEFAULT '1',
  `includeStandByOnCall` tinyint(1) NOT NULL DEFAULT '0',
  `useEgenmeldingLogic` tinyint(1) NOT NULL DEFAULT '0',
  `reminderDay1` int(2) NOT NULL DEFAULT '0',
  `reminderDay2` int(2) NOT NULL DEFAULT '0',
  `shiftAction` tinyint(1) NOT NULL DEFAULT '0',
  `splitByMidnight` tinyint(1) NOT NULL DEFAULT '0',
  `maxConsecutiveDays` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `leavereasonType_id` int(11) unsigned DEFAULT NULL,
  `dayCountGroupId` int(11) unsigned NOT NULL DEFAULT '0',
  `showInWebpunch` tinyint(1) NOT NULL DEFAULT '0',
  `preceededByLeaveId` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `extCodeIdx` (`extCode`),
  KEY `leavereasonType_id` (`leavereasonType_id`),
  KEY `dayCountGroupId` (`dayCountGroupId`),
  CONSTRAINT `leavereason_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `leavereason_ibfk_1` FOREIGN KEY (`leavereasonType_id`) REFERENCES `leavereason_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19709 DEFAULT CHARSET=utf8;



# Dump of table leavereason_reminder
# ------------------------------------------------------------

CREATE TABLE `leavereason_reminder` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dayNo` tinyint(4) unsigned NOT NULL,
  `message` text NOT NULL,
  `leavereason_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `leavereason_id` (`leavereason_id`),
  CONSTRAINT `leavereason_reminder_ibfk_1` FOREIGN KEY (`leavereason_id`) REFERENCES `leavereason` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=559 DEFAULT CHARSET=utf8;



# Dump of table leavereason_salary_type
# ------------------------------------------------------------

CREATE TABLE `leavereason_salary_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `maxMinutes` smallint(4) DEFAULT NULL,
  `maxMinutesPerPeriod` tinyint(4) DEFAULT '0',
  `dependedOnDayNo` tinyint(1) NOT NULL DEFAULT '1',
  `fromDayNo` smallint(6) DEFAULT NULL,
  `toDayNo` smallint(6) DEFAULT NULL,
  `fromEmploymentDay` int(11) NOT NULL DEFAULT '0',
  `toEmploymentDay` int(11) NOT NULL DEFAULT '0',
  `maxPerYear` tinyint(4) DEFAULT NULL,
  `generatesOB` tinyint(1) NOT NULL DEFAULT '0',
  `generatesOT` tinyint(1) NOT NULL DEFAULT '0',
  `leavereason_id` int(11) NOT NULL,
  `salaryType_id` int(11) NOT NULL,
  `replacementSalaryType_id` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `leavereason_id` (`leavereason_id`),
  KEY `salaryType_id` (`salaryType_id`),
  CONSTRAINT `leavereason_salary_type_ibfk_1` FOREIGN KEY (`leavereason_id`) REFERENCES `leavereason` (`id`) ON DELETE CASCADE,
  CONSTRAINT `leavereason_salary_type_ibfk_2` FOREIGN KEY (`salaryType_id`) REFERENCES `salarytypes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13752 DEFAULT CHARSET=utf8;



# Dump of table leavereason_type
# ------------------------------------------------------------

CREATE TABLE `leavereason_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `transfer` tinyint(1) NOT NULL,
  `isVacation` tinyint(1) NOT NULL,
  `isPermission` tinyint(1) NOT NULL,
  `isSickness` tinyint(1) NOT NULL,
  `isWorkHours` tinyint(1) NOT NULL DEFAULT '1',
  `restaurant_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restaurant_id` (`restaurant_id`),
  CONSTRAINT `leavereason_type_ibfk_1` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10253 DEFAULT CHARSET=utf8;



# Dump of table locales
# ------------------------------------------------------------

CREATE TABLE `locales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL DEFAULT '',
  `en_US` varchar(512) NOT NULL,
  `ja_JP` text,
  `pl_PL` text,
  `ru_RU` text,
  `ro_RO` text,
  `sv_SE` text,
  `hu_HU` text,
  `fi_FI` text,
  `da_DK` text,
  `no_NO` text,
  `de_DE` text,
  `lt_LT` text,
  `es_ES` text,
  `fr_FR` text,
  `et_EE` text,
  `nl_NL` text,
  `inVersion` int(4) DEFAULT NULL,
  `comment` text,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_2` (`tag`),
  UNIQUE KEY `tag_3` (`tag`),
  KEY `tag` (`tag`),
  KEY `english_index` (`en_US`(255))
) ENGINE=InnoDB AUTO_INCREMENT=7610 DEFAULT CHARSET=utf8;



# Dump of table locked_salary_period
# ------------------------------------------------------------

CREATE TABLE `locked_salary_period` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unitId` int(10) unsigned NOT NULL,
  `currentLockDateTime` datetime NOT NULL,
  `lastLockDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unit_id` (`unitId`)
) ENGINE=InnoDB AUTO_INCREMENT=3214 DEFAULT CHARSET=utf8;



# Dump of table locked_salary_period_changes_log
# ------------------------------------------------------------

CREATE TABLE `locked_salary_period_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=31440 DEFAULT CHARSET=utf8;



# Dump of table locked_salary_period_summary_log
# ------------------------------------------------------------

CREATE TABLE `locked_salary_period_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=26806 DEFAULT CHARSET=utf8;



# Dump of table loginlogg
# ------------------------------------------------------------

CREATE TABLE `loginlogg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `empId` int(11) DEFAULT '0',
  `year` year(4) DEFAULT NULL,
  `month` int(2) DEFAULT '0',
  `week` int(2) DEFAULT '0',
  `loggDate` date NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId_loggDate` (`restId`,`loggDate`),
  KEY `empId_loggDate` (`empId`,`loggDate`),
  KEY `loggDate` (`loggDate`),
  KEY `year` (`year`,`month`,`week`),
  KEY `restId_year_month` (`restId`,`year`,`month`),
  CONSTRAINT `loginlogg_1` FOREIGN KEY (`empId`) REFERENCES `employee` (`id`),
  CONSTRAINT `loginlogg_2` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54134890 DEFAULT CHARSET=utf8;



# Dump of table loginlogg_archive
# ------------------------------------------------------------

CREATE TABLE `loginlogg_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `empId` int(11) DEFAULT '0',
  `year` year(4) DEFAULT NULL,
  `month` int(2) DEFAULT '0',
  `week` int(2) DEFAULT '0',
  `loggDate` date NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=508 DEFAULT CHARSET=utf8;



# Dump of table loginlogg_failed
# ------------------------------------------------------------

CREATE TABLE `loginlogg_failed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `empId` int(11) DEFAULT '0',
  `year` year(4) DEFAULT NULL,
  `month` int(2) DEFAULT '0',
  `week` int(2) DEFAULT '0',
  `loggDate` date NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `status` varchar(20) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId_loggDate` (`restId`,`loggDate`),
  KEY `empId_loggDate` (`empId`,`loggDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table maillogg
# ------------------------------------------------------------

CREATE TABLE `maillogg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `fromMail` varchar(50) NOT NULL,
  `toPersId` int(11) DEFAULT '0',
  `toMail` varchar(50) NOT NULL,
  `toGroupId` int(11) DEFAULT '0',
  `year` year(4) NOT NULL,
  `month` int(2) NOT NULL DEFAULT '0',
  `week` int(2) NOT NULL DEFAULT '0',
  `loggDate` date NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `maillogg_1` (`toPersId`),
  KEY `maillogg_2` (`fromPersId`),
  KEY `maillogg_3` (`restId`),
  CONSTRAINT `maillogg_2` FOREIGN KEY (`fromPersId`) REFERENCES `employee` (`id`),
  CONSTRAINT `maillogg_3` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9740754 DEFAULT CHARSET=utf8;



# Dump of table maillogg_archive
# ------------------------------------------------------------

CREATE TABLE `maillogg_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `fromMail` varchar(50) NOT NULL,
  `toPersId` int(11) DEFAULT '0',
  `toMail` varchar(50) NOT NULL,
  `toGroupId` int(11) DEFAULT '0',
  `year` year(4) NOT NULL,
  `month` int(2) NOT NULL DEFAULT '0',
  `week` int(2) NOT NULL DEFAULT '0',
  `loggDate` date NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table managermemo
# ------------------------------------------------------------

CREATE TABLE `managermemo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `memoDate` date NOT NULL,
  `memo` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `managermemo_2` (`restId`,`memoDate`),
  CONSTRAINT `managermemo_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14060 DEFAULT CHARSET=utf8;



# Dump of table manual_salaries
# ------------------------------------------------------------

CREATE TABLE `manual_salaries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timepunch_id` int(11) NOT NULL,
  `salarytype_id` int(11) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `hours` decimal(6,2) NOT NULL,
  `operator` int(1) NOT NULL,
  `typeOfSalary` int(1) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `manSalRuleId` int(10) unsigned NOT NULL DEFAULT '0',
  `calculatedCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `socialCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `leaveCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `timepunch_id` (`timepunch_id`),
  KEY `salarytype_id` (`salarytype_id`),
  KEY `manSalRuleId` (`manSalRuleId`),
  CONSTRAINT `manual_salaries_ibfk_1` FOREIGN KEY (`timepunch_id`) REFERENCES `timepunch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `manual_salaries_ibfk_2` FOREIGN KEY (`salarytype_id`) REFERENCES `salarytypes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=58660983 DEFAULT CHARSET=utf8;



# Dump of table manual_salary_types
# ------------------------------------------------------------

CREATE TABLE `manual_salary_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salTypeStCode1` int(11) NOT NULL DEFAULT '-1',
  `salTypeStCode2` int(11) NOT NULL DEFAULT '-1',
  `functionType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `timeIntervalType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `multiFactor` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ruleCondition` tinyint(4) NOT NULL DEFAULT '0',
  `ruleOperator` tinyint(4) NOT NULL DEFAULT '3',
  `ruleSalaryTypeStCode` int(11) NOT NULL DEFAULT '0',
  `ruleWorkedHrs` decimal(5,2) NOT NULL DEFAULT '0.00',
  `startTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `minMinutes` decimal(6,2) NOT NULL DEFAULT '0.00',
  `maxMinutes` decimal(6,2) NOT NULL DEFAULT '0.00',
  `refType` tinyint(3) unsigned NOT NULL,
  `refId` int(11) NOT NULL DEFAULT '0',
  `parentRuleId` int(10) unsigned NOT NULL DEFAULT '0',
  `inheritParent` int(1) unsigned NOT NULL DEFAULT '1',
  `comment` text,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `refType` (`refType`),
  KEY `refId` (`refId`)
) ENGINE=InnoDB AUTO_INCREMENT=86984254 DEFAULT CHARSET=utf8;



# Dump of table manual_salary_types_sched_chfl
# ------------------------------------------------------------

CREATE TABLE `manual_salary_types_sched_chfl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salTypeStCode1` int(11) NOT NULL DEFAULT '-1',
  `salTypeStCode2` int(11) NOT NULL DEFAULT '-1',
  `functionType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `timeIntervalType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `multiFactor` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ruleCondition` tinyint(4) NOT NULL DEFAULT '0',
  `ruleOperator` tinyint(4) NOT NULL DEFAULT '3',
  `ruleSalaryTypeStCode` int(11) NOT NULL DEFAULT '0',
  `ruleWorkedHrs` decimal(5,2) NOT NULL DEFAULT '0.00',
  `startTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `minMinutes` decimal(6,2) NOT NULL DEFAULT '0.00',
  `maxMinutes` decimal(6,2) NOT NULL DEFAULT '0.00',
  `refType` tinyint(3) unsigned NOT NULL,
  `refId` int(11) NOT NULL,
  `parentRuleId` int(10) unsigned NOT NULL DEFAULT '0',
  `inheritParent` int(1) unsigned NOT NULL DEFAULT '1',
  `comment` text,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `refType` (`refType`),
  KEY `refId` (`refId`)
) ENGINE=InnoDB AUTO_INCREMENT=403149 DEFAULT CHARSET=utf8;



# Dump of table manual_salary_types_sched_deleted
# ------------------------------------------------------------

CREATE TABLE `manual_salary_types_sched_deleted` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salTypeStCode1` int(11) NOT NULL DEFAULT '-1',
  `salTypeStCode2` int(11) NOT NULL DEFAULT '-1',
  `functionType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `timeIntervalType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `multiFactor` decimal(5,2) NOT NULL DEFAULT '0.00',
  `ruleCondition` tinyint(4) NOT NULL DEFAULT '0',
  `ruleOperator` tinyint(4) NOT NULL DEFAULT '3',
  `ruleSalaryTypeStCode` int(11) NOT NULL DEFAULT '0',
  `ruleWorkedHrs` decimal(5,2) NOT NULL DEFAULT '0.00',
  `startTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `minMinutes` decimal(6,2) NOT NULL DEFAULT '0.00',
  `maxMinutes` decimal(6,2) NOT NULL DEFAULT '0.00',
  `refType` tinyint(3) unsigned NOT NULL,
  `refId` int(11) NOT NULL,
  `parentRuleId` int(10) unsigned NOT NULL DEFAULT '0',
  `inheritParent` int(1) unsigned NOT NULL DEFAULT '1',
  `comment` text,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `refType` (`refType`),
  KEY `refId` (`refId`)
) ENGINE=InnoDB AUTO_INCREMENT=22537382 DEFAULT CHARSET=utf8;



# Dump of table mood
# ------------------------------------------------------------

CREATE TABLE `mood` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeId` int(11) NOT NULL,
  `mood` tinyint(4) NOT NULL,
  `originType` int(11) DEFAULT NULL,
  `originId` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_mood_1` (`employeeId`),
  KEY `employeeId_ts` (`employeeId`,`ts`),
  CONSTRAINT `fk_mood_1` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;



# Dump of table move_employee
# ------------------------------------------------------------

CREATE TABLE `move_employee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employeeId` int(11) NOT NULL,
  `newUnitId` int(11) NOT NULL,
  `oldUnitId` int(11) NOT NULL,
  `date` date NOT NULL,
  `toDate` date DEFAULT NULL,
  `eventType` tinyint(4) NOT NULL,
  `isCompleted` tinyint(1) DEFAULT '0',
  `rollOutShiftEnabled` tinyint(1) DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `employee_id_idx` (`employeeId`),
  KEY `date_index` (`date`),
  CONSTRAINT `employee_id` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table move_employee_attributes
# ------------------------------------------------------------

CREATE TABLE `move_employee_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moveEmployeeId` int(11) unsigned NOT NULL,
  `attributeName` varchar(45) NOT NULL,
  `attributeValue` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `move_employee_idx` (`moveEmployeeId`),
  CONSTRAINT `move_employee` FOREIGN KEY (`moveEmployeeId`) REFERENCES `move_employee` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table news
# ------------------------------------------------------------

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(80) NOT NULL,
  `htmlText` text NOT NULL,
  `pubDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `restId` int(11) NOT NULL DEFAULT '0',
  `isPublic` tinyint(1) NOT NULL DEFAULT '1',
  `documentURL` varchar(75) NOT NULL,
  `doc_ext` varchar(10) NOT NULL,
  `pictureURL` varchar(75) NOT NULL,
  `asset_dir` varchar(100) NOT NULL DEFAULT '',
  `localNews` tinyint(1) NOT NULL DEFAULT '1',
  `empGroupId` int(11) NOT NULL DEFAULT '0',
  `createdByForceBy` int(11) NOT NULL DEFAULT '0',
  `category_name` char(32) DEFAULT NULL,
  `submodule_id` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `news_1` (`restId`),
  KEY `createdByForceBy` (`createdByForceBy`),
  KEY `submodule_id` (`submodule_id`),
  CONSTRAINT `news_ibfk_1` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78588 DEFAULT CHARSET=utf8;



# Dump of table news_comment
# ------------------------------------------------------------

CREATE TABLE `news_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `htmlText` text NOT NULL,
  `pubDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `persId` int(11) NOT NULL DEFAULT '0',
  `newsId` int(11) NOT NULL DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `news_comment_1` (`newsId`),
  KEY `news_comment_2` (`persId`),
  CONSTRAINT `news_comment_1` FOREIGN KEY (`newsId`) REFERENCES `news` (`id`),
  CONSTRAINT `news_comment_2` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1787 DEFAULT CHARSET=utf8;



# Dump of table news_employee_status
# ------------------------------------------------------------

CREATE TABLE `news_employee_status` (
  `employeeId` int(11) NOT NULL,
  `newsId` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`employeeId`,`newsId`,`status`),
  KEY `employeeId` (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table nodate
# ------------------------------------------------------------

CREATE TABLE `nodate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reddayId` int(11) NOT NULL DEFAULT '0',
  `noDate` date NOT NULL DEFAULT '0000-00-00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `reddayId` (`reddayId`),
  CONSTRAINT `nodate_1` FOREIGN KEY (`reddayId`) REFERENCES `reddays` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10013 DEFAULT CHARSET=utf8;



# Dump of table oauth_access
# ------------------------------------------------------------

CREATE TABLE `oauth_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeId` int(11) NOT NULL,
  `idpId` int(11) NOT NULL,
  `accessToken` mediumtext,
  `accessTokenExpire` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_oauth_access_1_idx` (`employeeId`),
  KEY `fk_oauth_access_2_idx` (`idpId`),
  CONSTRAINT `fk_oauth_access_1` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_oauth_access_2` FOREIGN KEY (`idpId`) REFERENCES `oauth_idp` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



# Dump of table oauth_idp
# ------------------------------------------------------------

CREATE TABLE `oauth_idp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qt_customerId` int(11) NOT NULL,
  `idpLoginUrl` varchar(1024) DEFAULT NULL,
  `redirectUrl` varchar(255) DEFAULT '',
  `endpointUrl` varchar(255) DEFAULT NULL,
  `clientId` varchar(45) DEFAULT NULL,
  `clientSecret` varchar(45) DEFAULT NULL,
  `implementation` varchar(45) NOT NULL DEFAULT '',
  `display` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_oauth_idp_1_idx` (`qt_customerId`),
  CONSTRAINT `fk_oauth_idp_1` FOREIGN KEY (`qt_customerId`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



# Dump of table od_text
# ------------------------------------------------------------

CREATE TABLE `od_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(11) NOT NULL,
  `tag` char(30) NOT NULL,
  `mheader` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `localetag` (`locale`,`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;



# Dump of table original_shift_info
# ------------------------------------------------------------

CREATE TABLE `original_shift_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `originalShiftId` int(10) unsigned NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `originalShiftId_UNIQUE` (`originalShiftId`),
  KEY `original_shift` (`originalShiftId`)
) ENGINE=InnoDB AUTO_INCREMENT=58380 DEFAULT CHARSET=utf8;



# Dump of table overtime
# ------------------------------------------------------------

CREATE TABLE `overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `templateId` int(11) NOT NULL DEFAULT '0',
  `reddayId` int(11) NOT NULL DEFAULT '0',
  `weekDay` tinyint(1) NOT NULL DEFAULT '0',
  `overtimeFrom` time NOT NULL DEFAULT '00:00:00',
  `overtimeTo` time NOT NULL DEFAULT '00:00:00',
  `oType` tinyint(2) NOT NULL DEFAULT '1',
  `firstHours` decimal(4,2) NOT NULL DEFAULT '0.00',
  `firstType` tinyint(2) NOT NULL DEFAULT '0',
  `firstBeg` time NOT NULL DEFAULT '00:00:00',
  `firstEnd` time NOT NULL DEFAULT '00:00:00',
  `isAdditionalSalaryType` tinyint(1) NOT NULL DEFAULT '0',
  `replaceBaseSalaries` tinyint(1) NOT NULL DEFAULT '1',
  `version` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `reddayId` (`reddayId`),
  KEY `restId` (`restId`,`templateId`,`reddayId`,`oType`,`weekDay`,`overtimeFrom`),
  KEY `templateId` (`templateId`,`reddayId`,`oType`,`weekDay`,`overtimeFrom`),
  KEY `allId` (`restId`,`oType`,`templateId`,`reddayId`,`weekDay`,`overtimeFrom`)
) ENGINE=InnoDB AUTO_INCREMENT=3200822 DEFAULT CHARSET=utf8;



# Dump of table overtime_method
# ------------------------------------------------------------

CREATE TABLE `overtime_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `base` tinyint(1) NOT NULL DEFAULT '0',
  `periodType` tinyint(1) NOT NULL DEFAULT '0',
  `startDate` date NOT NULL DEFAULT '0000-00-00',
  `periodLength` int(3) NOT NULL DEFAULT '0',
  `hours` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `countUpwards` tinyint(1) NOT NULL DEFAULT '0',
  `thresholdType` tinyint(1) NOT NULL DEFAULT '0',
  `thresholdStartDate` date NOT NULL DEFAULT '0000-00-00',
  `thresholdLength` int(3) NOT NULL DEFAULT '0',
  `thresholdHours` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `otLevelType` tinyint(1) NOT NULL DEFAULT '0',
  `isSystemMethod` tinyint(1) NOT NULL DEFAULT '0',
  `reduceWithBankHolidays` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  CONSTRAINT `fk_overtime_method_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1122 DEFAULT CHARSET=utf8;



# Dump of table overtime_method_level
# ------------------------------------------------------------

CREATE TABLE `overtime_method_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `overtimeMethodId` int(11) NOT NULL DEFAULT '0',
  `hours` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `overtimeType` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `overtimeMethodId` (`overtimeMethodId`),
  CONSTRAINT `fk_overtime_method_level_1` FOREIGN KEY (`overtimeMethodId`) REFERENCES `overtime_method` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=609 DEFAULT CHARSET=utf8;



# Dump of table password_used
# ------------------------------------------------------------

CREATE TABLE `password_used` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empId` int(11) DEFAULT NULL,
  `md5password` varchar(32) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=584539 DEFAULT CHARSET=utf8;



# Dump of table paxml_standard_codes
# ------------------------------------------------------------

CREATE TABLE `paxml_standard_codes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` char(15) DEFAULT NULL,
  `description` char(100) DEFAULT NULL,
  `salaryType` int(2) unsigned NOT NULL DEFAULT '0',
  `ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;



# Dump of table payrolltransfer
# ------------------------------------------------------------

CREATE TABLE `payrolltransfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timepunch_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `chfl_id` int(11) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `transferred_date` datetime NOT NULL,
  `transferred_by_persId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `timepunch_id` (`timepunch_id`),
  KEY `schedule_id` (`schedule_id`),
  KEY `transferred_by_persId` (`transferred_by_persId`),
  KEY `chfl_id` (`chfl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5087 DEFAULT CHARSET=latin1;



# Dump of table pre_defined_comments
# ------------------------------------------------------------

CREATE TABLE `pre_defined_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `comment` tinytext NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_restId_idx` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;



# Dump of table prebook_schedule
# ------------------------------------------------------------

CREATE TABLE `prebook_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `weeks` int(2) NOT NULL DEFAULT '0',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `expires` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `comment` text NOT NULL,
  `lastDate` date NOT NULL DEFAULT '0000-00-00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId` (`persId`),
  KEY `prebook_schedule_1` (`restId`),
  CONSTRAINT `prebook_schedule_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;



# Dump of table prebook_schedule_shift
# ------------------------------------------------------------

CREATE TABLE `prebook_schedule_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fSchedId` int(11) NOT NULL,
  `weekNo` int(2) NOT NULL,
  `weekDay` int(1) NOT NULL,
  `weekDayAlt` int(1) NOT NULL,
  `begTime` time NOT NULL,
  `endTime` time NOT NULL,
  `section` int(11) NOT NULL DEFAULT '1',
  `comment` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fSchedId` (`fSchedId`),
  CONSTRAINT `prebook_schedule_shift_1` FOREIGN KEY (`fSchedId`) REFERENCES `prebook_schedule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;



# Dump of table predefined_values
# ------------------------------------------------------------

CREATE TABLE `predefined_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(160) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci,
  `orderNr` int(11) DEFAULT NULL,
  `submodule_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `predefined_values_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3278 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table product_risk_relation
# ------------------------------------------------------------

CREATE TABLE `product_risk_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productPredefinedId` int(11) NOT NULL,
  `productRiskPredefinedId` int(11) NOT NULL,
  `activityGroupId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;



# Dump of table project
# ------------------------------------------------------------

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectNo` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `begDate` date NOT NULL,
  `endDate` date NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  `customerId` int(11) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL DEFAULT '0',
  `groupId` int(11) NOT NULL DEFAULT '0',
  `projectCategoryId` int(11) NOT NULL DEFAULT '0',
  `info` text NOT NULL,
  `totalHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totalVolume` decimal(10,2) NOT NULL DEFAULT '0.00',
  `monCallable` tinyint(1) NOT NULL DEFAULT '1',
  `monBegTime` time NOT NULL DEFAULT '00:00:00',
  `monEndTime` time NOT NULL DEFAULT '00:00:00',
  `tueCallable` tinyint(1) NOT NULL DEFAULT '1',
  `tueBegTime` time NOT NULL DEFAULT '00:00:00',
  `tueEndTime` time NOT NULL DEFAULT '00:00:00',
  `wedCallable` tinyint(1) NOT NULL DEFAULT '1',
  `wedBegTime` time NOT NULL DEFAULT '00:00:00',
  `wedEndTime` time NOT NULL DEFAULT '00:00:00',
  `thuCallable` tinyint(1) NOT NULL DEFAULT '1',
  `thuBegTime` time NOT NULL DEFAULT '00:00:00',
  `thuEndTime` time NOT NULL DEFAULT '00:00:00',
  `friCallable` tinyint(1) NOT NULL DEFAULT '1',
  `friBegTime` time NOT NULL DEFAULT '00:00:00',
  `friEndTime` time NOT NULL DEFAULT '00:00:00',
  `satCallable` tinyint(1) NOT NULL DEFAULT '1',
  `satBegTime` time NOT NULL DEFAULT '00:00:00',
  `satEndTime` time NOT NULL DEFAULT '00:00:00',
  `sunCallable` tinyint(1) NOT NULL DEFAULT '1',
  `sunBegTime` time NOT NULL DEFAULT '00:00:00',
  `sunEndTime` time NOT NULL DEFAULT '00:00:00',
  `scheduleReds` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `restId` (`restId`),
  KEY `groupId` (`groupId`),
  KEY `project_3` (`customerId`),
  KEY `project_4` (`managerId`),
  KEY `project_5` (`projectCategoryId`),
  KEY `projectNoIndex` (`projectNo`),
  CONSTRAINT `project_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `project_2` FOREIGN KEY (`groupId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `project_3` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`),
  CONSTRAINT `project_5` FOREIGN KEY (`projectCategoryId`) REFERENCES `projectcategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1576 DEFAULT CHARSET=utf8;



# Dump of table project_breaks
# ------------------------------------------------------------

CREATE TABLE `project_breaks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `hours` decimal(8,2) NOT NULL DEFAULT '0.00',
  `breakMinutes` int(4) NOT NULL DEFAULT '0',
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `distribute` tinyint(1) NOT NULL DEFAULT '1',
  `noOfBreaks` tinyint(1) NOT NULL DEFAULT '1',
  `afterHours` decimal(8,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `project_breaks_2` (`restId`),
  CONSTRAINT `project_breaks_2` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=943 DEFAULT CHARSET=utf8;



# Dump of table project_otype
# ------------------------------------------------------------

CREATE TABLE `project_otype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` int(11) NOT NULL DEFAULT '0',
  `no` int(2) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `budget` int(11) NOT NULL DEFAULT '0',
  `productive` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `project_otype_2` (`projectId`),
  CONSTRAINT `project_otype_2` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23978 DEFAULT CHARSET=utf8;



# Dump of table projectcategory
# ------------------------------------------------------------

CREATE TABLE `projectcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `unitType` varchar(50) NOT NULL DEFAULT 'Calls',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `projectcategory_2` (`restId`),
  CONSTRAINT `projectcategory_2` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;



# Dump of table projectcategory_otype
# ------------------------------------------------------------

CREATE TABLE `projectcategory_otype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `no` int(2) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `projectcategory_otype_2` (`categoryId`),
  CONSTRAINT `projectcategory_otype_2` FOREIGN KEY (`categoryId`) REFERENCES `projectcategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;



# Dump of table projectlogg
# ------------------------------------------------------------

CREATE TABLE `projectlogg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` int(11) NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `projectlogg_2` (`projectId`),
  CONSTRAINT `projectlogg_2` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14598 DEFAULT CHARSET=utf8;



# Dump of table projecttask
# ------------------------------------------------------------

CREATE TABLE `projecttask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` int(11) NOT NULL DEFAULT '0',
  `name` varchar(80) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `begDate` date NOT NULL,
  `endDate` date NOT NULL,
  `monCallable` tinyint(1) NOT NULL DEFAULT '1',
  `monBegTime` time NOT NULL DEFAULT '00:00:00',
  `monEndTime` time NOT NULL DEFAULT '00:00:00',
  `tueCallable` tinyint(1) NOT NULL DEFAULT '1',
  `tueBegTime` time NOT NULL DEFAULT '00:00:00',
  `tueEndTime` time NOT NULL DEFAULT '00:00:00',
  `wedCallable` tinyint(1) NOT NULL DEFAULT '1',
  `wedBegTime` time NOT NULL DEFAULT '00:00:00',
  `wedEndTime` time NOT NULL DEFAULT '00:00:00',
  `thuCallable` tinyint(1) NOT NULL DEFAULT '1',
  `thuBegTime` time NOT NULL DEFAULT '00:00:00',
  `thuEndTime` time NOT NULL DEFAULT '00:00:00',
  `friCallable` tinyint(1) NOT NULL DEFAULT '1',
  `friBegTime` time NOT NULL DEFAULT '00:00:00',
  `friEndTime` time NOT NULL DEFAULT '00:00:00',
  `satCallable` tinyint(1) NOT NULL DEFAULT '1',
  `satBegTime` time NOT NULL DEFAULT '00:00:00',
  `satEndTime` time NOT NULL DEFAULT '00:00:00',
  `sunCallable` tinyint(1) NOT NULL DEFAULT '1',
  `sunBegTime` time NOT NULL DEFAULT '00:00:00',
  `sunEndTime` time NOT NULL DEFAULT '00:00:00',
  `scheduleReds` tinyint(1) NOT NULL DEFAULT '0',
  `transPlanned` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transCompleted` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transSuccess` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transDecline` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transLost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `scheduleCategoryId` int(11) NOT NULL DEFAULT '0',
  `assignedRestId` int(11) NOT NULL DEFAULT '0',
  `sectionId` int(11) NOT NULL DEFAULT '0',
  `shiftsCreated` tinyint(1) NOT NULL DEFAULT '0',
  `introCategoryId` int(11) NOT NULL DEFAULT '0',
  `introMinutes` int(4) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `projectId` (`projectId`),
  KEY `scheduleCategoryId` (`scheduleCategoryId`),
  CONSTRAINT `projecttask_1` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`),
  CONSTRAINT `projecttask_2` FOREIGN KEY (`scheduleCategoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4950 DEFAULT CHARSET=utf8;



# Dump of table projecttask_orderlog
# ------------------------------------------------------------

CREATE TABLE `projecttask_orderlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL DEFAULT '0',
  `no` int(2) NOT NULL DEFAULT '1',
  `orders` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL DEFAULT '',
  `transDate` date NOT NULL DEFAULT '0000-00-00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `projecttask_orderlog_2` (`taskId`),
  CONSTRAINT `projecttask_orderlog_2` FOREIGN KEY (`taskId`) REFERENCES `projecttask` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17875710 DEFAULT CHARSET=utf8;



# Dump of table projecttask_otype
# ------------------------------------------------------------

CREATE TABLE `projecttask_otype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL DEFAULT '0',
  `no` int(2) NOT NULL DEFAULT '1',
  `orders` int(11) NOT NULL DEFAULT '0',
  `budget` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `projecttask_otype_2` (`taskId`),
  CONSTRAINT `projecttask_otype_2` FOREIGN KEY (`taskId`) REFERENCES `projecttask` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44259 DEFAULT CHARSET=utf8;



# Dump of table qff_news
# ------------------------------------------------------------

CREATE TABLE `qff_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) NOT NULL DEFAULT 'en_US',
  `subject` varchar(80) NOT NULL,
  `htmlText` text NOT NULL,
  `pubDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pictureURL` varchar(75) NOT NULL,
  `asset_dir` varchar(100) NOT NULL DEFAULT '',
  `section` tinyint(1) NOT NULL DEFAULT '1',
  `createdByForceBy` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `createdByForceBy` (`createdByForceBy`)
) ENGINE=InnoDB AUTO_INCREMENT=952 DEFAULT CHARSET=utf8;



# Dump of table qff_news_comment
# ------------------------------------------------------------

CREATE TABLE `qff_news_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `htmlText` text NOT NULL,
  `pubDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `persId` int(11) NOT NULL DEFAULT '0',
  `newsId` int(11) NOT NULL DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qff_news_comment_1` (`newsId`),
  KEY `qff_news_comment_2` (`persId`),
  CONSTRAINT `qff_news_comment_1` FOREIGN KEY (`newsId`) REFERENCES `qff_news` (`id`),
  CONSTRAINT `qff_news_comment_2` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;



# Dump of table qmail
# ------------------------------------------------------------

CREATE TABLE `qmail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `readdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `toGroupId` int(11) NOT NULL DEFAULT '0',
  `moderated` tinyint(1) DEFAULT '1',
  `beenViewed` tinyint(1) NOT NULL DEFAULT '0',
  `asset_dir` varchar(100) NOT NULL DEFAULT '',
  `createdByForceBy` int(11) NOT NULL DEFAULT '0',
  `sendReceipt` tinyint(1) NOT NULL DEFAULT '0',
  `leaveAppId` int(11) NOT NULL DEFAULT '0',
  `isMarkedAsImportant` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qmail_1` (`toPersId`),
  KEY `qmail_2` (`fromPersId`),
  KEY `createdByForceBy` (`createdByForceBy`),
  KEY `toPersId` (`toPersId`,`moderated`,`beenViewed`,`createdByForceBy`,`senddate`),
  KEY `leaveAppId_idx` (`leaveAppId`) USING BTREE,
  CONSTRAINT `qmail_2` FOREIGN KEY (`fromPersId`) REFERENCES `employee` (`id`),
  CONSTRAINT `qmail_3` FOREIGN KEY (`toPersId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20513623 DEFAULT CHARSET=utf8;



# Dump of table qmail_archive
# ------------------------------------------------------------

CREATE TABLE `qmail_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `readdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `toGroupId` int(11) NOT NULL DEFAULT '0',
  `moderated` tinyint(1) DEFAULT '1',
  `beenViewed` tinyint(1) NOT NULL DEFAULT '0',
  `asset_dir` varchar(100) NOT NULL DEFAULT '',
  `restId` int(11) DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;



# Dump of table qmail_log
# ------------------------------------------------------------

CREATE TABLE `qmail_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(80) NOT NULL,
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `messageId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18233078 DEFAULT CHARSET=utf8;



# Dump of table qmailBAK
# ------------------------------------------------------------

CREATE TABLE `qmailBAK` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(80) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `readdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `toGroupId` int(11) NOT NULL DEFAULT '0',
  `moderated` tinyint(1) DEFAULT '1',
  `beenViewed` tinyint(1) NOT NULL DEFAULT '0',
  `asset_dir` varchar(100) NOT NULL DEFAULT '',
  `createdByForceBy` int(11) NOT NULL DEFAULT '0',
  `sendReceipt` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qmail_1` (`toPersId`),
  KEY `qmail_2` (`fromPersId`),
  KEY `createdByForceBy` (`createdByForceBy`),
  KEY `toPersId` (`toPersId`,`moderated`,`beenViewed`,`createdByForceBy`,`senddate`)
) ENGINE=InnoDB AUTO_INCREMENT=1442570 DEFAULT CHARSET=utf8;



# Dump of table qt_customer
# ------------------------------------------------------------

CREATE TABLE `qt_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `seoName` varchar(255) DEFAULT NULL,
  `ssoRoute` varchar(45) DEFAULT NULL,
  `contact` varchar(40) NOT NULL,
  `address1` varchar(50) NOT NULL DEFAULT '',
  `address2` varchar(50) NOT NULL DEFAULT '',
  `zip` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `cellPhone` varchar(20) NOT NULL DEFAULT '',
  `phoneNo` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `unitName` varchar(50) NOT NULL DEFAULT '',
  `unitNames` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `customMovies` tinyint(1) NOT NULL DEFAULT '0',
  `rootStoreId` int(11) NOT NULL DEFAULT '1',
  `dateCreated` date NOT NULL DEFAULT '0000-00-00',
  `validUpto` date NOT NULL DEFAULT '0000-00-00',
  `shareSetup` tinyint(1) NOT NULL DEFAULT '0',
  `shareStaff` tinyint(1) NOT NULL DEFAULT '0',
  `shareWithinDistrict` tinyint(1) NOT NULL DEFAULT '0',
  `selectByCombo` tinyint(1) NOT NULL DEFAULT '0',
  `isTrial` tinyint(1) NOT NULL DEFAULT '0',
  `reminderSent` tinyint(1) NOT NULL DEFAULT '0',
  `allGroupId` int(11) NOT NULL DEFAULT '0',
  `dealerId` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(15) NOT NULL DEFAULT 'en_US',
  `edition` tinyint(1) NOT NULL DEFAULT '2',
  `appBgColor` int(8) NOT NULL DEFAULT '4477535',
  `appTextColor` int(11) NOT NULL DEFAULT '16777215',
  `customerLogoUrl` varchar(100) NOT NULL,
  `loginLogoUrl` varchar(255) DEFAULT NULL,
  `md5Salt` varchar(20) NOT NULL DEFAULT '',
  `domain` varchar(20) NOT NULL DEFAULT '',
  `emailIntegration` tinyint(1) NOT NULL DEFAULT '0',
  `resizeTiming` int(5) NOT NULL DEFAULT '250',
  `nextProjectNo` int(10) NOT NULL DEFAULT '5000',
  `nextCrmCustomerNo` int(8) NOT NULL DEFAULT '4000',
  `zpNextCrmCustomerNo` tinyint(1) NOT NULL DEFAULT '1',
  `nextCrmDecideNo` int(8) NOT NULL DEFAULT '1000',
  `zpNextCrmDecideNo` tinyint(1) NOT NULL DEFAULT '0',
  `useDistricts` tinyint(1) NOT NULL DEFAULT '0',
  `nextBadgeNo` int(6) NOT NULL DEFAULT '1000',
  `checkInGroup` tinyint(1) NOT NULL DEFAULT '0',
  `restoreStatus` tinyint(1) NOT NULL DEFAULT '0',
  `useQFF_TA` tinyint(1) NOT NULL DEFAULT '0',
  `module_01` tinyint(1) NOT NULL DEFAULT '0',
  `module_02` tinyint(1) NOT NULL DEFAULT '0',
  `module_03` tinyint(1) NOT NULL DEFAULT '0',
  `module_04` tinyint(1) NOT NULL DEFAULT '0',
  `module_05` tinyint(1) NOT NULL DEFAULT '1',
  `module_06` tinyint(1) NOT NULL DEFAULT '1',
  `module_07` tinyint(1) NOT NULL DEFAULT '0',
  `module_08` tinyint(4) NOT NULL DEFAULT '1',
  `module_09` tinyint(4) NOT NULL DEFAULT '1',
  `localShiftTypes` tinyint(1) NOT NULL DEFAULT '0',
  `samlIdp` varchar(255) NOT NULL DEFAULT '',
  `samlAuthType` tinyint(1) NOT NULL DEFAULT '0',
  `samlAuthField` varchar(255) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `licenseIsPerUser` tinyint(1) NOT NULL DEFAULT '1',
  `userLicenses` int(11) DEFAULT NULL,
  `unitLicenses` int(11) DEFAULT NULL,
  `hideLogotype` tinyint(1) DEFAULT '0',
  `externalId` varchar(48) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `ssoRouteIndex` (`ssoRoute`),
  KEY `seoNameIndex` (`seoName`),
  KEY `groupId` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=3753 DEFAULT CHARSET=utf8;



# Dump of table qt_customer_changes_log
# ------------------------------------------------------------

CREATE TABLE `qt_customer_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=49476 DEFAULT CHARSET=latin1;



# Dump of table qt_customer_deletes_log
# ------------------------------------------------------------

CREATE TABLE `qt_customer_deletes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `qt_custId` int(11) NOT NULL,
  `objectName` varchar(128) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=733 DEFAULT CHARSET=latin1;



# Dump of table qt_customer_sftp
# ------------------------------------------------------------

CREATE TABLE `qt_customer_sftp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qtCustomerId` int(11) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `port` tinyint(4) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `pathToFile` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`qtCustomerId`),
  CONSTRAINT `fk_qt_customer_sftp_1` FOREIGN KEY (`qtCustomerId`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1456 DEFAULT CHARSET=latin1;



# Dump of table qt_customer_summary_log
# ------------------------------------------------------------

CREATE TABLE `qt_customer_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=8556 DEFAULT CHARSET=latin1;



# Dump of table qt_dealer
# ------------------------------------------------------------

CREATE TABLE `qt_dealer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `contact` varchar(40) NOT NULL,
  `address1` varchar(50) NOT NULL DEFAULT '',
  `address2` varchar(50) NOT NULL DEFAULT '',
  `zip` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `cellPhone` varchar(20) NOT NULL DEFAULT '',
  `phoneNo` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



# Dump of table redday_salarytype_rule
# ------------------------------------------------------------

CREATE TABLE `redday_salarytype_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reddayId` int(11) DEFAULT NULL,
  `stCode` int(11) NOT NULL DEFAULT '0',
  `fromTime` time NOT NULL DEFAULT '00:00:00',
  `fromType` tinyint(1) NOT NULL DEFAULT '1',
  `toTime` time NOT NULL DEFAULT '00:00:00',
  `toType` tinyint(1) NOT NULL DEFAULT '2',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amountType` tinyint(1) NOT NULL DEFAULT '0',
  `hasWorkRequirement` tinyint(1) NOT NULL DEFAULT '0',
  `workRequirement` tinyint(1) NOT NULL DEFAULT '0',
  `hasShiftRequirement` tinyint(1) NOT NULL DEFAULT '0',
  `shiftRequirement` tinyint(1) NOT NULL DEFAULT '0',
  `leaveTypeId` int(11) unsigned DEFAULT NULL,
  `hasAlternateSalary` tinyint(1) NOT NULL DEFAULT '0',
  `alternateStCode` int(11) NOT NULL DEFAULT '0',
  `alternateAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `alternateFromTime` time NOT NULL DEFAULT '00:00:00',
  `alternateFromType` tinyint(1) NOT NULL DEFAULT '1',
  `alternateToTime` time NOT NULL DEFAULT '00:00:00',
  `alternateToType` tinyint(1) NOT NULL DEFAULT '2',
  `replaceBaseSalaries` tinyint(1) NOT NULL DEFAULT '0',
  `replaceUTSalaries` tinyint(1) NOT NULL DEFAULT '0',
  `replaceATSalaries` tinyint(1) NOT NULL DEFAULT '0',
  `replaceOTSalaries` tinyint(1) NOT NULL DEFAULT '0',
  `replaceSickLeaveSalaries` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_redday_salarytype_rule_1_idx` (`reddayId`),
  KEY `fk_redday_salarytype_rule_2_idx` (`leaveTypeId`),
  CONSTRAINT `fk_redday_salarytype_rule_1` FOREIGN KEY (`reddayId`) REFERENCES `reddays` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_redday_salarytype_rule_2` FOREIGN KEY (`leaveTypeId`) REFERENCES `leavereason_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=292707 DEFAULT CHARSET=utf8;



# Dump of table reddays
# ------------------------------------------------------------

CREATE TABLE `reddays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `templateId` int(11) NOT NULL DEFAULT '0',
  `agreeId` int(11) NOT NULL DEFAULT '0',
  `redDate` date NOT NULL,
  `description` varchar(60) NOT NULL,
  `oType` tinyint(1) NOT NULL DEFAULT '2',
  `reduxBefRedDay` decimal(6,2) NOT NULL DEFAULT '0.00',
  `reduxBefRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `reduxRedDay` decimal(6,2) NOT NULL DEFAULT '100.00',
  `reduxRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `countAsScheduledHours` tinyint(1) NOT NULL DEFAULT '0',
  `countAsScheduledFrom` time NOT NULL DEFAULT '00:00:00',
  `countAsScheduledTo` time NOT NULL DEFAULT '00:00:00',
  `canScheduleTwoYears` tinyint(1) NOT NULL DEFAULT '1',
  `validity` int(1) NOT NULL DEFAULT '0',
  `createLeaveIfScheduled` tinyint(1) NOT NULL DEFAULT '0',
  `createLeaveReasonId` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `redDate` (`redDate`),
  KEY `templateId` (`templateId`),
  KEY `agreeId` (`agreeId`),
  KEY `restId` (`restId`,`templateId`,`agreeId`,`redDate`),
  KEY `fk_reddays_1_idx` (`createLeaveReasonId`)
) ENGINE=InnoDB AUTO_INCREMENT=1324755 DEFAULT CHARSET=utf8;



# Dump of table remainder
# ------------------------------------------------------------

CREATE TABLE `remainder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `remainderDate` date NOT NULL,
  `remainderType` tinyint(2) NOT NULL DEFAULT '1',
  `remainderText` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `remainder_2` (`restId`,`remainderDate`),
  CONSTRAINT `remainder_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=472 DEFAULT CHARSET=utf8;



# Dump of table reminder_event
# ------------------------------------------------------------

CREATE TABLE `reminder_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qt_customerId` int(11) NOT NULL,
  `restId` int(11) DEFAULT NULL,
  `name` varchar(80) NOT NULL,
  `event` int(3) NOT NULL DEFAULT '0',
  `sendOnTime` time NOT NULL,
  `sendOnPeriodType` tinyint(1) NOT NULL DEFAULT '0',
  `sendOnThreshold` int(11) NOT NULL DEFAULT '0',
  `sendOnThresholdType` int(11) NOT NULL DEFAULT '0',
  `offsetDays` int(11) NOT NULL DEFAULT '0',
  `offsetTiming` tinyint(3) NOT NULL DEFAULT '0',
  `during` int(11) NOT NULL DEFAULT '0',
  `duringUnit` tinyint(1) NOT NULL DEFAULT '0',
  `consecutive` tinyint(1) NOT NULL DEFAULT '0',
  `sendToAffectedEmployees` tinyint(1) NOT NULL DEFAULT '0',
  `sendToAffectedEmployeeManagers` tinyint(1) NOT NULL DEFAULT '0',
  `sendToStaffGroup` tinyint(1) NOT NULL DEFAULT '0',
  `sendFromPersId` int(11) DEFAULT NULL,
  `useStandardChannels` tinyint(1) NOT NULL DEFAULT '1',
  `useQmail` tinyint(1) NOT NULL DEFAULT '0',
  `useEmail` tinyint(1) NOT NULL DEFAULT '0',
  `useSMS` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `header` varchar(80) NOT NULL,
  `message` text NOT NULL,
  `lastExecuted` datetime DEFAULT NULL,
  `leaveReasonId` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `qt_customerId` (`qt_customerId`),
  KEY `restId` (`restId`),
  KEY `sendFromPersId` (`sendFromPersId`),
  KEY `leaveReasonId` (`leaveReasonId`),
  CONSTRAINT `reminder_event_ibfk_4` FOREIGN KEY (`leaveReasonId`) REFERENCES `leavereason` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=latin1;



# Dump of table reminder_event_empgroup
# ------------------------------------------------------------

CREATE TABLE `reminder_event_empgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `eventId` (`eventId`),
  KEY `groupId` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=6493 DEFAULT CHARSET=latin1;



# Dump of table reminder_log
# ------------------------------------------------------------

CREATE TABLE `reminder_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NOT NULL,
  `persId` int(11) NOT NULL,
  `sendOnTime` time NOT NULL,
  `sent` datetime NOT NULL,
  `sentByQmail` tinyint(1) DEFAULT '0',
  `sentByEmail` tinyint(1) DEFAULT '0',
  `sentBySMS` tinyint(1) DEFAULT '0',
  `eventPersId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eventId` (`eventId`),
  KEY `persId` (`persId`),
  KEY `eventPersId` (`eventPersId`)
) ENGINE=InnoDB AUTO_INCREMENT=385903 DEFAULT CHARSET=latin1;



# Dump of table report
# ------------------------------------------------------------

CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `typeId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `subscriptionInterval` int(11) DEFAULT NULL,
  `deviation` tinyint(1) DEFAULT NULL,
  `late` tinyint(1) DEFAULT NULL,
  `registered` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_report_customer` (`customerId`),
  KEY `fk_report_restaurant` (`restaurantId`),
  CONSTRAINT `fk_report_customer` FOREIGN KEY (`customerId`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_report_restaurant` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table report_activities
# ------------------------------------------------------------

CREATE TABLE `report_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `activityGroupId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_report_activities_activity` (`activityGroupId`),
  KEY `fk_report_activities_report` (`reportId`),
  CONSTRAINT `fk_report_activities_activity` FOREIGN KEY (`activityGroupId`) REFERENCES `activity` (`groupId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_report_activities_report` FOREIGN KEY (`reportId`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table report_attributes
# ------------------------------------------------------------

CREATE TABLE `report_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `attributeId` int(11) NOT NULL,
  `value` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table report_control_point_lists
# ------------------------------------------------------------

CREATE TABLE `report_control_point_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `listId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_report_control_point_lists` (`listId`),
  KEY `fk_report_control_point_lists_report` (`reportId`),
  CONSTRAINT `fk_report_control_point_lists` FOREIGN KEY (`listId`) REFERENCES `control_point_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_report_control_point_lists_report` FOREIGN KEY (`reportId`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table report_predefined_values
# ------------------------------------------------------------

CREATE TABLE `report_predefined_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `predefinedValueId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_report_predefined_values_predefined_values` (`predefinedValueId`),
  KEY `fk_report_predefined_values_report` (`reportId`),
  CONSTRAINT `fk_report_predefined_values_predefined_values` FOREIGN KEY (`predefinedValueId`) REFERENCES `predefined_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_report_predefined_values_report` FOREIGN KEY (`reportId`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table report_receivers
# ------------------------------------------------------------

CREATE TABLE `report_receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_report_subscription_receivers_employee` (`employeeId`),
  KEY `fk_report_subscription_receivers_report` (`reportId`),
  CONSTRAINT `fk_report_subscription_receivers_employee` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_report_subscription_receivers_report` FOREIGN KEY (`reportId`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table report_type
# ------------------------------------------------------------

CREATE TABLE `report_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `implementClass` varchar(100) DEFAULT NULL,
  `implementFunction` varchar(100) DEFAULT NULL,
  `implementFile` varchar(250) NOT NULL,
  `isSubscription` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table report_type_attributes
# ------------------------------------------------------------

CREATE TABLE `report_type_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeId` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rescue_data_schedule_king_food
# ------------------------------------------------------------

CREATE TABLE `rescue_data_schedule_king_food` (
  `id` int(11) NOT NULL DEFAULT '0',
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) CHARACTER SET utf8 NOT NULL,
  `changeReason` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(4) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table restaurant_activities_products
# ------------------------------------------------------------

CREATE TABLE `restaurant_activities_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityVersionId` int(11) NOT NULL,
  `predefinedValueId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_restaurant_activities` (`activityVersionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1470 DEFAULT CHARSET=utf8;



# Dump of table restaurant_activities_subgroups
# ------------------------------------------------------------

CREATE TABLE `restaurant_activities_subgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityVersionId` int(11) NOT NULL,
  `subgroupId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_restaurant_activities` (`activityVersionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1407 DEFAULT CHARSET=utf8;



# Dump of table restaurant_coordinates
# ------------------------------------------------------------

CREATE TABLE `restaurant_coordinates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurantId` int(11) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `radius` int(4) unsigned DEFAULT NULL,
  `comment` varchar(50) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restaurantId` (`restaurantId`),
  CONSTRAINT `restaurant_coordinates_ibfk_1` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5061 DEFAULT CHARSET=utf8;



# Dump of table restaurant_documents
# ------------------------------------------------------------

CREATE TABLE `restaurant_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurantid` int(11) NOT NULL,
  `documentid` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table restaurants
# ------------------------------------------------------------

CREATE TABLE `restaurants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `managerId` int(11) NOT NULL DEFAULT '0',
  `managerGroupId` int(11) NOT NULL DEFAULT '0',
  `scheduleGroupId` int(11) DEFAULT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '127.0.0.1',
  `API_key` varchar(64) NOT NULL DEFAULT '',
  `extApplicationId` varchar(30) NOT NULL DEFAULT '',
  `maxHourDay` decimal(6,2) NOT NULL DEFAULT '8.00',
  `maxHourWeek` decimal(6,2) NOT NULL DEFAULT '40.00',
  `minShift` int(2) NOT NULL DEFAULT '1',
  `minRest` int(2) NOT NULL DEFAULT '13',
  `weekStart` tinyint(1) NOT NULL DEFAULT '1',
  `atDayBreak` tinyint(1) NOT NULL DEFAULT '1',
  `maxDays` int(2) NOT NULL DEFAULT '5',
  `minFreeDays` int(2) NOT NULL DEFAULT '2',
  `reduxBefRedDay` decimal(6,2) NOT NULL DEFAULT '0.00',
  `reduxBefRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `reduxRedDay` decimal(6,2) NOT NULL DEFAULT '100.00',
  `reduxRedDayType` tinyint(1) NOT NULL DEFAULT '0',
  `maxCap` int(4) NOT NULL DEFAULT '0',
  `qt_customerId` int(11) NOT NULL DEFAULT '1',
  `publishedTo` date NOT NULL DEFAULT '2008-06-01',
  `publishAutoIntervalDays` int(11) unsigned DEFAULT NULL,
  `publishAutoIntervalMonths` int(11) unsigned DEFAULT NULL,
  `publishedReady` date DEFAULT NULL,
  `smsCost` int(10) NOT NULL DEFAULT '57',
  `smsGateWay` varchar(50) NOT NULL,
  `smsGateWayType` int(2) NOT NULL DEFAULT '0',
  `smsGateWayPrefix` varchar(5) NOT NULL DEFAULT '',
  `smsGateWayAccount` varchar(100) NOT NULL DEFAULT '',
  `smsGateWayUser` varchar(50) NOT NULL DEFAULT '',
  `smsGateWayPassword` varchar(50) NOT NULL DEFAULT '',
  `smsGateWaySender` varchar(50) NOT NULL DEFAULT '',
  `smsGateWayNumber` varchar(50) DEFAULT '',
  `TAGateWayLevel` tinyint(1) NOT NULL DEFAULT '1',
  `TAGateWay` varchar(255) NOT NULL,
  `TAGateWay2` varchar(255) NOT NULL,
  `TAGateWay3` varchar(255) NOT NULL,
  `useIFrame` tinyint(1) NOT NULL DEFAULT '1',
  `stColoring` tinyint(1) NOT NULL DEFAULT '0',
  `stColoring2` tinyint(1) NOT NULL DEFAULT '0',
  `unassBgColor` int(10) NOT NULL DEFAULT '16763904',
  `unassTextColor` int(10) NOT NULL DEFAULT '0',
  `assBgColor` int(10) NOT NULL DEFAULT '16777215',
  `assTextColor` int(10) NOT NULL DEFAULT '0',
  `noiBgColor` int(10) NOT NULL DEFAULT '39168',
  `noiTextColor` int(10) NOT NULL DEFAULT '16777215',
  `fixBgColor` int(10) NOT NULL DEFAULT '13209',
  `fixTextColor` int(10) NOT NULL DEFAULT '16777215',
  `othUnitBgColor` int(10) NOT NULL DEFAULT '13421772',
  `othUnitTextColor` int(10) NOT NULL DEFAULT '255',
  `vacBgColor` int(10) NOT NULL DEFAULT '13107',
  `vacTextColor` int(10) NOT NULL DEFAULT '16777215',
  `prelvacBgColor` int(10) NOT NULL DEFAULT '13421721',
  `prelvacTextColor` int(10) NOT NULL DEFAULT '0',
  `lappBgColor` int(10) NOT NULL DEFAULT '39372',
  `lappTextColor` int(10) NOT NULL DEFAULT '16777215',
  `otime1BgColor` int(10) NOT NULL DEFAULT '16777062',
  `otime1TextColor` int(10) NOT NULL DEFAULT '3355443',
  `otime2BgColor` int(10) NOT NULL DEFAULT '16764006',
  `otime2TextColor` int(10) NOT NULL DEFAULT '3355443',
  `otime3BgColor` int(10) NOT NULL DEFAULT '16750950',
  `otime3TextColor` int(10) NOT NULL DEFAULT '3355443',
  `otime4BgColor` int(10) NOT NULL DEFAULT '16724838',
  `otime4TextColor` int(10) NOT NULL DEFAULT '0',
  `otime5BgColor` int(10) NOT NULL DEFAULT '16777062',
  `otime5TextColor` int(10) NOT NULL DEFAULT '3355443',
  `otime6BgColor` int(10) NOT NULL DEFAULT '16777062',
  `otime6TextColor` int(10) NOT NULL DEFAULT '3355443',
  `otime7BgColor` int(10) NOT NULL DEFAULT '16777062',
  `otime7TextColor` int(10) NOT NULL DEFAULT '3355443',
  `otime8BgColor` int(10) NOT NULL DEFAULT '16777062',
  `otime8TextColor` int(10) NOT NULL DEFAULT '3355443',
  `noiShowWeekly` tinyint(1) NOT NULL DEFAULT '1',
  `noiShowDaily` tinyint(1) NOT NULL DEFAULT '1',
  `breakBgColor` int(10) NOT NULL DEFAULT '0',
  `use24clock` tinyint(1) NOT NULL DEFAULT '1',
  `useScheduledStaff` tinyint(1) NOT NULL DEFAULT '1',
  `useLoginId` tinyint(1) NOT NULL DEFAULT '0',
  `useWebpunch` tinyint(1) NOT NULL DEFAULT '1',
  `notifyNewShift` tinyint(1) NOT NULL DEFAULT '1',
  `notifyDeleteShift` tinyint(1) NOT NULL DEFAULT '1',
  `notifyChangeShift` tinyint(1) NOT NULL DEFAULT '1',
  `notifyTakeShift` tinyint(1) NOT NULL DEFAULT '1',
  `notifyManager` tinyint(1) NOT NULL DEFAULT '1',
  `notifyShiftRequest` tinyint(1) NOT NULL DEFAULT '0',
  `notifySwapRequest` tinyint(1) NOT NULL DEFAULT '0',
  `sendReceipt` int(1) NOT NULL DEFAULT '0',
  `allGroupId` int(11) NOT NULL DEFAULT '3',
  `dateFormat` varchar(20) NOT NULL DEFAULT 'YYYY-MM-DD',
  `firstDayOfWeek` int(1) NOT NULL DEFAULT '1',
  `daysExtraction` int(2) NOT NULL DEFAULT '0',
  `currency` varchar(3) NOT NULL DEFAULT 'SEK',
  `autoLogOut` varchar(2) NOT NULL DEFAULT '5',
  `nextBadgeNo` int(6) NOT NULL DEFAULT '1000',
  `noBreaks` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `allowBooking` tinyint(1) NOT NULL DEFAULT '1',
  `allowBookingOtherUnits` tinyint(1) NOT NULL DEFAULT '0',
  `allowSwap` tinyint(1) NOT NULL DEFAULT '1',
  `allowSwapChoice` tinyint(1) NOT NULL DEFAULT '1',
  `salesMargin` int(4) NOT NULL DEFAULT '100',
  `staffMail` tinyint(1) NOT NULL DEFAULT '1',
  `selectSection` tinyint(1) NOT NULL DEFAULT '0',
  `premium1` int(4) NOT NULL DEFAULT '100',
  `premium2` int(4) NOT NULL DEFAULT '100',
  `premium3` int(4) NOT NULL DEFAULT '100',
  `premium4` int(4) NOT NULL DEFAULT '100',
  `premium5` int(4) NOT NULL DEFAULT '100',
  `premium6` int(4) NOT NULL DEFAULT '100',
  `premium7` int(4) NOT NULL DEFAULT '100',
  `premium8` int(4) NOT NULL DEFAULT '100',
  `noOfPremiums` tinyint(1) NOT NULL DEFAULT '4',
  `locale` varchar(15) NOT NULL DEFAULT 'en_US',
  `baseDate` date NOT NULL DEFAULT '2009-02-01',
  `isTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `districtId` int(11) NOT NULL DEFAULT '1',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `showHistoricalShifts` tinyint(1) NOT NULL DEFAULT '1',
  `daysOfVisibility` int(3) NOT NULL DEFAULT '60',
  `anonomysShifts` tinyint(1) NOT NULL DEFAULT '0',
  `hideOldCollShift` tinyint(1) NOT NULL DEFAULT '1',
  `daySum_1` int(2) NOT NULL DEFAULT '6',
  `daySum_2` int(2) NOT NULL DEFAULT '7',
  `daySum_3` int(2) NOT NULL DEFAULT '1',
  `daySum_4` int(2) NOT NULL DEFAULT '2',
  `daySum_5` int(2) NOT NULL DEFAULT '3',
  `daySum_6` int(2) NOT NULL DEFAULT '4',
  `dayVariable_1` int(11) NOT NULL DEFAULT '0',
  `dayVariable_2` int(11) NOT NULL DEFAULT '0',
  `dayVariable_3` int(11) NOT NULL DEFAULT '0',
  `chngPword` tinyint(1) NOT NULL DEFAULT '1',
  `requirePin` tinyint(1) NOT NULL DEFAULT '0',
  `useStaffCosting` tinyint(1) NOT NULL DEFAULT '0',
  `alwaysCheckForSalaryOnBankHolidays` tinyint(1) NOT NULL DEFAULT '0',
  `socialCost` decimal(5,2) NOT NULL DEFAULT '0.00',
  `excelVersion` int(4) NOT NULL DEFAULT '2003',
  `narrowLayout` tinyint(4) NOT NULL DEFAULT '0',
  `minHrsWeek` decimal(6,2) NOT NULL DEFAULT '0.00',
  `minRestWeek` decimal(6,2) NOT NULL DEFAULT '0.00',
  `minWeekEndYear` int(2) NOT NULL DEFAULT '0',
  `seqDays` int(3) NOT NULL DEFAULT '0',
  `seqMaxDays` int(3) NOT NULL DEFAULT '0',
  `seqMaxHrs` int(3) NOT NULL DEFAULT '0',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `maxTimeWoBreak` decimal(6,2) NOT NULL DEFAULT '8.00',
  `minBreak` int(2) NOT NULL DEFAULT '0',
  `maxBreak` int(4) NOT NULL DEFAULT '0',
  `scheduleLockPeriod` int(3) NOT NULL DEFAULT '0',
  `scheduleLockPeriodType` tinyint(1) NOT NULL DEFAULT '0',
  `dayBreak` time NOT NULL DEFAULT '00:00:00',
  `timeStepper` int(2) NOT NULL DEFAULT '15',
  `schedWeekEnds` tinyint(1) NOT NULL DEFAULT '1',
  `threeSixtyYear` tinyint(1) NOT NULL DEFAULT '1',
  `fiveDayWeek` tinyint(1) NOT NULL DEFAULT '1',
  `fcastConfig` varchar(30) NOT NULL DEFAULT '',
  `staffSorting` tinyint(1) NOT NULL DEFAULT '0',
  `allowParallelLeaves` tinyint(4) NOT NULL DEFAULT '0',
  `profileId` int(11) DEFAULT NULL,
  `boxAddress` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipCode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `companyAddress` varchar(255) DEFAULT NULL,
  `companyZipCode` varchar(255) DEFAULT NULL,
  `companyCity` varchar(255) DEFAULT NULL,
  `IDNumber` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) NOT NULL,
  `faxNumber` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `municipality` varchar(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `companyOrganizationNumber` varchar(255) NOT NULL,
  `companyRealEstateNumber` varchar(255) NOT NULL,
  `companyEstimatedEmployees` varchar(255) NOT NULL,
  `companyOtherInfo` varchar(255) NOT NULL,
  `buyerFirstName` varchar(255) NOT NULL,
  `buyerLastName` varchar(255) NOT NULL,
  `buyerPhone` varchar(255) NOT NULL,
  `buyerEmail` varchar(255) NOT NULL,
  `buyerOtherInfo` varchar(255) NOT NULL,
  `qSupFirstName` varchar(255) DEFAULT NULL,
  `qSupLastName` varchar(255) DEFAULT NULL,
  `qSupPhone` varchar(255) DEFAULT NULL,
  `qSupEmail` varchar(255) DEFAULT NULL,
  `qSupOtherInfo` varchar(255) DEFAULT NULL,
  `buyerQualityEmployeeId` varchar(255) NOT NULL,
  `qualityName` varchar(255) NOT NULL,
  `qualityPhone` varchar(255) NOT NULL,
  `qualityEmail` varchar(255) NOT NULL,
  `qualityComment` varchar(255) NOT NULL,
  `showSimEmpPortal` tinyint(1) NOT NULL DEFAULT '0',
  `addrLAtoManager` tinyint(1) NOT NULL DEFAULT '0',
  `removeNOIwhenBooked` tinyint(1) NOT NULL DEFAULT '0',
  `utThreshold` decimal(10,2) NOT NULL DEFAULT '0.00',
  `utThresholdType` int(3) NOT NULL DEFAULT '0',
  `approveBookingByOtherUnit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `restaurants_1` (`qt_customerId`),
  KEY `API_key` (`API_key`),
  KEY `districtIndex` (`districtId`),
  CONSTRAINT `restaurants_1` FOREIGN KEY (`qt_customerId`) REFERENCES `qt_customer` (`id`),
  CONSTRAINT `restaurants_ibfk_1` FOREIGN KEY (`districtId`) REFERENCES `district` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14814 DEFAULT CHARSET=utf8;



# Dump of table restaurants_changes_log
# ------------------------------------------------------------

CREATE TABLE `restaurants_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=913909 DEFAULT CHARSET=latin1;



# Dump of table restaurants_deletes_log
# ------------------------------------------------------------

CREATE TABLE `restaurants_deletes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `qt_custId` int(11) NOT NULL,
  `objectName` varchar(128) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14304 DEFAULT CHARSET=latin1;



# Dump of table restaurants_summary_log
# ------------------------------------------------------------

CREATE TABLE `restaurants_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=34127 DEFAULT CHARSET=latin1;



# Dump of table restaurantscheduleminlength
# ------------------------------------------------------------

CREATE TABLE `restaurantscheduleminlength` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `length` float NOT NULL,
  `fromTime` varchar(8) NOT NULL,
  `toTime` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurantIndex` (`restId`),
  CONSTRAINT `restaurant_fk` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=55781 DEFAULT CHARSET=latin1;



# Dump of table restgroup
# ------------------------------------------------------------

CREATE TABLE `restgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qt_customerId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_restgroup_1_idx` (`qt_customerId`),
  CONSTRAINT `fk_qt_customer` FOREIGN KEY (`qt_customerId`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;



# Dump of table restgroupmember
# ------------------------------------------------------------

CREATE TABLE `restgroupmember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restgroupId` int(11) NOT NULL,
  `restaurantId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restgroupmember_1` (`restgroupId`),
  KEY `restgroupmember_2` (`restaurantId`),
  CONSTRAINT `restgroupmember_1` FOREIGN KEY (`restgroupId`) REFERENCES `restgroup` (`id`),
  CONSTRAINT `restgroupmember_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8;



# Dump of table risk_analysis
# ------------------------------------------------------------

CREATE TABLE `risk_analysis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `activityGroupId` int(11) NOT NULL,
  `processStepId` int(11) NOT NULL,
  `healthRiskId` int(11) NOT NULL,
  `reasonText` text COLLATE utf8_unicode_ci,
  `consequence` int(11) DEFAULT NULL,
  `probability` int(11) DEFAULT NULL,
  `requiresDecisionTree` tinyint(1) NOT NULL DEFAULT '0',
  `gfCPP` int(11) NOT NULL DEFAULT '0',
  `question1` text COLLATE utf8_unicode_ci,
  `question2` text COLLATE utf8_unicode_ci,
  `question3` text COLLATE utf8_unicode_ci,
  `question4` text COLLATE utf8_unicode_ci,
  `question1Value` int(11) DEFAULT '0',
  `question2Value` int(11) DEFAULT '0',
  `question3Value` int(11) DEFAULT '0',
  `question4Value` int(11) DEFAULT '0',
  `isTemplate` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_risk_analysis_predefined_process_step` (`processStepId`),
  KEY `fk_risk_analysis_predefined_risk` (`healthRiskId`),
  KEY `idx_activity` (`activityGroupId`),
  CONSTRAINT `fk_risk_analysis_activity` FOREIGN KEY (`activityGroupId`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=331 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table risk_analysis_description
# ------------------------------------------------------------

CREATE TABLE `risk_analysis_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityGroupId` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `risk_analysis_description_ibfk_1` (`activityGroupId`),
  CONSTRAINT `risk_analysis_description_ibfk_1` FOREIGN KEY (`activityGroupId`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table risk_analysis_routine
# ------------------------------------------------------------

CREATE TABLE `risk_analysis_routine` (
  `riskAnalysisId` int(11) NOT NULL,
  `documentId` int(11) NOT NULL,
  `documentGroupId` int(11) NOT NULL,
  `isSpecific` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`riskAnalysisId`,`documentId`),
  KEY `fk_document` (`documentId`),
  KEY `fk_riskAnalysis` (`riskAnalysisId`),
  CONSTRAINT `fk_riskAnalysis` FOREIGN KEY (`riskAnalysisId`) REFERENCES `risk_analysis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table risk_analysis_version_collection
# ------------------------------------------------------------

CREATE TABLE `risk_analysis_version_collection` (
  `id` int(11) NOT NULL,
  `riskAnalysisId` int(11) NOT NULL,
  `submodule_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`riskAnalysisId`),
  KEY `fk_riskAnalysisCollection_riskAnalysis` (`riskAnalysisId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table risk_template
# ------------------------------------------------------------

CREATE TABLE `risk_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `reasonText` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `question1` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `question2` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `question3` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `question4` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `question1Value` int(11) NOT NULL DEFAULT '0',
  `question2Value` int(11) NOT NULL DEFAULT '0',
  `question3Value` int(11) NOT NULL DEFAULT '0',
  `question4Value` int(11) NOT NULL DEFAULT '0',
  `customerId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;



# Dump of table rollback
# ------------------------------------------------------------

CREATE TABLE `rollback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rType` char(1) NOT NULL,
  `restId` int(11) NOT NULL,
  `persId` int(11) NOT NULL,
  `scheduleId` int(11) NOT NULL,
  `rbackId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rbKey` (`rType`,`restId`,`persId`),
  KEY `rollback_1` (`restId`),
  KEY `rollback_2` (`persId`),
  CONSTRAINT `rollback_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `rollback_2` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=709719 DEFAULT CHARSET=utf8;



# Dump of table salaryfile
# ------------------------------------------------------------

CREATE TABLE `salaryfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `useJavaService` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;



# Dump of table salarytypes
# ------------------------------------------------------------

CREATE TABLE `salarytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) DEFAULT '0',
  `stCode` int(11) DEFAULT '0',
  `salaryCode` varchar(20) NOT NULL DEFAULT '',
  `localLabel` varchar(50) NOT NULL DEFAULT '',
  `trackerId` int(11) NOT NULL DEFAULT '0',
  `trackerOperator` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor` decimal(5,2) NOT NULL DEFAULT '1.00',
  `trackerId2` int(11) NOT NULL DEFAULT '0',
  `trackerOperator2` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor2` decimal(5,2) NOT NULL DEFAULT '1.00',
  `trackerId3` int(11) NOT NULL DEFAULT '0',
  `trackerOperator3` tinyint(1) NOT NULL DEFAULT '0',
  `trackerFactor3` decimal(5,2) NOT NULL DEFAULT '1.00',
  `requiresApproval` tinyint(1) NOT NULL DEFAULT '0',
  `approvedByDefault` tinyint(1) NOT NULL DEFAULT '0',
  `round` int(2) NOT NULL DEFAULT '0',
  `roundCeil` tinyint(1) NOT NULL DEFAULT '1',
  `category` int(2) DEFAULT '0',
  `costType` int(3) NOT NULL DEFAULT '0',
  `incInTotalCost` tinyint(1) NOT NULL DEFAULT '1',
  `salaryCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `minimumValue` decimal(5,2) NOT NULL DEFAULT '0.00',
  `maximumValue` decimal(5,2) NOT NULL DEFAULT '0.00',
  `transferToPayroll` tinyint(1) NOT NULL DEFAULT '1',
  `utReduction` decimal(5,2) NOT NULL DEFAULT '0.00',
  `roundingSalaryTypeId` int(11) unsigned NOT NULL DEFAULT '0',
  `allowManualAdd` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `salarytypes_1` (`restId`),
  KEY `salarytypes_2` (`stCode`),
  CONSTRAINT `salarytypes_ibfk_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1527916 DEFAULT CHARSET=utf8;



# Dump of table salesdata
# ------------------------------------------------------------

CREATE TABLE `salesdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `section` int(11) NOT NULL DEFAULT '0',
  `variableType` int(2) NOT NULL DEFAULT '0',
  `salesdataDate` date NOT NULL,
  `isTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `templateName` varchar(50) NOT NULL,
  `salesTotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `comments` varchar(200) NOT NULL,
  `year` year(4) NOT NULL DEFAULT '2008',
  `month` int(2) NOT NULL DEFAULT '0',
  `week` int(2) NOT NULL DEFAULT '0',
  `weekday` tinyint(1) NOT NULL DEFAULT '0',
  `salesdata00_01` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata01_02` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata02_03` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata03_04` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata04_05` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata05_06` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata06_07` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata07_08` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata08_09` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata09_10` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata10_11` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata11_12` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata12_13` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata13_14` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata14_15` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata15_16` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata16_17` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata17_18` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata18_19` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata19_20` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata20_21` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata21_22` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata22_23` decimal(10,2) NOT NULL DEFAULT '0.00',
  `salesdata23_24` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `salesdata_2` (`restId`,`salesdataDate`),
  KEY `salesdata_3` (`week`,`weekday`,`restId`),
  CONSTRAINT `salesdata_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41273970 DEFAULT CHARSET=utf8;



# Dump of table schedule
# ------------------------------------------------------------

CREATE TABLE `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `changeReason` varchar(60) NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(4) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `d_search` (`restId`,`begDate`,`begTime`),
  KEY `persindex` (`persId`),
  KEY `fixSchedIndex` (`fixSchedId`),
  KEY `projectTaskId` (`projectTaskId`),
  KEY `fxxSchedId` (`fxxSchedId`),
  KEY `categoryId` (`categoryId`),
  KEY `grabbId` (`grabbId`),
  KEY `persId` (`persId`,`begDate`,`begTime`),
  KEY `persId_2` (`persId`,`begDate`,`begTime`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `agreementId` (`agreementId`),
  KEY `begDate` (`begDate`),
  KEY `endDate` (`endDate`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `orgScheduleId` (`orgScheduleId`),
  KEY `section` (`section`),
  CONSTRAINT `schedule_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=97440513 DEFAULT CHARSET=utf8;



# Dump of table schedule_archive
# ------------------------------------------------------------

CREATE TABLE `schedule_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `changeReason` varchar(60) NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(4) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `orgScheduleId` (`orgScheduleId`),
  CONSTRAINT `schedule_archive_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_archive_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4680623 DEFAULT CHARSET=utf8;



# Dump of table schedule_booking_log
# ------------------------------------------------------------

CREATE TABLE `schedule_booking_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL,
  `shiftEmployeeId` int(11) NOT NULL,
  `bookEmployeeId` int(11) NOT NULL,
  `bookingType` int(3) NOT NULL,
  `action` int(3) NOT NULL,
  `actionEmployeeId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `scheduleId` (`scheduleId`),
  KEY `shiftEmployeeId` (`shiftEmployeeId`),
  KEY `bookEmployeeId` (`bookEmployeeId`),
  KEY `actionEmployeeId` (`actionEmployeeId`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;



# Dump of table schedule_changes
# ------------------------------------------------------------

CREATE TABLE `schedule_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL,
  `updatedBy` varchar(50) CHARACTER SET utf8 NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `scheduleIdIndex` (`scheduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=2226958 DEFAULT CHARSET=latin1;



# Dump of table schedule_chfl
# ------------------------------------------------------------

CREATE TABLE `schedule_chfl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `changeReason` varchar(60) NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `leaveAppId` int(11) NOT NULL DEFAULT '0',
  `payrollTransf` tinyint(1) NOT NULL DEFAULT '0',
  `beenTransfered` tinyint(1) NOT NULL DEFAULT '0',
  `lockedTimetrackers` tinyint(1) NOT NULL DEFAULT '0',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `dayCount` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `persindex` (`persId`),
  KEY `fixSchedId` (`fixSchedId`),
  KEY `fxxSchedId` (`fxxSchedId`),
  KEY `leaveAppId` (`leaveAppId`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `orgScheduleId` (`orgScheduleId`),
  CONSTRAINT `schedule_chfl_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `schedule_chfl_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_chfl_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9914389 DEFAULT CHARSET=utf8;



# Dump of table schedule_chfl_deleted
# ------------------------------------------------------------

CREATE TABLE `schedule_chfl_deleted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `changeReason` varchar(60) NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `leaveAppId` int(11) NOT NULL DEFAULT '0',
  `payrollTransf` tinyint(1) NOT NULL DEFAULT '0',
  `beenTransfered` tinyint(1) NOT NULL DEFAULT '0',
  `lockedTimetrackers` tinyint(1) NOT NULL DEFAULT '0',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `dayCount` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `persindex` (`persId`),
  KEY `fixSchedId` (`fixSchedId`),
  KEY `fxxSchedId` (`fxxSchedId`),
  KEY `leaveAppId` (`leaveAppId`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `orgScheduleId` (`orgScheduleId`),
  CONSTRAINT `schedule_chfl_deleted_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `schedule_chfl_deleted_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_chfl_deleted_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9914388 DEFAULT CHARSET=utf8;



# Dump of table schedule_chfl_self_assigned
# ------------------------------------------------------------

CREATE TABLE `schedule_chfl_self_assigned` (
  `scheduleChflId` int(11) NOT NULL,
  `persId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`scheduleChflId`),
  KEY `persId` (`persId`),
  CONSTRAINT `schedule_chfl_self_assigned_ibfk_1` FOREIGN KEY (`scheduleChflId`) REFERENCES `schedule_chfl` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table schedule_deleted
# ------------------------------------------------------------

CREATE TABLE `schedule_deleted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `changeReason` varchar(60) NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `d_search` (`restId`,`begDate`,`begTime`),
  KEY `persindex` (`persId`),
  KEY `categoryId` (`categoryId`),
  KEY `grabbId` (`grabbId`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `orgScheduleId` (`orgScheduleId`),
  CONSTRAINT `schedule_deleted_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `schedule_deleted_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_deleted_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=97440469 DEFAULT CHARSET=utf8;



# Dump of table schedule_empqueu
# ------------------------------------------------------------

CREATE TABLE `schedule_empqueu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '0',
  `employeeId` int(11) NOT NULL DEFAULT '0',
  `swapedScheduleId` int(11) NOT NULL DEFAULT '0',
  `fulfills` tinyint(1) NOT NULL DEFAULT '1',
  `approveRestId` int(11) NOT NULL DEFAULT '0',
  `denyMessageToShiftOwner` tinyint(1) NOT NULL DEFAULT '0',
  `requestType` int(3) NOT NULL DEFAULT '0',
  `approvedByShiftOwner` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `scheduleId` (`scheduleId`),
  KEY `employeeId` (`employeeId`),
  KEY `approveRestId` (`approveRestId`),
  CONSTRAINT `schedule_empqueu_ibfk_1` FOREIGN KEY (`scheduleId`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3003609 DEFAULT CHARSET=latin1;



# Dump of table schedule_export
# ------------------------------------------------------------

CREATE TABLE `schedule_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `scheduleId` int(11) NOT NULL,
  `startTime` datetime NOT NULL,
  `endTime` datetime NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table schedule_fxdref
# ------------------------------------------------------------

CREATE TABLE `schedule_fxdref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '0',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `persId` int(11) NOT NULL DEFAULT '0',
  `changerId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `schedule_fxdref_1` (`scheduleId`),
  KEY `schedule_fxdref_2` (`fixSchedId`)
) ENGINE=InnoDB AUTO_INCREMENT=35295698 DEFAULT CHARSET=utf8;



# Dump of table schedule_log
# ------------------------------------------------------------

CREATE TABLE `schedule_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `recordType` tinyint(1) DEFAULT '0',
  `scheduleId` int(11) DEFAULT '0',
  `fieldNo` int(2) DEFAULT '0',
  `numOldValue` int(11) DEFAULT '0',
  `numNewValue` int(11) DEFAULT '0',
  `strOldValue` varchar(150) NOT NULL,
  `strNewValue` varchar(150) NOT NULL,
  `changerId` int(11) DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `restIdts` (`restId`,`ts`),
  KEY `scheduleId` (`scheduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=296671043 DEFAULT CHARSET=utf8;



# Dump of table schedule_rback
# ------------------------------------------------------------

CREATE TABLE `schedule_rback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `changeReason` varchar(60) NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `orgScheduleId` (`orgScheduleId`),
  CONSTRAINT `schedule_rback_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_rback_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=190935 DEFAULT CHARSET=utf8;



# Dump of table schedule_self_assigned
# ------------------------------------------------------------

CREATE TABLE `schedule_self_assigned` (
  `scheduleId` int(11) NOT NULL,
  `persId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`scheduleId`),
  KEY `persId` (`persId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table schedule_skills
# ------------------------------------------------------------

CREATE TABLE `schedule_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catId` int(11) NOT NULL,
  `skillId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `schedSort` (`catId`,`skillId`),
  KEY `schedule_skills_2` (`skillId`),
  CONSTRAINT `schedule_skills_1` FOREIGN KEY (`catId`) REFERENCES `schedulecategory` (`id`),
  CONSTRAINT `schedule_skills_2` FOREIGN KEY (`skillId`) REFERENCES `skills` (`skillId`)
) ENGINE=InnoDB AUTO_INCREMENT=207768 DEFAULT CHARSET=utf8;



# Dump of table schedule_template_shifts
# ------------------------------------------------------------

CREATE TABLE `schedule_template_shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL,
  `weekDay` tinyint(1) NOT NULL DEFAULT '0',
  `begTime` time NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL,
  `endBreak` time NOT NULL,
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(4) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `countScheduleHours` tinyint(1) NOT NULL DEFAULT '1',
  `countPunchHours` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `templateId` (`templateId`),
  KEY `categoryId` (`categoryId`),
  KEY `persId` (`persId`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `section` (`section`),
  CONSTRAINT `schedule_template_shifts_1` FOREIGN KEY (`templateId`) REFERENCES `schedule_templates` (`id`),
  CONSTRAINT `schedule_template_shifts_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_template_shifts_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1278431 DEFAULT CHARSET=utf8;



# Dump of table schedule_templates
# ------------------------------------------------------------

CREATE TABLE `schedule_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `templateType` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `schedule_templates_1` (`restId`),
  CONSTRAINT `schedule_templates_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35800 DEFAULT CHARSET=utf8;



# Dump of table schedule_work
# ------------------------------------------------------------

CREATE TABLE `schedule_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL DEFAULT '00:00:00',
  `endBreak` time NOT NULL DEFAULT '00:00:00',
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `breakIsWorkTime` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `grabbAble` tinyint(1) NOT NULL DEFAULT '0',
  `grabbId` int(11) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `changeReason` varchar(60) NOT NULL DEFAULT '',
  `section` int(11) NOT NULL DEFAULT '0',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(4) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `costEstimate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pDiff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `shiftChanged` tinyint(1) NOT NULL DEFAULT '0',
  `projectTaskId` int(11) NOT NULL DEFAULT '0',
  `fxxSchedId` int(11) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `chkOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `genOverTime` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `genMoreTime` tinyint(1) NOT NULL DEFAULT '0',
  `moreTimeHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lateScheduleChange` tinyint(1) NOT NULL DEFAULT '0',
  `chkBreakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakDayrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `chkBreakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrest` tinyint(1) NOT NULL DEFAULT '0',
  `breakWeekrestHrs` decimal(10,2) NOT NULL DEFAULT '0.00',
  `udf_value_id` int(11) unsigned DEFAULT NULL,
  `orgScheduleId` int(11) unsigned DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `restId_2` (`restId`,`begDate`,`begTime`),
  KEY `categoryId` (`categoryId`),
  KEY `crmSchedId` (`crmSchedId`),
  KEY `agreementId` (`agreementId`),
  KEY `udf_value_id` (`udf_value_id`),
  KEY `orgScheduleId` (`orgScheduleId`),
  CONSTRAINT `schedule_work_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `schedule_work_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `schedule_work_ibfk_2` FOREIGN KEY (`udf_value_id`) REFERENCES `user_defined_fields_values` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1203135 DEFAULT CHARSET=utf8;



# Dump of table schedulecategory
# ------------------------------------------------------------

CREATE TABLE `schedulecategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost4` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost5` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost6` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost7` decimal(10,2) NOT NULL DEFAULT '0.00',
  `premiumCost8` decimal(10,2) NOT NULL DEFAULT '0.00',
  `useColors` tinyint(1) NOT NULL DEFAULT '0',
  `bgColor` int(10) NOT NULL DEFAULT '16777215',
  `textColor` int(10) NOT NULL DEFAULT '0',
  `section` int(11) NOT NULL DEFAULT '0',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `breakType` tinyint(1) NOT NULL DEFAULT '0',
  `begBreak` time NOT NULL DEFAULT '00:00:00',
  `endBreak` time NOT NULL DEFAULT '00:00:00',
  `begBreak2` time NOT NULL DEFAULT '00:00:00',
  `endBreak2` time NOT NULL DEFAULT '00:00:00',
  `begBreak3` time NOT NULL DEFAULT '00:00:00',
  `endBreak3` time NOT NULL DEFAULT '00:00:00',
  `begBreak4` time NOT NULL DEFAULT '00:00:00',
  `endBreak4` time NOT NULL DEFAULT '00:00:00',
  `comment` varchar(50) CHARACTER SET latin1 NOT NULL,
  `rank` int(3) NOT NULL DEFAULT '50',
  `autoMan` tinyint(1) NOT NULL DEFAULT '1',
  `fixedTask` tinyint(1) NOT NULL DEFAULT '0',
  `taskHours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `inCalc` tinyint(1) NOT NULL DEFAULT '1',
  `isGeneric` tinyint(1) NOT NULL DEFAULT '0',
  `isOvertime` tinyint(1) NOT NULL DEFAULT '0',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `accountNo` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `onCall` tinyint(1) NOT NULL DEFAULT '0',
  `onCallQualified` tinyint(1) NOT NULL DEFAULT '0',
  `onCallExtra` tinyint(1) NOT NULL DEFAULT '0',
  `standBy` tinyint(1) NOT NULL DEFAULT '0',
  `standByQualified` tinyint(1) NOT NULL DEFAULT '0',
  `standByExtra` tinyint(1) NOT NULL DEFAULT '0',
  `overTimeType` tinyint(1) NOT NULL DEFAULT '0',
  `onlyXtraSaltypes` tinyint(1) NOT NULL DEFAULT '0',
  `isActive` int(1) NOT NULL DEFAULT '1',
  `isFreeDay` tinyint(1) NOT NULL DEFAULT '0',
  `showInWebpunch` tinyint(1) NOT NULL DEFAULT '0',
  `countPunchHours` tinyint(1) NOT NULL DEFAULT '1',
  `countScheduleHours` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=140320 DEFAULT CHARSET=utf8;



# Dump of table scheduled_employee_event
# ------------------------------------------------------------

CREATE TABLE `scheduled_employee_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduledEmployeeUpdateId` int(11) NOT NULL,
  `eventTypeId` int(11) NOT NULL,
  `targetUnitId` int(11) DEFAULT NULL,
  `scheduledDate` date NOT NULL,
  `isCompleted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `fk_scheduled_employee_idx` (`scheduledEmployeeUpdateId`),
  KEY `fk_event_type_idx` (`eventTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=386292 DEFAULT CHARSET=latin1;



# Dump of table scheduled_employee_event_attribute
# ------------------------------------------------------------

CREATE TABLE `scheduled_employee_event_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduledEmployeeEventId` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `fk_scheduled_employee_event_idx` (`scheduledEmployeeEventId`)
) ENGINE=InnoDB AUTO_INCREMENT=386254 DEFAULT CHARSET=latin1;



# Dump of table scheduled_employee_event_type
# ------------------------------------------------------------

CREATE TABLE `scheduled_employee_event_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;



# Dump of table scheduled_employee_update
# ------------------------------------------------------------

CREATE TABLE `scheduled_employee_update` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeId` int(11) NOT NULL,
  `ts` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `fk_employeeId_idx` (`employeeId`)
) ENGINE=InnoDB AUTO_INCREMENT=386178 DEFAULT CHARSET=latin1;



# Dump of table scheduletask
# ------------------------------------------------------------

CREATE TABLE `scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `scheduletask_1` (`scheduleId`),
  KEY `scheduletask_2` (`categoryId`),
  CONSTRAINT `scheduletask_1` FOREIGN KEY (`scheduleId`) REFERENCES `schedule` (`id`),
  CONSTRAINT `scheduletask_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9045755 DEFAULT CHARSET=utf8;



# Dump of table scheduletask_chfl
# ------------------------------------------------------------

CREATE TABLE `scheduletask_chfl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `begTime` time NOT NULL,
  `endTime` time NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `indexForScheduleId` (`scheduleId`),
  CONSTRAINT `fk_scheduletask_chfl_id` FOREIGN KEY (`scheduleId`) REFERENCES `schedule_chfl` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;



# Dump of table section
# ------------------------------------------------------------

CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `restId` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `managerId` int(11) NOT NULL DEFAULT '0',
  `managerGroupId` int(11) NOT NULL DEFAULT '0',
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `havePublishedTo` tinyint(1) NOT NULL DEFAULT '0',
  `sectPublishedTo` date NOT NULL DEFAULT '0000-00-00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `GROUP` (`groupId`),
  CONSTRAINT `section_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18900 DEFAULT CHARSET=utf8;



# Dump of table self_schedule_period
# ------------------------------------------------------------

CREATE TABLE `self_schedule_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `openFrom` datetime NOT NULL,
  `openTo` datetime NOT NULL,
  `periodEndDate` datetime NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  CONSTRAINT `self_schedule_period_ibfk_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;



# Dump of table sessioncookies
# ------------------------------------------------------------

CREATE TABLE `sessioncookies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie` varchar(40) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cookie` (`cookie`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;



# Dump of table shared_documents
# ------------------------------------------------------------

CREATE TABLE `shared_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) DEFAULT NULL,
  `employeeId` int(11) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `fileUrl` varchar(255) NOT NULL,
  `fileType` varchar(10) DEFAULT NULL,
  `fileSize` decimal(10,0) DEFAULT NULL,
  `description` text,
  `folderIds` varchar(255) DEFAULT NULL,
  `uploadedDate` datetime DEFAULT NULL,
  `uploadedById` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2315 DEFAULT CHARSET=utf8;



# Dump of table shiftsalarytype
# ------------------------------------------------------------

CREATE TABLE `shiftsalarytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shiftTypeId` int(11) NOT NULL DEFAULT '1',
  `salaryType` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shiftsalarytype_1` (`shiftTypeId`),
  CONSTRAINT `shiftsalarytype_1` FOREIGN KEY (`shiftTypeId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5588 DEFAULT CHARSET=utf8;



# Dump of table shifttype_decisiontypes
# ------------------------------------------------------------

CREATE TABLE `shifttype_decisiontypes` (
  `shiftTypeId` int(11) NOT NULL,
  `textItemId` int(11) NOT NULL,
  PRIMARY KEY (`shiftTypeId`,`textItemId`),
  KEY `textItemId` (`textItemId`),
  KEY `shiftTypeId` (`shiftTypeId`),
  CONSTRAINT `shifttype_decisiontypes_ibfk_2` FOREIGN KEY (`shiftTypeId`) REFERENCES `schedulecategory` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table shifttypecategory
# ------------------------------------------------------------

CREATE TABLE `shifttypecategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shifttypecategory_1` (`restId`),
  CONSTRAINT `shifttypecategory_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3571 DEFAULT CHARSET=utf8;



# Dump of table skillcategory
# ------------------------------------------------------------

CREATE TABLE `skillcategory` (
  `scatId` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `scatShort` char(6) NOT NULL,
  `scatName` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`scatId`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=10129 DEFAULT CHARSET=utf8;



# Dump of table skills
# ------------------------------------------------------------

CREATE TABLE `skills` (
  `skillId` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `skillCategoryId` int(11) NOT NULL,
  `skillRefNo` char(15) NOT NULL DEFAULT '',
  `skillDescription` varchar(255) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`skillId`),
  KEY `skillSort` (`skillCategoryId`,`skillRefNo`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=39857 DEFAULT CHARSET=utf8;



# Dump of table smslogg
# ------------------------------------------------------------

CREATE TABLE `smslogg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `toSMS` varchar(50) NOT NULL,
  `toGroupId` int(11) DEFAULT '0',
  `smsCost` int(11) NOT NULL DEFAULT '0',
  `year` year(4) NOT NULL,
  `month` int(2) NOT NULL DEFAULT '0',
  `week` int(2) NOT NULL DEFAULT '0',
  `loggDate` date NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `smslogg_1` (`toPersId`),
  KEY `smslogg_2` (`fromPersId`),
  KEY `smslogg_3` (`restId`),
  CONSTRAINT `smslogg_2` FOREIGN KEY (`fromPersId`) REFERENCES `employee` (`id`),
  CONSTRAINT `smslogg_3` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6972463 DEFAULT CHARSET=utf8;



# Dump of table smslogg_archive
# ------------------------------------------------------------

CREATE TABLE `smslogg_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `bodytext` text NOT NULL,
  `senddate` datetime DEFAULT '0000-00-00 00:00:00',
  `fromPersId` int(11) DEFAULT '0',
  `toPersId` int(11) DEFAULT '0',
  `toSMS` varchar(50) NOT NULL,
  `toGroupId` int(11) DEFAULT '0',
  `smsCost` int(11) NOT NULL DEFAULT '0',
  `year` year(4) NOT NULL,
  `month` int(2) NOT NULL DEFAULT '0',
  `week` int(2) NOT NULL DEFAULT '0',
  `loggDate` date NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;



# Dump of table smsreplyqueue
# ------------------------------------------------------------

CREATE TABLE `smsreplyqueue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL DEFAULT '0',
  `scheduleId` int(11) NOT NULL DEFAULT '0',
  `employeeId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `scheduleId` (`scheduleId`),
  KEY `employeeId` (`employeeId`),
  CONSTRAINT `smsreplyqueue_1` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`),
  CONSTRAINT `smsreplyqueue_2` FOREIGN KEY (`scheduleId`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1396 DEFAULT CHARSET=utf8;



# Dump of table socialcost
# ------------------------------------------------------------

CREATE TABLE `socialcost` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `templateId` int(11) NOT NULL DEFAULT '0',
  `agreementId` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `fromAge` int(11) NOT NULL DEFAULT '0',
  `toAge` int(11) NOT NULL DEFAULT '0',
  `startOn` tinyint(1) NOT NULL DEFAULT '0',
  `costPercent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_socialcost_restId` (`restId`),
  KEY `fk_socialcost_templateId` (`templateId`),
  KEY `fk_socialcost_agreementId` (`agreementId`)
) ENGINE=InnoDB AUTO_INCREMENT=529689 DEFAULT CHARSET=utf8;



# Dump of table specialdays
# ------------------------------------------------------------

CREATE TABLE `specialdays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `specialDate` date NOT NULL,
  `description` varchar(60) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `specialDate` (`specialDate`),
  CONSTRAINT `specialdays_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=990749 DEFAULT CHARSET=utf8;



# Dump of table staffbooking
# ------------------------------------------------------------

CREATE TABLE `staffbooking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `begDate` date NOT NULL,
  `begTime` time NOT NULL,
  `endDate` date NOT NULL,
  `endTime` time NOT NULL,
  `persId` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL,
  `section` int(11) NOT NULL DEFAULT '0',
  `hours` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fixSchedId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `w_search` (`restId`,`begDate`),
  KEY `d_search` (`restId`,`begDate`,`begTime`),
  KEY `persId` (`persId`),
  KEY `fixSchedId` (`fixSchedId`),
  CONSTRAINT `staffbooking_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `staffbooking_2` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8;



# Dump of table staffrole
# ------------------------------------------------------------

CREATE TABLE `staffrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `groupId` int(11) NOT NULL DEFAULT '0',
  `staffType` char(1) NOT NULL DEFAULT 'A',
  `name` varchar(50) NOT NULL DEFAULT '',
  `managerId` int(11) NOT NULL DEFAULT '0',
  `editableByWebservice` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `groupId` (`groupId`),
  KEY `managerId` (`managerId`)
) ENGINE=InnoDB AUTO_INCREMENT=2035 DEFAULT CHARSET=utf8;



# Dump of table staffrole_member
# ------------------------------------------------------------

CREATE TABLE `staffrole_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `groupSort` (`groupId`,`employeeId`),
  KEY `staffrole_member_1` (`employeeId`),
  CONSTRAINT `staffrole_member_1` FOREIGN KEY (`groupId`) REFERENCES `staffrole` (`id`),
  CONSTRAINT `staffrole_member_2` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1816667 DEFAULT CHARSET=utf8;



# Dump of table stats_vertical
# ------------------------------------------------------------

CREATE TABLE `stats_vertical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;



# Dump of table stats_vertical_customer
# ------------------------------------------------------------

CREATE TABLE `stats_vertical_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `verticalid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vertical_idx` (`verticalid`),
  KEY `fk_restid_idx` (`customerid`),
  CONSTRAINT `fk_customer` FOREIGN KEY (`customerid`) REFERENCES `qt_customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vertical` FOREIGN KEY (`verticalid`) REFERENCES `stats_vertical` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=latin1;



# Dump of table submodule_employee
# ------------------------------------------------------------

CREATE TABLE `submodule_employee` (
  `submodule_id` int(11) unsigned NOT NULL,
  `employee_id` int(11) NOT NULL,
  KEY `submodule_id` (`submodule_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `submodule_employee_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `submodule_employee_ibfk_3` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table submodule_restaurant
# ------------------------------------------------------------

CREATE TABLE `submodule_restaurant` (
  `submodule_id` int(10) unsigned NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  PRIMARY KEY (`submodule_id`,`restaurant_id`),
  KEY `restaurant_id` (`restaurant_id`),
  CONSTRAINT `submodule_restaurant_ibfk_2` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE,
  CONSTRAINT `submodule_restaurant_ibfk_3` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table submodules
# ------------------------------------------------------------

CREATE TABLE `submodules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `submodules_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=423 DEFAULT CHARSET=utf8;



# Dump of table subshifttype
# ------------------------------------------------------------

CREATE TABLE `subshifttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shiftTypeId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `subshifttype_1` (`shiftTypeId`),
  KEY `subshifttype_2` (`categoryId`),
  CONSTRAINT `subshifttype_1` FOREIGN KEY (`shiftTypeId`) REFERENCES `schedulecategory` (`id`),
  CONSTRAINT `subshifttype_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13927 DEFAULT CHARSET=utf8;



# Dump of table supplier_file
# ------------------------------------------------------------

CREATE TABLE `supplier_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplierId` int(11) DEFAULT NULL,
  `fileId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



# Dump of table suppliers
# ------------------------------------------------------------

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '1',
  `customerId` int(11) NOT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `employeeId` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `orgNr` varchar(250) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `mail` varchar(250) DEFAULT NULL,
  `info` text,
  `origin` int(1) NOT NULL DEFAULT '0',
  `isDeleted` int(1) NOT NULL DEFAULT '0',
  `submodule_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `submodule_id` (`submodule_id`),
  CONSTRAINT `suppliers_ibfk_1` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;



# Dump of table survey
# ------------------------------------------------------------

CREATE TABLE `survey` (
  `surveyId` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `surveyName` char(100) NOT NULL,
  `surveyInfo` text NOT NULL,
  `surveyQuestion1` char(100) NOT NULL,
  `surveyQuestion2` char(100) NOT NULL,
  `surveyQuestion3` char(100) NOT NULL,
  `surveyQuestion4` char(100) NOT NULL,
  `surveyStartDate` date NOT NULL,
  `surveyEndDate` date DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`surveyId`),
  KEY `surveyStartDate` (`surveyStartDate`),
  KEY `restId` (`restId`)
) ENGINE=InnoDB AUTO_INCREMENT=1019 DEFAULT CHARSET=utf8;



# Dump of table survey_resp
# ------------------------------------------------------------

CREATE TABLE `survey_resp` (
  `srespId` int(11) NOT NULL AUTO_INCREMENT,
  `srespSurveyId` int(11) NOT NULL DEFAULT '0',
  `srespPersId` int(11) NOT NULL DEFAULT '0',
  `sresp1` int(5) NOT NULL DEFAULT '3',
  `sresp2` int(5) NOT NULL DEFAULT '3',
  `sresp3` int(5) NOT NULL DEFAULT '3',
  `sresp4` int(5) NOT NULL DEFAULT '3',
  `srespComment` text,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`srespId`),
  KEY `survey_resp_1` (`srespSurveyId`),
  KEY `survey_resp_2` (`srespPersId`),
  CONSTRAINT `survey_resp_1` FOREIGN KEY (`srespSurveyId`) REFERENCES `survey` (`surveyId`),
  CONSTRAINT `survey_resp_2` FOREIGN KEY (`srespPersId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3177 DEFAULT CHARSET=utf8;



# Dump of table survey_v2
# ------------------------------------------------------------

CREATE TABLE `survey_v2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `name` varchar(200) NOT NULL,
  `intro` text NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date DEFAULT NULL,
  `empView` tinyint(1) NOT NULL DEFAULT '1',
  `showAnonymous` tinyint(1) NOT NULL DEFAULT '1',
  `isPublished` tinyint(1) NOT NULL DEFAULT '1',
  `staffCategory` int(11) NOT NULL DEFAULT '0',
  `staffRole` int(4) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `startDate` (`startDate`),
  KEY `survey_v2_1` (`restId`),
  CONSTRAINT `survey_v2_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8;



# Dump of table survey_v2_envquest
# ------------------------------------------------------------

CREATE TABLE `survey_v2_envquest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surveyId` int(11) NOT NULL DEFAULT '1',
  `rectype` tinyint(1) NOT NULL DEFAULT '0',
  `qno` int(3) NOT NULL DEFAULT '1',
  `question` varchar(255) NOT NULL,
  `toolTip` text NOT NULL,
  `questType` int(2) NOT NULL DEFAULT '0',
  `sliderMin` int(6) NOT NULL DEFAULT '0',
  `sliderMax` int(6) NOT NULL DEFAULT '100',
  `sliderSnap` int(6) NOT NULL DEFAULT '10',
  `sliderMinLabel` varchar(20) NOT NULL,
  `sliderMaxLabel` varchar(20) NOT NULL,
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `hasDefault` tinyint(1) NOT NULL DEFAULT '0',
  `useDefault` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rectype` (`rectype`,`qno`),
  KEY `survey_v2_envquest_1` (`surveyId`),
  CONSTRAINT `survey_v2_envquest_1` FOREIGN KEY (`surveyId`) REFERENCES `survey_v2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7349 DEFAULT CHARSET=utf8;



# Dump of table survey_v2_questalt
# ------------------------------------------------------------

CREATE TABLE `survey_v2_questalt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questId` int(11) NOT NULL DEFAULT '1',
  `qno` int(3) NOT NULL DEFAULT '1',
  `alternative` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rectype` (`questId`,`qno`),
  CONSTRAINT `survey_v2_questalt_1` FOREIGN KEY (`questId`) REFERENCES `survey_v2_envquest` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55639 DEFAULT CHARSET=utf8;



# Dump of table surveyresp_answer
# ------------------------------------------------------------

CREATE TABLE `surveyresp_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respId` int(11) NOT NULL DEFAULT '1',
  `questId` int(11) NOT NULL DEFAULT '0',
  `qno` int(4) NOT NULL DEFAULT '0',
  `responce` int(11) NOT NULL DEFAULT '0',
  `multiResponce` varchar(200) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `questId` (`questId`),
  KEY `respOrder` (`respId`,`qno`),
  CONSTRAINT `surveyresp_answer_1` FOREIGN KEY (`respId`) REFERENCES `surveyresp_v2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76610 DEFAULT CHARSET=utf8;



# Dump of table surveyresp_environment
# ------------------------------------------------------------

CREATE TABLE `surveyresp_environment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `respId` int(11) NOT NULL DEFAULT '1',
  `questId` int(11) NOT NULL DEFAULT '0',
  `qno` int(4) NOT NULL DEFAULT '0',
  `responce` int(11) NOT NULL DEFAULT '0',
  `multiResponce` varchar(200) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `questId` (`questId`),
  KEY `respOrder` (`respId`,`qno`),
  CONSTRAINT `surveyresp_environment_1` FOREIGN KEY (`respId`) REFERENCES `surveyresp_v2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7072 DEFAULT CHARSET=utf8;



# Dump of table surveyresp_v2
# ------------------------------------------------------------

CREATE TABLE `surveyresp_v2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surveyId` int(11) NOT NULL DEFAULT '1',
  `persId` int(11) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `surveyresp_v2_1` (`persId`),
  KEY `surveyresp_v2_2` (`surveyId`),
  CONSTRAINT `surveyresp_v2_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`),
  CONSTRAINT `surveyresp_v2_2` FOREIGN KEY (`surveyId`) REFERENCES `survey_v2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6266 DEFAULT CHARSET=utf8;



# Dump of table surveyresptemplate
# ------------------------------------------------------------

CREATE TABLE `surveyresptemplate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `surveyresptemplate_1` (`restId`),
  CONSTRAINT `surveyresptemplate_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;



# Dump of table surveytemplate_questalt
# ------------------------------------------------------------

CREATE TABLE `surveytemplate_questalt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL DEFAULT '1',
  `qno` int(3) NOT NULL DEFAULT '1',
  `alternative` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rectype` (`templateId`,`qno`),
  CONSTRAINT `surveytemplate_questalt_1` FOREIGN KEY (`templateId`) REFERENCES `surveyresptemplate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=941 DEFAULT CHARSET=utf8;



# Dump of table sysconstants
# ------------------------------------------------------------

CREATE TABLE `sysconstants` (
  `sysId` char(10) NOT NULL,
  `sysNum` int(11) NOT NULL DEFAULT '0',
  `sysString` varchar(125) NOT NULL,
  PRIMARY KEY (`sysId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table template_scheduletask
# ------------------------------------------------------------

CREATE TABLE `template_scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `template_scheduletask_1` (`scheduleId`),
  KEY `template_scheduletask_2` (`categoryId`),
  CONSTRAINT `template_scheduletask_1` FOREIGN KEY (`scheduleId`) REFERENCES `schedule_template_shifts` (`id`),
  CONSTRAINT `template_scheduletask_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46838 DEFAULT CHARSET=utf8;



# Dump of table textitem
# ------------------------------------------------------------

CREATE TABLE `textitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `tag` char(1) NOT NULL,
  `ttext` varchar(80) NOT NULL DEFAULT '',
  `extCode` varchar(20) NOT NULL DEFAULT '',
  `showInWebpunch` tinyint(1) NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '1',
  `reportingType` int(11) DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `extCodeIdx` (`extCode`),
  CONSTRAINT `textitem_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36440 DEFAULT CHARSET=utf8;



# Dump of table texttag
# ------------------------------------------------------------

CREATE TABLE `texttag` (
  `tagId` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL,
  `tagPgm` char(30) NOT NULL,
  `tagText` text NOT NULL,
  `tagHtml` tinyint(1) NOT NULL DEFAULT '1',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tagId`),
  KEY `restId` (`restId`),
  CONSTRAINT `texttag_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1138 DEFAULT CHARSET=utf8;



# Dump of table time_parameters
# ------------------------------------------------------------

CREATE TABLE `time_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `warnOnEmpApprovalColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnCustApprovalColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnAdmApprovalColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnTimeMissingColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnShiftMissingColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnTimeDifferenceColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnNoLinkColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnTimeDiffNegColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnEmpChangeColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnOvertimeColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnMoretimeColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnAbsenceColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnSalaryApprovalColor` int(10) NOT NULL DEFAULT '13369344',
  `empAutoTime` tinyint(1) NOT NULL DEFAULT '0',
  `shiftSelectionAddsInteruptionShift` tinyint(1) NOT NULL DEFAULT '0',
  `genMoretime` tinyint(1) NOT NULL DEFAULT '1',
  `obOnCall` tinyint(1) NOT NULL DEFAULT '0',
  `obLeave` tinyint(1) NOT NULL DEFAULT '0',
  `shiftMatching` tinyint(1) NOT NULL DEFAULT '0',
  `warnOnEmpApproval` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnCustApproval` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnAdmiApproval` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnTimeMissing` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnShiftMissing` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnNoLink` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnTimeDifference` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnTimeDiffNeg` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnEmpChange` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnOvertime` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnMoretime` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnAbsence` tinyint(1) NOT NULL DEFAULT '1',
  `warnOnSalaryApproval` tinyint(1) NOT NULL DEFAULT '0',
  `warnTimeMinutesNeg` int(3) NOT NULL DEFAULT '15',
  `warnTimeMinutes` int(3) NOT NULL DEFAULT '15',
  `strictApprovals` tinyint(1) NOT NULL DEFAULT '0',
  `payrollSystem` int(2) NOT NULL DEFAULT '0',
  `showOnCall` tinyint(1) NOT NULL DEFAULT '1',
  `showStandby` tinyint(1) NOT NULL DEFAULT '1',
  `showSchedTime` tinyint(1) NOT NULL DEFAULT '0',
  `showOb1` tinyint(1) NOT NULL DEFAULT '1',
  `showOb2` tinyint(1) NOT NULL DEFAULT '1',
  `showOb3` tinyint(1) NOT NULL DEFAULT '1',
  `showOb4` tinyint(1) NOT NULL DEFAULT '1',
  `showOb5` tinyint(1) NOT NULL DEFAULT '0',
  `showOb6` tinyint(1) NOT NULL DEFAULT '0',
  `showOb7` tinyint(1) NOT NULL DEFAULT '0',
  `showOb8` tinyint(1) NOT NULL DEFAULT '0',
  `showObTotal` tinyint(1) NOT NULL DEFAULT '0',
  `showShifttype` tinyint(1) NOT NULL DEFAULT '0',
  `showOvertimeOrd` tinyint(1) NOT NULL DEFAULT '0',
  `showOvertimeQaul` tinyint(1) NOT NULL DEFAULT '0',
  `showOvertimeXtra` tinyint(1) NOT NULL DEFAULT '0',
  `showOvertime4` tinyint(1) NOT NULL DEFAULT '0',
  `showOvertimeTotal` tinyint(1) NOT NULL DEFAULT '0',
  `showMoretime` tinyint(1) NOT NULL DEFAULT '0',
  `mgrApproveStaff` tinyint(1) NOT NULL DEFAULT '0',
  `mgrApproveCustomer` tinyint(1) NOT NULL DEFAULT '0',
  `apprPayOnAbsence` tinyint(1) NOT NULL DEFAULT '1',
  `payOtime` tinyint(1) NOT NULL DEFAULT '0',
  `decimalHours` tinyint(1) NOT NULL DEFAULT '1',
  `attestIntoFuture` tinyint(1) NOT NULL DEFAULT '0',
  `onlyTransFullyApproved` tinyint(1) NOT NULL DEFAULT '0',
  `multiUpdate` tinyint(1) NOT NULL DEFAULT '0',
  `withinDistricts` tinyint(1) NOT NULL DEFAULT '0',
  `empCanAddLeavePost` tinyint(1) NOT NULL DEFAULT '0',
  `empCanAddLeavePre` tinyint(1) NOT NULL DEFAULT '0',
  `showSalaryBasedLeave` tinyint(1) NOT NULL DEFAULT '0',
  `showNonSalaryBasedLeave` tinyint(1) NOT NULL DEFAULT '0',
  `showCostCentre` tinyint(1) NOT NULL DEFAULT '0',
  `showProject` tinyint(1) NOT NULL DEFAULT '0',
  `showAccount` tinyint(1) NOT NULL DEFAULT '0',
  `warnOnTimeChanged` tinyint(1) NOT NULL DEFAULT '0',
  `warnOnTimeChangedColor` int(10) NOT NULL DEFAULT '13369344',
  `warnOnAutoPunchColor` int(10) DEFAULT '13369344',
  `incNonApprovedEmpPunchInPayroll` tinyint(1) NOT NULL DEFAULT '0',
  `maximumAdjustedSalaryHoursPerDay` smallint(6) NOT NULL DEFAULT '0',
  `warnOnAutoPunch` tinyint(1) NOT NULL DEFAULT '0',
  `autoPunchChangeRequiresToAttest` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  CONSTRAINT `time_parameters_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42940 DEFAULT CHARSET=utf8;



# Dump of table time_parameters_changes_log
# ------------------------------------------------------------

CREATE TABLE `time_parameters_changes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `logId` (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=52291 DEFAULT CHARSET=latin1;



# Dump of table time_parameters_deletes_log
# ------------------------------------------------------------

CREATE TABLE `time_parameters_deletes_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logId` int(11) NOT NULL,
  `restId` int(11) NOT NULL,
  `qt_custId` int(11) NOT NULL,
  `objectName` varchar(128) NOT NULL,
  `columnName` varchar(128) NOT NULL,
  `oldValue` varchar(128) NOT NULL,
  `newValue` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table time_parameters_summary_log
# ------------------------------------------------------------

CREATE TABLE `time_parameters_summary_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isObjectNew` tinyint(1) DEFAULT '0',
  `isObjectRemoved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `objectId` (`objectId`)
) ENGINE=InnoDB AUTO_INCREMENT=4302 DEFAULT CHARSET=latin1;



# Dump of table timepunch
# ------------------------------------------------------------

CREATE TABLE `timepunch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `punchDate` date NOT NULL DEFAULT '0000-00-00',
  `punchIn` time NOT NULL DEFAULT '00:00:00',
  `punchOut` time NOT NULL DEFAULT '00:00:00',
  `comment` varchar(100) NOT NULL DEFAULT '',
  `mgrComment` text,
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `isOpen` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `auth1` tinyint(1) NOT NULL DEFAULT '0',
  `auth2` tinyint(1) NOT NULL DEFAULT '0',
  `auth3` tinyint(1) NOT NULL DEFAULT '0',
  `autoPunchOut` tinyint(1) NOT NULL DEFAULT '0',
  `shiftReference` int(11) NOT NULL DEFAULT '0',
  `subShiftReference` int(11) NOT NULL DEFAULT '0',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `leaveId` int(11) NOT NULL DEFAULT '0',
  `leaveAppId` int(11) NOT NULL DEFAULT '0',
  `orgDate` date NOT NULL DEFAULT '0000-00-00',
  `orgIn` time NOT NULL DEFAULT '00:00:00',
  `orgOut` time NOT NULL DEFAULT '00:00:00',
  `payOtime` tinyint(1) NOT NULL DEFAULT '0',
  `otime` decimal(10,2) NOT NULL DEFAULT '0.00',
  `empChange` tinyint(1) NOT NULL DEFAULT '0',
  `payrollTransf` tinyint(1) NOT NULL DEFAULT '0',
  `beenTransfered` tinyint(1) NOT NULL DEFAULT '0',
  `lockedTimetrackers` tinyint(1) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `externalInfo1` varchar(50) DEFAULT NULL,
  `externalInfo2` varchar(50) DEFAULT NULL,
  `externalInfo3` varchar(50) DEFAULT NULL,
  `decisionId` int(11) NOT NULL DEFAULT '0',
  `teamId` int(11) NOT NULL DEFAULT '0',
  `changerId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persIdDate` (`persId`,`punchDate`),
  KEY `shiftReference` (`shiftReference`),
  KEY `agreementId` (`agreementId`),
  KEY `teamId` (`teamId`),
  KEY `punchdate_idx` (`punchDate`),
  CONSTRAINT `timepunch_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`),
  CONSTRAINT `timepunch_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51608860 DEFAULT CHARSET=utf8;



# Dump of table timepunch_autopunch
# ------------------------------------------------------------

CREATE TABLE `timepunch_autopunch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timepunchId` int(11) NOT NULL,
  `qtCustomerId` int(11) NOT NULL,
  `punchDate` varchar(10) NOT NULL,
  `punchOut` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_timepunch_id_idx` (`timepunchId`),
  KEY `fk_qtcustomer_id_idx` (`qtCustomerId`),
  KEY `fk_punch_date_idx` (`punchDate`),
  CONSTRAINT `fk_qtcustomer_id` FOREIGN KEY (`qtCustomerId`) REFERENCES `qt_customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timepunch_id` FOREIGN KEY (`timepunchId`) REFERENCES `timepunch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=68173 DEFAULT CHARSET=latin1;



# Dump of table timepunch_deleted
# ------------------------------------------------------------

CREATE TABLE `timepunch_deleted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `punchDate` date NOT NULL DEFAULT '0000-00-00',
  `punchIn` time NOT NULL DEFAULT '00:00:00',
  `punchOut` time NOT NULL DEFAULT '00:00:00',
  `comment` varchar(100) NOT NULL DEFAULT '',
  `mgrComment` text,
  `costCentre` int(11) NOT NULL DEFAULT '0',
  `projectNo` int(11) NOT NULL DEFAULT '0',
  `isOpen` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `auth1` tinyint(1) NOT NULL DEFAULT '0',
  `auth2` tinyint(1) NOT NULL DEFAULT '0',
  `auth3` tinyint(1) NOT NULL DEFAULT '0',
  `autoPunchOut` tinyint(1) NOT NULL DEFAULT '0',
  `shiftReference` int(11) NOT NULL DEFAULT '0',
  `salaryType` int(11) NOT NULL DEFAULT '0',
  `leaveId` int(11) NOT NULL DEFAULT '0',
  `leaveAppId` int(11) NOT NULL DEFAULT '0',
  `orgDate` date NOT NULL DEFAULT '0000-00-00',
  `orgIn` time NOT NULL DEFAULT '00:00:00',
  `orgOut` time NOT NULL DEFAULT '00:00:00',
  `payOtime` tinyint(1) NOT NULL DEFAULT '0',
  `otime` decimal(10,2) NOT NULL DEFAULT '0.00',
  `empChange` tinyint(1) NOT NULL DEFAULT '0',
  `payrollTransf` tinyint(1) NOT NULL DEFAULT '0',
  `beenTransfered` tinyint(1) NOT NULL DEFAULT '0',
  `lockedTimetrackers` tinyint(1) NOT NULL DEFAULT '0',
  `crmSchedId` int(11) NOT NULL DEFAULT '0',
  `agreementId` int(11) DEFAULT NULL,
  `externalInfo1` varchar(50) DEFAULT NULL,
  `externalInfo2` varchar(50) DEFAULT NULL,
  `externalInfo3` varchar(50) DEFAULT NULL,
  `decisionId` int(11) NOT NULL DEFAULT '0',
  `teamId` int(11) NOT NULL DEFAULT '0',
  `changerId` int(11) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persIdDate` (`persId`,`punchDate`),
  KEY `shiftReference` (`shiftReference`),
  KEY `agreementId` (`agreementId`),
  KEY `teamId` (`teamId`),
  CONSTRAINT `timepunch_deleted_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`),
  CONSTRAINT `timepunch_deleted_ibfk_1` FOREIGN KEY (`agreementId`) REFERENCES `agreement_v2` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51607025 DEFAULT CHARSET=utf8;



# Dump of table timepunch_devices
# ------------------------------------------------------------

CREATE TABLE `timepunch_devices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `device_key` varchar(255) DEFAULT NULL,
  `persId` int(11) DEFAULT NULL,
  `authorized` int(1) DEFAULT NULL,
  `last_used` timestamp NULL DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId` (`persId`),
  CONSTRAINT `timepunch_devices_ibfk_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18785916 DEFAULT CHARSET=utf8;



# Dump of table timepunch_log
# ------------------------------------------------------------

CREATE TABLE `timepunch_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `recordType` tinyint(1) DEFAULT '0',
  `recordId` int(11) DEFAULT '0',
  `fieldNo` int(2) DEFAULT '0',
  `numOldValue` int(11) DEFAULT '0',
  `numNewValue` int(11) DEFAULT '0',
  `strOldValue` varchar(150) NOT NULL,
  `strNewValue` varchar(150) NOT NULL,
  `changerId` int(11) DEFAULT '0',
  `customerId` int(11) DEFAULT '0',
  `persId` int(11) DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  KEY `restIdts` (`restId`,`ts`),
  KEY `recordId` (`recordId`),
  KEY `persId` (`persId`)
) ENGINE=InnoDB AUTO_INCREMENT=269856346 DEFAULT CHARSET=utf8;



# Dump of table timepunch_salaries
# ------------------------------------------------------------

CREATE TABLE `timepunch_salaries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `timepunch_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `chfl_id` int(11) DEFAULT NULL,
  `agreement_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `dayCount` int(11) DEFAULT NULL,
  `salaryApproved` tinyint(1) NOT NULL DEFAULT '0',
  `salaryRounded` tinyint(1) NOT NULL DEFAULT '0',
  `calculatedCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `socialCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `leaveCost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sourceType` int(3) NOT NULL DEFAULT '0',
  `trackerDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `timepunch_id` (`timepunch_id`),
  KEY `schedule_id` (`schedule_id`),
  KEY `user_id` (`user_id`),
  KEY `chfl_id` (`chfl_id`),
  KEY `timepunch_salaries_ibfk_6` (`agreement_id`),
  CONSTRAINT `timepunch_salaries_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `salarytypes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `timepunch_salaries_ibfk_2` FOREIGN KEY (`timepunch_id`) REFERENCES `timepunch` (`id`) ON DELETE CASCADE,
  CONSTRAINT `timepunch_salaries_ibfk_3` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`) ON DELETE CASCADE,
  CONSTRAINT `timepunch_salaries_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  CONSTRAINT `timepunch_salaries_ibfk_5` FOREIGN KEY (`chfl_id`) REFERENCES `schedule_chfl` (`id`) ON DELETE CASCADE,
  CONSTRAINT `timepunch_salaries_ibfk_6` FOREIGN KEY (`agreement_id`) REFERENCES `agreement_v2` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=219505199 DEFAULT CHARSET=utf8;



# Dump of table timepunchextended
# ------------------------------------------------------------

CREATE TABLE `timepunchextended` (
   `id` INT(11) NOT NULL DEFAULT '0',
   `persId` INT(11) NOT NULL DEFAULT '0',
   `punchDate` DATE NOT NULL DEFAULT '0000-00-00',
   `punchIn` TIME NOT NULL DEFAULT '00:00:00',
   `punchInDate` VARCHAR(21) NOT NULL DEFAULT '',
   `punchOut` TIME NOT NULL DEFAULT '00:00:00',
   `punchOutDate` VARCHAR(21) NULL DEFAULT NULL,
   `comment` VARCHAR(100) NOT NULL DEFAULT '',
   `mgrComment` TEXT NULL DEFAULT NULL,
   `costCentre` INT(11) NOT NULL DEFAULT '0',
   `projectNo` INT(11) NOT NULL DEFAULT '0',
   `isOpen` TINYINT(1) NOT NULL DEFAULT '0',
   `editable` TINYINT(1) NOT NULL DEFAULT '1',
   `auth1` TINYINT(1) NOT NULL DEFAULT '0',
   `auth2` TINYINT(1) NOT NULL DEFAULT '0',
   `auth3` TINYINT(1) NOT NULL DEFAULT '0',
   `autoPunchOut` TINYINT(1) NOT NULL DEFAULT '0',
   `shiftReference` INT(11) NOT NULL DEFAULT '0',
   `salaryType` INT(11) NOT NULL DEFAULT '0',
   `leaveId` INT(11) NOT NULL DEFAULT '0',
   `leaveAppId` INT(11) NOT NULL DEFAULT '0',
   `orgDate` DATE NOT NULL DEFAULT '0000-00-00',
   `orgIn` TIME NOT NULL DEFAULT '00:00:00',
   `orgOut` TIME NOT NULL DEFAULT '00:00:00',
   `payOtime` TINYINT(1) NOT NULL DEFAULT '0',
   `otime` DECIMAL(10) NOT NULL DEFAULT '0.00',
   `empChange` TINYINT(1) NOT NULL DEFAULT '0',
   `payrollTransf` TINYINT(1) NOT NULL DEFAULT '0',
   `beenTransfered` TINYINT(1) NOT NULL DEFAULT '0',
   `crmSchedId` INT(11) NOT NULL DEFAULT '0',
   `agreementId` INT(11) NULL DEFAULT NULL,
   `ts` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM;



# Dump of table timerule_log
# ------------------------------------------------------------

CREATE TABLE `timerule_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `persId` int(11) DEFAULT NULL,
  `managerId` int(11) DEFAULT NULL,
  `restId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `startTime` datetime NOT NULL,
  `endTime` datetime NOT NULL,
  `approvedTime` datetime NOT NULL,
  `warning` text,
  `scheduleId` int(11) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `persId` (`persId`),
  KEY `managerId` (`managerId`),
  KEY `restId` (`restId`),
  KEY `startTime` (`startTime`),
  KEY `search` (`restId`,`startTime`)
) ENGINE=InnoDB AUTO_INCREMENT=30214744 DEFAULT CHARSET=utf8;



# Dump of table timetracker
# ------------------------------------------------------------

CREATE TABLE `timetracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restId` int(11) NOT NULL DEFAULT '0',
  `trackerCode` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `isHours` tinyint(1) NOT NULL DEFAULT '1',
  `trackerType` int(2) NOT NULL DEFAULT '0',
  `hasPeriodStart` tinyint(1) NOT NULL DEFAULT '0',
  `periodStartDate` date NOT NULL,
  `comment` text,
  `showOnTimeCard` tinyint(1) NOT NULL DEFAULT '1',
  `balanceDate` varchar(64) NOT NULL DEFAULT '',
  `checkOnLimits` tinyint(1) NOT NULL DEFAULT '0',
  `minValue` decimal(6,2) NOT NULL DEFAULT '0.00',
  `maxValue` decimal(6,2) NOT NULL DEFAULT '0.00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `restId` (`restId`),
  CONSTRAINT `timetracker_1` FOREIGN KEY (`restId`) REFERENCES `restaurants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2258 DEFAULT CHARSET=utf8;



# Dump of table trackertrans
# ------------------------------------------------------------

CREATE TABLE `trackertrans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trackerId` int(11) NOT NULL DEFAULT '1',
  `persId` int(11) NOT NULL DEFAULT '1',
  `trackerDate` date NOT NULL,
  `numValue` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `adjustmentMethod` tinyint(1) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `trackerId` (`trackerId`),
  KEY `persId` (`persId`),
  KEY `trackerdate` (`trackerDate`),
  CONSTRAINT `trackertrans_1` FOREIGN KEY (`trackerId`) REFERENCES `timetracker` (`id`),
  CONSTRAINT `trackertrans_2` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=519144 DEFAULT CHARSET=utf8;



# Dump of table user_defined_fields
# ------------------------------------------------------------

CREATE TABLE `user_defined_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  `filterable` tinyint(1) unsigned NOT NULL,
  `functionGroup` tinyint(11) unsigned NOT NULL,
  `type` tinyint(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `isDeleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `user_defined_fields_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `qt_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;



# Dump of table user_defined_fields_values
# ------------------------------------------------------------

CREATE TABLE `user_defined_fields_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `udf_id` int(11) unsigned NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  `uniqueResource` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `restaurant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurant_id` (`restaurant_id`),
  KEY `udf_id` (`udf_id`),
  CONSTRAINT `user_defined_fields_values_ibfk_2` FOREIGN KEY (`udf_id`) REFERENCES `user_defined_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_defined_fields_values_ibfk_3` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;



# Dump of table vacation
# ------------------------------------------------------------

CREATE TABLE `vacation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `fromTime` time NOT NULL DEFAULT '00:00:00',
  `toTime` time NOT NULL DEFAULT '00:00:00',
  `reason` varchar(50) NOT NULL,
  `leaveAppId` int(11) NOT NULL DEFAULT '0',
  `isPreliminary` tinyint(1) NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `leaveAppIdx` (`leaveAppId`),
  KEY `persId_prel_fromDate` (`persId`,`isPreliminary`,`fromDate`),
  CONSTRAINT `vacation_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4775992 DEFAULT CHARSET=utf8;



# Dump of table version
# ------------------------------------------------------------

CREATE TABLE `version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `customerId` int(11) NOT NULL,
  `restaurantId` int(11) DEFAULT NULL,
  `groupId` int(11) DEFAULT NULL,
  `objId` int(11) DEFAULT NULL,
  `majorRevision` int(11) NOT NULL,
  `minorRevision` int(11) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `publishDate` datetime DEFAULT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isReady` tinyint(1) DEFAULT '0',
  `isImportant` tinyint(1) DEFAULT NULL,
  `isLocal` tinyint(1) DEFAULT '0',
  `actionStatus` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `editor` int(11) DEFAULT NULL,
  `publisher` int(11) DEFAULT NULL,
  `actionUserId` int(11) DEFAULT NULL,
  `createTimestamp` timestamp NULL DEFAULT NULL,
  `editTimestamp` timestamp NULL DEFAULT NULL,
  `publishTimestamp` timestamp NULL DEFAULT NULL,
  `actionTimestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customerId` (`customerId`),
  KEY `restaurantId` (`restaurantId`),
  KEY `fk_creator_version_employee` (`creator`),
  KEY `fk_editor_version_employee` (`editor`),
  KEY `fk_publisher_version_employee` (`publisher`),
  KEY `version_ibfk_58` (`actionUserId`),
  KEY `type` (`type`),
  KEY `groupId` (`groupId`),
  KEY `objId` (`objId`),
  KEY `majorRevision` (`majorRevision`),
  KEY `minorRevision` (`minorRevision`),
  KEY `type_customer_restaurant` (`type`,`customerId`,`restaurantId`),
  KEY `type_customer` (`type`,`customerId`),
  KEY `type_2` (`type`,`groupId`,`restaurantId`),
  CONSTRAINT `version_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `qt_customer` (`id`),
  CONSTRAINT `version_ibfk_2` FOREIGN KEY (`restaurantId`) REFERENCES `restaurants` (`id`),
  CONSTRAINT `version_ibfk_59` FOREIGN KEY (`creator`) REFERENCES `employee` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `version_ibfk_60` FOREIGN KEY (`editor`) REFERENCES `employee` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `version_ibfk_61` FOREIGN KEY (`publisher`) REFERENCES `employee` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `version_ibfk_62` FOREIGN KEY (`actionUserId`) REFERENCES `employee` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10190 DEFAULT CHARSET=utf8;



# Dump of table videolibrary
# ------------------------------------------------------------

CREATE TABLE `videolibrary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(10) NOT NULL,
  `section` tinyint(1) NOT NULL DEFAULT '1',
  `restId` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `filename` varchar(60) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;



# Dump of table wishlist
# ------------------------------------------------------------

CREATE TABLE `wishlist` (
  `wlistId` int(11) NOT NULL AUTO_INCREMENT,
  `wlistSection` char(1) NOT NULL DEFAULT '0',
  `wlistHeader` varchar(200) NOT NULL,
  `wlistInfo` text,
  `wlistVote1` int(11) NOT NULL DEFAULT '0',
  `wlistVote2` int(11) NOT NULL DEFAULT '0',
  `wlistVote3` int(11) NOT NULL DEFAULT '0',
  `wlistVote4` int(11) NOT NULL DEFAULT '0',
  `wlistVote5` int(11) NOT NULL DEFAULT '0',
  `wlistName` varchar(50) NOT NULL,
  `wlistEmail` varchar(50) NOT NULL,
  `wlistTag` int(11) NOT NULL DEFAULT '0',
  `wlistModerated` tinyint(1) NOT NULL DEFAULT '1',
  `wlistDate` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `persId` int(11) NOT NULL DEFAULT '0',
  `attachment` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`wlistId`)
) ENGINE=InnoDB AUTO_INCREMENT=514 DEFAULT CHARSET=utf8;



# Dump of table wlist_comm
# ------------------------------------------------------------

CREATE TABLE `wlist_comm` (
  `wcommId` int(11) NOT NULL AUTO_INCREMENT,
  `wcommWlistId` int(11) NOT NULL DEFAULT '0',
  `wcommTag` int(11) NOT NULL DEFAULT '0',
  `wcommComment` text NOT NULL,
  `wcommName` varchar(50) NOT NULL,
  `wcommEmail` varchar(50) NOT NULL,
  `wcommModerated` tinyint(1) NOT NULL DEFAULT '1',
  `wcommDate` date NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`wcommId`),
  KEY `wlist_resp_1` (`wcommWlistId`),
  CONSTRAINT `wlist_resp_1` FOREIGN KEY (`wcommWlistId`) REFERENCES `wishlist` (`wlistId`)
) ENGINE=InnoDB AUTO_INCREMENT=365 DEFAULT CHARSET=utf8;



# Dump of table work_scheduletask
# ------------------------------------------------------------

CREATE TABLE `work_scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL DEFAULT '1',
  `categoryId` int(11) NOT NULL DEFAULT '1',
  `begTime` time NOT NULL DEFAULT '00:00:00',
  `endTime` time NOT NULL DEFAULT '00:00:00',
  `ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `work_scheduletask_1` (`scheduleId`),
  KEY `work_scheduletask_2` (`categoryId`),
  CONSTRAINT `work_scheduletask_1` FOREIGN KEY (`scheduleId`) REFERENCES `schedule_work` (`id`),
  CONSTRAINT `work_scheduletask_2` FOREIGN KEY (`categoryId`) REFERENCES `schedulecategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3792 DEFAULT CHARSET=utf8;



# Dump of table wsdlapilog
# ------------------------------------------------------------

CREATE TABLE `wsdlapilog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` year(4) DEFAULT NULL,
  `month` int(2) DEFAULT '0',
  `week` int(2) DEFAULT '0',
  `loggDate` date NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `API_key` varchar(32) NOT NULL DEFAULT '',
  `service` varchar(255) NOT NULL DEFAULT '',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28238588 DEFAULT CHARSET=utf8;



# Dump of table xtrawork
# ------------------------------------------------------------

CREATE TABLE `xtrawork` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persId` int(11) NOT NULL DEFAULT '0',
  `fromDate` date NOT NULL DEFAULT '0000-00-00',
  `toDate` date NOT NULL DEFAULT '9999-12-31',
  `fromTime` time NOT NULL DEFAULT '00:00:00',
  `toTime` time NOT NULL DEFAULT '00:00:00',
  `reason` varchar(50) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fromDate` (`fromDate`,`toDate`),
  KEY `persId_fromDate` (`persId`,`fromDate`),
  CONSTRAINT `xtrawork_1` FOREIGN KEY (`persId`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2575480 DEFAULT CHARSET=utf8;





# Replace placeholder table for timepunchextended with correct view syntax
# ------------------------------------------------------------

DROP TABLE `timepunchextended`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dbadmin`@`10.%` SQL SECURITY DEFINER VIEW `timepunchextended`
AS SELECT
   `timepunch`.`id` AS `id`,
   `timepunch`.`persId` AS `persId`,
   `timepunch`.`punchDate` AS `punchDate`,
   `timepunch`.`punchIn` AS `punchIn`,concat(`timepunch`.`punchDate`,' ',`timepunch`.`punchIn`) AS `punchInDate`,
   `timepunch`.`punchOut` AS `punchOut`,if((`timepunch`.`punchOut` > `timepunch`.`punchIn`),concat(`timepunch`.`punchDate`,' ',`timepunch`.`punchOut`),concat((`timepunch`.`punchDate` + interval 1 day),' ',`timepunch`.`punchOut`)) AS `punchOutDate`,
   `timepunch`.`comment` AS `comment`,
   `timepunch`.`mgrComment` AS `mgrComment`,
   `timepunch`.`costCentre` AS `costCentre`,
   `timepunch`.`projectNo` AS `projectNo`,
   `timepunch`.`isOpen` AS `isOpen`,
   `timepunch`.`editable` AS `editable`,
   `timepunch`.`auth1` AS `auth1`,
   `timepunch`.`auth2` AS `auth2`,
   `timepunch`.`auth3` AS `auth3`,
   `timepunch`.`autoPunchOut` AS `autoPunchOut`,
   `timepunch`.`shiftReference` AS `shiftReference`,
   `timepunch`.`salaryType` AS `salaryType`,
   `timepunch`.`leaveId` AS `leaveId`,
   `timepunch`.`leaveAppId` AS `leaveAppId`,
   `timepunch`.`orgDate` AS `orgDate`,
   `timepunch`.`orgIn` AS `orgIn`,
   `timepunch`.`orgOut` AS `orgOut`,
   `timepunch`.`payOtime` AS `payOtime`,
   `timepunch`.`otime` AS `otime`,
   `timepunch`.`empChange` AS `empChange`,
   `timepunch`.`payrollTransf` AS `payrollTransf`,
   `timepunch`.`beenTransfered` AS `beenTransfered`,
   `timepunch`.`crmSchedId` AS `crmSchedId`,
   `timepunch`.`agreementId` AS `agreementId`,
   `timepunch`.`ts` AS `ts`
FROM `timepunch`;

--
-- Dumping routines (PROCEDURE) for database 'quintime'
--
DELIMITER ;;

# Dump of PROCEDURE flexHoursPerHour
# ------------------------------------------------------------

/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`henrik`@`10.%`*/ /*!50003 PROCEDURE `flexHoursPerHour`()
BEGIN
        DROP TEMPORARY TABLE IF EXISTS tempFlexHours;
        CREATE TEMPORARY TABLE tempFlexHours (
            badgeNo varchar(10),
            givenName varchar(40),
            familyName varchar(40),
            salaryCode varchar(10),
            hourWorked varchar(10),
            hours decimal(11,2)
        );
        set @hour = 0;
        while @hour < 24 do
			set @time = sec_to_time(@hour*3600);
            
            insert into tempFlexHours (badgeNo, givenName, familyName, salaryCode, hourWorked, hours)
            select e.badgeno,  e.givenname, e.familyname, salarycode #tp.punchin, tp.punchout, tps.type_id, salarycode, tps.start, tps.end
,@time ,round(sum(
time_to_sec(least(addtime(@time, '01:00:00'),time(tps.end)))/3600 - time_to_sec(greatest(@time,time(tps.start)))/3600),2) 
 from timepunch_salaries tps
join salarytypes st on st.id = tps.type_id
join timepunch tp on tp.id = tps.timepunch_id
join employee e on e.id = tp.persid
join restaurants r on r.id = e.restid
where qt_customerid = 2839
and tp.punchdate between '2015-12-01' and '2015-12-31'
and salarycode like 'FL%'
			and time(tps.start) <= addtime(@time, '01:00:00')
			and time(tps.end) > @time
group by e.badgeno, salarycode;
            
            
            set @hour = @hour + 1;
        end while;

        
    END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
# Dump of PROCEDURE workedHoursPerHour
# ------------------------------------------------------------

/*!50003 SET SESSION SQL_MODE=""*/;;
/*!50003 CREATE*/ /*!50020 DEFINER=`henrik`@`10.%`*/ /*!50003 PROCEDURE `workedHoursPerHour`()
BEGIN
        DROP TEMPORARY TABLE IF EXISTS tempWorkedHours;
        CREATE TEMPORARY TABLE tempWorkedHours (
            UnitId int(11),
            UnitName varchar(40),
            dateWorked date,
            hourWorked varchar(10),
            hours decimal(11,2)
        );
        set @hour = 0;
        while @hour < 24 do
			set @time = sec_to_time(@hour*3600);
            
            insert into tempWorkedHours (UnitId, UnitName, dateWorked, hourWorked, hours)
            select r.id, r.name, 
			 
            tp.punchdate, #least(addtime(@time, '01:00:00'),tp.punchout)
            @time,
			round(sum(time_to_sec(least(addtime(@time, '01:00:00'),tp.punchout))/3600 - time_to_sec(greatest(@time,tp.punchin))/3600),2) 
			from timepunch tp
			join schedule s on s.id = tp.shiftreference
			join employee e on e.id = tp.persId
			join restaurants r on r.id = s.restid

			where qt_customerid = 2636
			#and weekday(tp.punchdate) >= 5
			and tp.punchdate between '2014-08-01' and '2015-09-30'
			#and tp.punchdate between '2015-07-20' and '2015-07-21'
			#and tp.punchdate in (
			#'2014-01-01',
			#'2014-01-06',
			#'2014-04-18',
			#'2014-04-19',
			#'2014-04-20',
			#'2014-04-21',
			#'2014-05-01',
			#'2014-05-29',
			#'2014-06-06',
			#'2014-06-07',
			#'2014-06-09',
			#'2014-06-20',
			#'2014-06-21',
			#'2014-11-01',
			#'2014-12-24',
			#'2014-12-25',
			#'2014-12-26',
			#'2014-12-31')
			and tp.punchin <= addtime(@time, '01:00:00')
			and tp.punchout > @time
			group by r.name, tp.punchdate
			;
            
            
            set @hour = @hour + 1;
        end while;

        
    END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
DELIMITER ;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
