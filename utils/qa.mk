
DCSPOCK := docker-compose -f compose/spock/docker-compose.yml

.PHONY: qa-spock-build
qa-spock-build:: ##@QA	Build spock container
	$(DCSPOCK) build

.PHONY: qa-spock-test
qa-spock-test:: ##@QA	Run spock tests (use `ARGS="" make qa-spock-test` to pass arguments)
	$(DCSPOCK) run --rm -w /scripts groovy mvn clean install $(ARGS)

# This may not be needed...
.PHONY: qa-spock-cmd
qa-spock-cmd:: ##@QA	Run a groovy command
	$(DCSPOCK) run --rm -w /script groovy $(CMD)

