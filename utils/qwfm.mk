PROJECT_NAME=qwfm
DC=docker-compose -p $(PROJECT_NAME)

.PHONY: build
build:: ##@QWFM	Build docker images
	$(DC) build --no-cache

.PHONY: rebuild
rebuild: ##@QWFM	Rebuild all images
	$(DC) down
	$(DC) up --force-recreate -d

.PHONY: start
start:: ##@QWFM	Start the dev envrionment
	$(DC) up -d
	@echo "Ready to go!"
	@echo "Development: http://qwfm"
	@echo "Mailhog: http://localhost:8025"
	@echo "Graylog: http://localhost:9000 (login: admin/admin)"

.PHONY: stop
stop:: ##@QWFM	Stop the dev environment
	$(DC) stop

.PHONY: destroy
destroy:: ##@QWFM	Stop dev environment and destroy images
	$(DC) down

.PHONY: restart
restart:: ##@QWFM	Resart dev environment
	$(DC) restart

.PHONY: status
status:: ##@QWFM	Show docker status
	docker ps

.PHONY: unit-test
unit-test:: ##@QWFM	Run unit tests. Pass params with 'make unit-test ARGS="--filter testFilterFreeDaysWithoutPunches"
	$(DC) run --rm -w /var/www/html/qwfm qwfm phpunit -d memory_limit=1024M $(ARGS) 

.PHONY: acceptance-test
acceptance-test:: ##@QWFM	Run acceptance tests
	@echo "NOT IMPLEMENTED"

.PHONY: composer-update
composer-update: ##@QWFM	Run PHP composer update    
	docker run -v $$PWD/code/php/qwfm:/app composer/composer update --ignore-platform-reqs

.PHONY: composer-fresh
composer-fresh: ##@QWFM	Delete vendor/ folder and re-install everything. Usually not needed, but shit happens...
	rm -rf $$PWD/code/php/qwfm/vendor
	mkdir -p $$PWD/code/php/qwfm/vendor
	$(DC) run --rm -w /var/www/html/qwfm qwfm composer install

.PHONY: wsdl-tester
wsdl-tester: ##@QWFM	Run WSDL tests
	$(DC) run --rm qwfm php /var/www/html/qwfm/src/qwfm/WsdlTester.php
