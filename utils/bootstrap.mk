
SRC_BASE := /var/www/html

define setup-laravel
mkdir -p code/php/$(APP)/cache/{views,sessions,logs,meta}
chmod 777 code/php/$(APP)/cache/{views,sessions,logs,meta}
cp code/php/$(APP)/app/config/app.php.default code/php/$(APP)/app/config/app.php
cp code/php/$(APP)/app/config/database.php.default code/php/$(APP)/app/config/database.php
docker run -it --rm -v $$PWD/code/php/$(APP):/app -v composer-cache:/composer/cache composer/composer install --ignore-platform-reqs
docker-compose -p qwfm run --rm -w $(SRC_BASE)/$(APP) qwfm  php artisan key:generate
endef


.PHONY: bootstrap-backend
bootstrap-backend:: code/php/api/vendor code/php/qwfm/vendor $$HOME/local/arcanist/ env logs ##@Bootstrap	Bootstrap backend environments (proto and neo)
	$(MAKE) flex-build-container
	$(MAKE) flex-build-app
	@echo "********************************************"
	@echo "DONE!"
	@echo "********************************************"
	@echo "QWFM (Proto) is ready to go"
	@echo ""
	@echo "PLEASE REMEBER TO:"
	@echo " - Update your PATH to include HOME/local/bin"
	@echo " - Update the env file with proper values"
	@echo ""
	@echo "Then you can get started with 'make start'"

docker-compose.yml: code
	-cp -n docker-compose.yml.sample docker-compose.yml

env: code
	-cp -n env.sample env

logs: code
	mkdir -p logs

code:
	git clone ssh://vcs@phabricator.quinyx.com/diffusion/C/code.git && \
	cd code && \
	git checkout develop && \
	cd ..

code/php/api/vendor: code
	$(eval APP := api)
	$(setup-laravel)

code/php/webpunch2/vendor: code
	$(eval APP := webpunch2)
	$(setup-laravel)

code/php/qwfm/vendor: code
	$(eval APP := qwfm)
	mkdir -p code/php/qwfm/public/{temp,dump} && \
	chmod 777 code/php/qwfm/public/{temp,dump} && \
	docker run -it --rm -v $$PWD/code/php/$(APP):/app -v composer-cache:/composer/cache composer/composer install --ignore-platform-reqs
	
$$HOME/local/arcanist/: 
	mkdir -p $$HOME/local && \
	rm -rf $$HOME/local/arcanist $$HOME/local/libphutil $$HOME/local/bin/arc && \
	git clone https://github.com/phacility/arcanist.git $$HOME/local/arcanist && \
	git clone https://github.com/phacility/libphutil.git $$HOME/local/libphutil && \
	ln -s $$HOME/local/arcanist/bin/arc $$HOME/local/bin/arc


.PHONY: bootstrap-android
bootstrap-android:: android ##@Bootstrap	Bootstrap a new Android development environment

android:
	git clone ssh://vcs@phabricator.quinyx.com/diffusion/AND/quinyx-android-code.git android

.PHONY: bootstrap-ios
bootstrap-ios:: ios ##@Bootstrap	Bootstrap a new iOS development environment

ios:
	git clone ssh://vcs@phabricator.quinyx.com/diffusion/IOS/quinyx-ios-code.git ios

