
SWF_FOLDER := code/php/qwfm/public/swf
MAJOR := 2
MINOR := $(shell git -C code/ log --oneline --decorate=short | grep "(tag:" | head -n1 | awk '{print $$3}' | tr -d ')\n')
COMMIT := $(shell git -C code/ rev-parse --short HEAD) 
BUILD := $(shell git -C code/ rev-list $(MINOR)..HEAD --count)

LINGO_URL := https://web.quinyx.com/api/lingo
LOCALES := sv_SE da_DK en_US fi_FI hu_HU ja_JP lt_LT no_NO pl_PL ro_RO ru_RU de_DE es_ES et_EE fr_FR nl_NL zh_CN
tmp_targets = $(addprefix QuinTime_Resources_$(MINOR)_$(BUILD)_, $(LOCALES))
locale_targets = $(addsuffix .json, $(tmp_targets))

.PHONY: flex-build-container
flex-build-container:: ##@QWFM (Flex)	Build the flex docker-container used to compile the app
	cd compose/flex-app &&\
	docker build --no-cache -t quinyx/flex-app . && \
	cd ../..

.PHONY: flex-build-app
flex-build-app:: $(SWF_FOLDER)/version.json $(locale_targets) ##@QWFM (Flex)	Build the flex app and all resources
	if [ -e "code/php/qwfm/public/swf/QuinTime.swf" ] ;\
	then \
		cp code/php/qwfm/public/swf/QuinTime.swf backups/Quintime.swf-$(shell date +%Y%m%d-%H%M) ; \
	fi;
	docker run --rm -it -v $$PWD/code:/code quinyx/flex-app /flex-sdk/bin/mxmlc \
	-source-path=/code/flex/quinyx-model/src \
	-source-path=/code/flex/third-party/src \
	-source-path=/code/flex/quinyx-app/src \
	-source-path+=/code/flex/quinyx-app/style \
	-source-path+=/code/flex/quinyx-app/assets \
	-source-path+=/code/flex/auto-libs/ \
	-library-path+=/code/flex/libs/ \
	-output /code/php/qwfm/public/swf/QuinTime.swf \
	-static-rsls=true \
	-services /code/php/qwfm/public/services-config.xml \
	-theme="/flex-sdk/frameworks/themes/Halo/halo.swc" \
	-default-background-color=0x45423f \
	/code/flex/quinyx-app/src/QuinTime.mxml
	cp $(SWF_FOLDER)/QuinTime.swf $(SWF_FOLDER)/QWFM-$(MAJOR).$(MINOR).$(BUILD).swf

$(locale_targets):
	$(eval LOC = $(shell echo $@ | cut -d '_' -f5,6 | cut -d '.' -f1))
	curl -# $(LINGO_URL)/tags/$(LOC) > $(SWF_FOLDER)/$@
	cp $(SWF_FOLDER)/$@ $(SWF_FOLDER)/QuinTime_Resources_$(LOC).json

.PHONY: language-files
language-files: $(locale_targets) ##@QWFM (Flex)	Download language files

 

$(SWF_FOLDER)/version.json:
	mkdir -p $(SWF_FOLDER)
	printf "{\"commit\": \"%s\", \"major\": %s, \"build\": %s, \"minor\": %s}" "$(COMMIT)" "$(MAJOR)" "$(BUILD)" "$(MINOR)" > code/php/qwfm/public/swf/version.json


.PHONY: flex-clean
flex-clean:: ##@QWFM (Flex)	Clean the flex artifacts
	rm -f $(SWF_FOLDER)/*

