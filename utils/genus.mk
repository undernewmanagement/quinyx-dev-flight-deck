
.PHONY: genus-build
genus-build:: ##@Genus	Build Genus .jar file
	docker run -it --rm --name genus -v "$$PWD/code/java":/app -v m2:/root/.m2 -w /app/standalone/ng/genus maven mvn install

.PHONY: genus-clean
genus-clean:: ##@Genus	Clean Genus build artifacts
	docker run -it --rm --name genus -v "$$PWD/code/java":/app -v m2:/root/.m2 -w /app/standalone/ng/genus maven mvn clean

