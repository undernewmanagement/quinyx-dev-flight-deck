# Introduction

This repo is a simple set of task runners that help you provision and manage 
multiple development environments for Quinyx development using docker images. 
There is little need to install too many packages into your host operating 
system. Currently you can:

  * Install arcanist
  * Stand up a Proto backend with nginx, php, redis, and greylog logviewer
  * Run proto unit tests
  * Compile the Flex application
  * Compile the Genus .jar file

![taskrunner screenshot](docs/images/screenshot.png)

Planned features:

  * Stand up Webpunch2, Webpunch3
  * Stand up Neo environment
  * Stand up Neo front-end development with react and all Javscript goodness...

Using only makefiles, docker, and docker-compose you can isolate multiple 
environments with ease. An added benefit is that you can always restore your 
environment(s) to a previous state with just a single command.

# But I already have a working environment!

**NOTE**: This is not thoroughly tested! Backup your code folder before attempting this.

No problem! You can work in a separate folder, or simply link your existing
code folder into this repo. To do that simply run:

```bash
$ cd REPO_FOLDER
$ ln -s /full/path/to/your/codebase code
```

# Requirements
There are some minimum requirements before you can get started with this:

 * `make` from the GNU utils.
 * `docker` and `docker-compose`, available from Docker.com (See install instructions)
 * ssh keypair installed in your `~/.ssh` folder and uploaded to your phabricator account.
 * a Phabricator account (See @devops team for this account)
 * Make sure you have the `qmwf` hostname in your `/etc/hosts` file.

```
127.0.0.1   localhost qwfm
```
 

## Installing requirements on OSX

  1. Get xcode command line tools with `xcode-select --install`. Confirm with
     dialogues and you are on your way.
  2. Install docker from here (https://download.docker.com/mac/stable/Docker.dmg)
  3. Ready to go!

## Installing requirements on Linux
This document assumes you are on a Debian/Ubuntu based system.

  1. Install development-essentials with `sudo apt-get update && sudo apt-get install build-essential`
  2. Installing docker
    * On Ubuntu: https://docs.docker.com/engine/installation/linux/ubuntulinux/
    * On Debian: https://docs.docker.com/engine/installation/linux/debian/
  3. Ready to go!

## Installing requirements on Windows
TODO (if we need it)


# Getting started
Once you have the requirements in place we can begin:

  1. `git clone ssh://vcs@phabricator.quinyx.com/diffusion/FD/quinyx-flightdeck.git`
  1. run `make bootstrap-backend` to standup a development environment. This takes a while.
  1. When bootstrapping is completed you must edit the `env` file with secrets for the test database.
Go here for that: https://phabricator.quinyx.com/K9

Your source code will live in the `code/` folder of this directory.

NGINX and QWFM logs live in the `logs/` folder

## Customise your docker-compose.yml file
Some of you may already have HTTP services running on your machnine. This might create
resource conflicts with TCP ports. When bootstrapping with `make bootstrap` the script
will copy a sample docker-compose.yml into place.

You can modify this file to customize ports to suit your envirionment.

Most likely you will want to modify the nginx config:
Have a look at the ports: section. 
```
    nginx:
        image: nginx
        depends_on: 
            - qwfm
        ports:
            - "YOUR_CUSTOM_PORT:80"
        volumes:
            - ./code/php/:/var/www/html
            - ./compose/nginx/nginx.conf:/etc/nginx/conf.d/qwfm.conf
            - ./logs/:/var/log/nginx
```

## Command groups
There are a lot of commands, and they are broken down into groups:

  * **Bootstrap** : Create new development environments or link your existing
    ones into docker
  * **Genus** : Manage cleaning / compiling the Genus .jar file
  * **QA** : Run tests (spock, etc) (NOT IMPLEMENTED) 
  * **QWFM** : Manage the Proto development environment
  * **QWFM (Flex)**: Manage building of Flex app

## The Constants.php file (Proto)
By convention this repo uses environment variables wherever it can
to make configuration and deployment easier. Therefore, we use
some docker tricks to overwrite the `Constants.php` with a custom
one (see `composer/qwf/Constants.php`).

Rather than change the values in `Constants.php` directly you should
set the values in the `env` file you copied.

**NOTE**: You can obtain the password to the test database from 
here: https://phabricator.quinyx.com/K9

## Starting the development environment 
  1. run `make start`   
**NOTE**: This may take a while the first time you run docker because it must 
download and compile several images. Also, logs appear in `logs/` folder. 

## Bootstrap a new environment
Simply type `make boostrap-backend` and watch the task runner provision a new 
envrionemnt. The source folder will live in `code/` in the same directory
as the Makefile. Note, if the `code/` folder already exists then the runner
will simply assume it is done and sotp.

## Building the flex application
If you need to compile the flex applicaton then do not worry. "There is a
docker image for that." 
  1. First you need to download the flash Builder SDK from google drive: https://drive.google.com/open?id=0B7T4QTeszu2YbWk5RWNWRkFlUmc
     *NOTE** you will need access / privileges to the Quinyx google drive shared folder to reach this file.
  2. Download and place this file in `compose/flex-app/` folder.
  3. Run `make flex-build-container` then `make flex-build-app`

It will take a little while to compile, but eventually it will place the 
compiled asset into the `code/php/qwfm/public/swf/` folder.


# FAQ

## Where are my logs?
Logs for nginx, php, and mysql queries are appear in `logs/` folder.

# Contributing

## Documentation
If you update the readme then please make sure to run `make readme-toc` to
regenerate the table of contents at the top of the file

## Can I use XDebug?
Yes, but holy hell is it complicated. Fortunately the setup onn the PHP
side is already done. This will cover setting up PHPStorm and docker.

This command will allow PHPStorm to connect and control docker
`socat TCP-LISTEN:2375,reuseaddr,fork,bind=localhost UNIX-CONNECT:/var/run/docker.sock`

*Some reference notes*
  * https://gist.github.com/chadrien/c90927ec2d160ffea9c4
  * https://www.youtube.com/watch?v=GokeXqI93x8&t=433s

