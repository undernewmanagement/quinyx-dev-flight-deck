include utils/*.mk

.PHONY: clean
clean:: ##@Other	Clean up logs, tmp files, and build artifacts
	rm logs/*.log

.PHONY: uninstall
uninstall:: ##@Other	Uninstall arcanist and applications
	rm -f $$HOME/local/bin/arc
	rm -rf $$HOME/local/arcanist
	rm -rf $$HOME/local/libphutil
	rm -rf code/
